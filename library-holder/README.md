Widget-Adapters-Holder
===============

This module contains basic **view holder** interfaces and implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-holder:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [AdapterHolder](https://bitbucket.org/android-universum/widget-adapters/src/main/library-holder/src/main/java/universum/studios/android/widget/adapter/holder/AdapterHolder.java)
- [AdapterBindingHolder](https://bitbucket.org/android-universum/widget-adapters/src/main/library-holder/src/main/java/universum/studios/android/widget/adapter/holder/AdapterBindingHolder.java)
- [ViewHolder](https://bitbucket.org/android-universum/widget-adapters/src/main/library-holder/src/main/java/universum/studios/android/widget/adapter/holder/ViewHolder.java)