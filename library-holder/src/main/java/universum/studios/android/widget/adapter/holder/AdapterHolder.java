/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Interface specifying basic API for holders used in adapters.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface AdapterHolder {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant that identifies invalid/unspecified position attached to a specified holder.
	 */
	int NO_POSITION = -1;

	/**
	 * Constant that identifies basic type of item view associated with a specified holder.
	 */
	int BASIC_TYPE = 0;

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Simple interface for adapters providing a convenience layer to access adapter's items at a
	 * desired positions.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 */
	interface ItemAdapter {

		/**
		 * Convenience method that may be used to obtain an item with its corresponding data from
		 * this adapter for the specified <var>position</var>.
		 *
		 * @param position Position of the desired item to obtain.
		 * @return Item model with the corresponding data.
		 */
		@NonNull
		Object getItem(int position);
	}

	/**
	 * Interface for factories that may be used to create instances of view holders for adapters.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <H> Type of the holder of which instances this factory can create.
	 */
	interface Factory<H extends AdapterHolder> {

		/**
		 * Creates a new instance of view holder with item view of the requested <var>viewType</var>.
		 *
		 * @param parent   Parent view that will host view of the created holder as its child view.
		 * @param viewType Type of the view with which should the factory create the requested holder.
		 * @return View holder with view according to the requested type.
		 */
		@NonNull
		H createHolder(@NonNull ViewGroup parent, int viewType);
	}

	/**
	 * A {@link Factory} implementation backed by {@link LayoutInflater} that may be used as base
	 * for simple holder factories that only create view holder instances along with theirs corresponding
	 * item views. This factory may be created via either {@link #InflaterFactory(Context)} or
	 * {@link #InflaterFactory(LayoutInflater)} constructor and then the desired item views may be
	 * inflated via {@link #inflateView(int, ViewGroup)}.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 */
	abstract class InflaterFactory<H extends AdapterHolder> implements Factory<H> {

		/**
		 * Layout inflater that is used to inflateView new item views for holders provided by this factory.
		 */
		private LayoutInflater inflater;

		/**
		 * Creates a new instance of InflaterFactory with a lazily initialized {@link LayoutInflater}
		 * that is obtained when {@link #createHolder(ViewGroup, int)} is for the first time called.
		 *
		 * @see #InflaterFactory(Context)
		 * @see #InflaterFactory(LayoutInflater)
		 */
		public InflaterFactory() {
			// Lazily initialized inflater in createView(...) method.
		}

		/**
		 * Creates a new instance of InflaterFactory with {@link LayoutInflater} obtained from the
		 * given <var>context</var>.
		 *
		 * @param context Context used to obtain layout inflater for this factory.
		 *
		 * @see #InflaterFactory(LayoutInflater)
		 */
		public InflaterFactory(@NonNull final Context context) {
			this(LayoutInflater.from(context));
		}

		/**
		 * Creates a new instance of InflaterFactory with the specified <var>inflater</var>.
		 *
		 * @param inflater Layout inflater that will be used by the new factory to inflateView item
		 *                 views for holders.
		 *
		 * @see #inflateView(int, ViewGroup)
		 */
		public InflaterFactory(@NonNull final LayoutInflater inflater) {
			this.inflater = inflater;
		}

		/**
		 * Inflates a new view hierarchy from the given xml resource.
		 *
		 * @param resource Resource id of a view to inflateView.
		 * @param parent   A parent view, to resolve correct layout params for the newly creating view.
		 * @return The root view of the inflated view hierarchy.
		 *
		 * @see LayoutInflater#inflate(int, ViewGroup)
		 */
		@NonNull
		public View inflateView(@LayoutRes final int resource, @NonNull final ViewGroup parent) {
			if (inflater == null) {
				inflater = LayoutInflater.from(parent.getContext());
			}

			return inflater.inflate(resource, parent, false);
		}
	}

	/**
	 * Interface for adapters to which may be attached a desired {@link Factory} in order to handle
	 * creation of {@link AdapterHolder} instances for such adapters.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <H> Type of the holder that may be created by the associated factory.
	 */
	interface FactoryAdapter<H extends AdapterHolder> {

		/**
		 * Sets a factory that should be used by this adapter to create holders for its item views.
		 *
		 * @param factory The desired factory. May be {@code null} to indicate that it is the adapter's
		 *                responsibility to handle creation of holders.
		 *
		 * @see Factory#createHolder(ViewGroup, int)
		 * @see #getHolderFactory()
		 */
		void setHolderFactory(@Nullable Factory<H> factory);

		/**
		 * Returns the holder factory associated with this adapter.
		 *
		 * @return Factory used by this adapter to create holder instances or {@code null} if no
		 * factory has been specified.
		 *
		 * @see #setHolderFactory(Factory)
		 */
		@Nullable
		Factory<H> getHolderFactory();
	}

	/**
	 * Interface for binders that may be used to bind view holders with data from adapter.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <A> Type of the adapter of which data will this binder bind to the holder.
	 * @param <H> Type of the holder that is to be bound with data from the adapter.
	 */
	interface Binder<A, H extends AdapterHolder> {

		/**
		 * Binds data from the specified <var>adapter</var> to the given <var>holder</var> according
		 * to the current adapter position of the holder.
		 *
		 * @param adapter  The adapter that holds data for the holder.
		 * @param holder   The holder to which to bind the associated data.
		 * @param position The position for which to obtain data from the adapter.
		 * @param payloads Additional list of payloads for the binding. May be an empty list.
		 */
		void bindHolder(@NonNull A adapter, @NonNull H holder, int position, @NonNull List<Object> payloads);
	}

	/**
	 * Interface for adapters to which may be attached a desired {@link Binder} in order to handle
	 * binding logic of data to {@link AdapterHolder} instances for such adapters.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <A> Type of the adapter of which instance will be passed to binder.
	 * @param <H> Type of the holder associated with the adapter.
	 */
	interface BinderAdapter<A, H extends AdapterHolder> extends ItemAdapter {

		/**
		 * Sets a binder that should be used by this adapter to perform binding of data from its
		 * data set to its corresponding holders.
		 *
		 * @param binder The desired binder. May be {@code null} to indicate that it is the adapter's
		 *               responsibility to handle binding logic for holders.
		 *
		 * @see Binder#bindHolder(Object, AdapterHolder, int, List)
		 * @see #getHolderBinder()
		 */
		void setHolderBinder(@Nullable Binder<A, H> binder);

		/**
		 * Returns the holder binder associated with this adapter.
		 *
		 * @return Binder used by this adapter to perform binding of data to holders or {@code null}
		 * if no binder has been specified.
		 *
		 * @see #setHolderBinder(Binder)
		 */
		@Nullable
		Binder<A, H> getHolderBinder();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the type of the item view associated with this holder instance.
	 * <p>
	 * If this holder has been created without item view type explicitly specified then this method
	 * should return {@link #BASIC_TYPE}.
	 *
	 * @return Type of this holder's item view.
	 */
	int getItemViewType();

	/**
	 * Returns the adapter position of the item represented by this holder instance.
	 *
	 * @return Position of the item within associated adapter's data set or {@link #NO_POSITION}
	 * if this holder is not bound yet or the position is unavailable at the time.
	 */
	int getBindingAdapterPosition();
}
