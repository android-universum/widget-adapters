/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * An extension of {@link AdapterHolder} that may be used for adapter holder implementations in
 * association with {@link AdapterHolder.Binder} to simplify data binding logic.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <A> Type of the adapter of which data may be bound to this holder.
 */
public interface AdapterBindingHolder<A> extends AdapterHolder {

	/**
	 * Binds data of the given <var>adapter</var> to this holder for the specified <var>position</var>.
	 *
	 * @param adapter  The adapter of which data to bind to this holder.
	 * @param position The position for which to obtain data from the adapter.
	 * @param payloads Additional list of payloads for the binding. May be an empty list.
	 */
	void bind(@NonNull A adapter, int position, @Nullable List<Object> payloads);
}