/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Factory that provides common implementations of adapter holder {@link AdapterHolder.Binder Binders}.
 *
 * <h3>Implementations</h3>
 * <ul>
 * <li>{@link #simple()}</li>
 * <li>{@link #binding()}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class ViewHolderBinders {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private ViewHolderBinders() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns a new instance of {@link AdapterHolder.Binder} which performs binding of desired view
	 * holders by attaching a String representation of for theirs current adapter position associated
	 * item to theirs item view if it is instance of {@link TextView} via {@link TextView#setText(CharSequence)}.
	 *
	 * @param <A>  Type of the adapter that will be used for binding operation.
	 * @param <VH> Type of the view holder that is to be bound with data provided by the adapter.
	 * @return Holder binder ready to be used.
	 *
	 * @see #binding()
	 */
	@NonNull public static <A extends AdapterHolder.ItemAdapter, VH extends ViewHolder> AdapterHolder.Binder<A, VH> simple() {
		return new Simple<>();
	}

	/**
	 * Returns a new instance of {@link AdapterHolder.Binder} which performs binding of desired view
	 * holders by simply invoking theirs {@link AdapterBindingHolder#bind(Object, int, List)} method
	 * and passing the supplied adapter, position along with payloads to it.
	 *
	 * @param <A>  Type of the adapter that will be used for binding operation.
	 * @param <VH> Type of the view holder that is to be bound with data provided by the adapter.
	 * @return Holder binder ready to be used.
	 *
	 * @see #simple()
	 */
	@NonNull public static <A extends AdapterHolder.ItemAdapter, VH extends AdapterBindingHolder<A>> AdapterHolder.Binder<A, VH> binding() {
		return new Binding<>();
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link AdapterHolder.Binder} implementation that performs simple binding of desired view
	 * holders by attaching a String representation of for theirs current adapter position associated
	 * item to theirs item view if it is instance of {@link TextView} via {@link TextView#setText(CharSequence)}.
	 */
	private static final class Simple<A extends AdapterHolder.ItemAdapter, VH extends ViewHolder> implements AdapterHolder.Binder<A, VH> {

		/**
		 */
		@Override public void bindHolder(@NonNull final A adapter, @NonNull final VH holder, final int position, @Nullable final List<Object> payloads) {
			if (holder.itemView instanceof TextView) {
				((TextView) holder.itemView).setText(adapter.getItem(position).toString());
			}
		}
	}

	/**
	 * A {@link AdapterHolder.Binder} implementation that performs binding of desired view holders
	 * simply by invoking theirs {@link AdapterBindingHolder#bind(Object, int, List)} method and passing
	 * the supplied adapter, position along with payloads to it.
	 */
	private static final class Binding<A extends AdapterHolder.ItemAdapter, VH extends AdapterBindingHolder<A>> implements AdapterHolder.Binder<A, VH> {

		/**
		 */
		@Override public void bindHolder(@NonNull final A adapter, @NonNull final VH holder, final int position, @Nullable final List<Object> payloads) {
			holder.bind(adapter, position, payloads);
		}
	}
}