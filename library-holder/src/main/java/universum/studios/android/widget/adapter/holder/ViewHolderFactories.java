/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

/**
 * Factory that provides common implementations of adapter holder {@link AdapterHolder.Factory Factories}.
 *
 * <h3>Implementations</h3>
 * <ul>
 * <li>{@link #simple()}</li>
 * <li>{@link #resource(int)}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class ViewHolderFactories {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Layout resource used for simple implementation of {@link AdapterHolder.Factory}.
	 *
	 * @see #simple()
	 */
	@LayoutRes public static final int SIMPLE_RESOURCE = android.R.layout.simple_list_item_1;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private ViewHolderFactories() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Same as {@link #resource(int)} with {@link android.R.layout#simple_list_item_1 @android:layout/simple_list_item_1}
	 * as <var>resource</var> parameter.
	 */
	@NonNull public static AdapterHolder.Factory<ViewHolder> simple() {
		return resource(SIMPLE_RESOURCE);
	}

	/**
	 * Returns a new instance of {@link AdapterHolder.Factory} which creates instances of {@link ViewHolder ViewHolders}
	 * with item views inflated from the specified layout <var>resource</var>.
	 *
	 * @param resource Layout resource used by the returned factory to inflateView item views for its
	 *                 provided holders.
	 * @return Holder factory ready to be used.
	 *
	 * @see #simple()
	 */
	@NonNull public static AdapterHolder.Factory<ViewHolder> resource(@LayoutRes final int resource) {
		return new ResourceFactory(resource);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link AdapterHolder.InflaterFactory} implementation that creates instances of
	 * {@link ViewHolder} with item view inflated for a desired layout resource specified via
	 * {@link #ResourceFactory(int)} constructor.
	 */
	private static final class ResourceFactory extends AdapterHolder.InflaterFactory<ViewHolder> {

		/**
		 * Layout resource used by this factory to inflateView new item views for its provided holders.
		 */
		private final int resource;

		/**
		 * Creates a new instance of ResourceFactory with the specified layout <var>resource</var>.
		 *
		 * @param resource Resource of layout which should be inflated as item view for view holders
		 *                 created by the new factory.
		 */
		ResourceFactory(final int resource) {
			super();
			this.resource = resource;
		}

		/**
		 */
		@Override @NonNull public ViewHolder createHolder(@NonNull final ViewGroup parent, final int viewType) {
			return new ViewHolder(inflateView(resource, parent));
		}
	}
}