/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.view.View;

import androidx.annotation.NonNull;

/**
 * A basic implementation of {@link AdapterHolder} that should be a required type of view holder for
 * all {@code BaseAdapter} implementations.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ViewHolder implements AdapterHolder {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ViewHolder";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * View with which has been this holder created.
	 */
	public final View itemView;

	/**
	 * Identifies type of the item view associated with this holder.
	 */
	private final int itemViewType;

	/**
	 * Current position of an item from the associated adapter's data set of which data are presented
	 * in item view of this holder.
	 */
	private int bindingAdapterPosition = NO_POSITION;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ViewHolder for the given <var>itemView</var>.
	 *
	 * @param itemView Instance of the view to be associated with new holder.
	 */
	public ViewHolder(@NonNull final View itemView) {
		this(itemView, BASIC_TYPE);
	}

	/**
	 * Creates a new instance of ViewHolder for the given <var>itemView</var> that is of the specified
	 * <var>itemViewType</var>.
	 *
	 * @param itemView Instance of the view to be associated with new holder.
	 */
	public ViewHolder(@NonNull final View itemView, final int itemViewType) {
		this.itemView = itemView;
		this.itemViewType = itemViewType;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	public final int getItemViewType() {
		return itemViewType;
	}

	/**
	 * Updates the current binding adapter position of this holder instance.
	 * <p>
	 * <b>Note that this method should be only called by the associated adapter for which context
	 * has been this holder instance created and from the appropriate {@code getView(...)} method.
	 * Calling this method from outside of such context may cause inconsistent results of
	 * {@link #getBindingAdapterPosition()}.</b>
	 *
	 * @param position The new adapter position for this holder.
	 *
	 * @see #getBindingAdapterPosition()
	 */
	public final void updateBindingAdapterPosition(final int position) {
		this.bindingAdapterPosition = position;
	}

	/**
	 */
	@Override
	public final int getBindingAdapterPosition() {
		return bindingAdapterPosition;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
