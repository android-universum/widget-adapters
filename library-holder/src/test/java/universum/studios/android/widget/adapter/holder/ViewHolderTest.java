/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.view.View;

import org.junit.Test;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class ViewHolderTest extends AndroidTestCase {

	@Test public void testInstantiation() {
		// Arrange:
		final View itemView = new View(getContext());

		// Act:
		final ViewHolder holder = new ViewHolder(itemView);

		// Assert:
		assertThat(holder.itemView, is(itemView));
		assertThat(holder.getBindingAdapterPosition(), is(ViewHolder.NO_POSITION));
		assertThat(holder.getItemViewType(), is(ViewHolder.BASIC_TYPE));
	}

	@Test public void testInstantiationWithViewType() {
		// Arrange:
		final View itemView = new View(getContext());

		// Act:
		final ViewHolder holder = new ViewHolder(itemView, 2);

		// Assert:
		assertThat(holder.getBindingAdapterPosition(), is(ViewHolder.NO_POSITION));
		assertThat(holder.getItemViewType(), is(2));
	}

	@Test public void testUpdateBindingAdapterPosition() {
		// Arrange:
		final View itemView = new View(getContext());
		final ViewHolder holder = new ViewHolder(itemView);

		// Act:
		holder.updateBindingAdapterPosition(13);

		// Assert:
		assertThat(holder.getBindingAdapterPosition(), is(13));
	}
}
