/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import androidx.annotation.NonNull;
import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class AdapterHolderTest extends AndroidTestCase {

	private ViewGroup container;

	@Before public void beforeTest() {
		this.container = new FrameLayout(getContext());
	}

	@After public void afterTest() {
		this.container = null;
	}

	@Test public void testConstants() {
		// Assert:
		assertThat(AdapterHolder.NO_POSITION, is(-1));
	}

	@Test public void testInflaterFactory() {
		// Arrange:
		final AdapterHolder.InflaterFactory<TestViewHolder> factory = new TestInflaterFactory();
		// Act + Assert:
		assertThat(factory.inflateView(android.R.layout.simple_list_item_2, container), is(notNullValue()));
		final TestViewHolder holder = factory.createHolder(container, 0);
		assertThat(holder, is(notNullValue()));
		assertThat(holder.itemView, is(notNullValue()));
	}

	@Test public void testInflaterFactoryWithInjectedContext() {
		// Arrange:
		final AdapterHolder.InflaterFactory<TestViewHolder> factory = new TestInflaterFactory(getContext());
		// Act + Assert:
		assertThat(factory.inflateView(android.R.layout.simple_list_item_2, container), is(notNullValue()));
		final TestViewHolder holder = factory.createHolder(container, 0);
		assertThat(holder, is(notNullValue()));
		assertThat(holder.itemView, is(notNullValue()));
	}

	@Test public void testInflaterFactoryWithInjectedInflater() {
		// Arrange:
		final AdapterHolder.InflaterFactory<TestViewHolder> factory = new TestInflaterFactory(LayoutInflater.from(getContext()));
		// Act + Assert:
		assertThat(factory.inflateView(android.R.layout.simple_list_item_2, container), is(notNullValue()));
		final TestViewHolder holder = factory.createHolder(container, 0);
		assertThat(holder, is(notNullValue()));
		assertThat(holder.itemView, is(notNullValue()));
	}

	private static final class TestInflaterFactory extends AdapterHolder.InflaterFactory<TestViewHolder> {

		TestInflaterFactory() { super(); }
		TestInflaterFactory(@NonNull final Context context) { super(context); }
		TestInflaterFactory(@NonNull final LayoutInflater inflater) { super(inflater); }

		@Override @NonNull public TestViewHolder createHolder(@NonNull final ViewGroup parent, final int viewType) {
			return new TestViewHolder(inflateView(android.R.layout.simple_list_item_1, parent));
		}
	}
}