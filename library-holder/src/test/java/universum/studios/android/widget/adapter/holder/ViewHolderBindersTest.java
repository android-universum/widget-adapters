/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Martin Albedinsky
 */
public final class ViewHolderBindersTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		ViewHolderBinders.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<ViewHolderBinders> constructor = ViewHolderBinders.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testSimple() {
		// Arrange:
		final AdapterHolder.Binder<AdapterHolder.ItemAdapter, ViewHolder> binder = ViewHolderBinders.simple();
		// Act + Assert:
		assertThat(binder, is(notNullValue()));
		assertThat(binder, is(not(ViewHolderBinders.simple())));
	}

	@SuppressWarnings("unchecked")
	@Test public void testSimpleBindHolder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter();
		final TestHolder holder = new TestHolder(new TextView(getContext()));
		final AdapterHolder.Binder<AdapterHolder.ItemAdapter, ViewHolder> binder = ViewHolderBinders.simple();
		for (int i = 0; i < 10; i++) {
			holder.updateBindingAdapterPosition(i);
			// Act:
			binder.bindHolder(adapter, holder, i, Collections.EMPTY_LIST);
			// Assert:
			assertThat(((TextView) holder.itemView).getText().toString(), is("Item at: " + i));
		}
	}

	@SuppressWarnings("unchecked")
	@Test public void testSimpleBindHolderWithItemViewNotTextView() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter();
		final TestHolder holder = new TestHolder(new LinearLayout(getContext()));
		final AdapterHolder.Binder<AdapterHolder.ItemAdapter, ViewHolder> binder = ViewHolderBinders.simple();
		for (int i = 0; i < 10; i++) {
			holder.updateBindingAdapterPosition(i);
			// Act:
			binder.bindHolder(adapter, holder, i, Collections.EMPTY_LIST);
		}
	}

	@Test public void testBinding() {
		// Arrange:
		final AdapterHolder.Binder<AdapterHolder.ItemAdapter, AdapterBindingHolder<AdapterHolder.ItemAdapter>> binder = ViewHolderBinders.binding();
		// Act + Assert:
		assertThat(binder, is(notNullValue()));
		assertThat(binder, is(not(ViewHolderBinders.binding())));
	}

	@SuppressWarnings("unchecked")
	@Test public void testBindingBindHolder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter();
		final AdapterBindingHolder mockHolder = mock(AdapterBindingHolder.class);
		final AdapterHolder.Binder<AdapterHolder.ItemAdapter, AdapterBindingHolder<AdapterHolder.ItemAdapter>> binder = ViewHolderBinders.binding();
		for (int i = 0; i < 10; i++) {
			// Act:
			binder.bindHolder(adapter, mockHolder, i, Collections.EMPTY_LIST);
			// Assert:
			verify(mockHolder).bind(adapter, i, Collections.EMPTY_LIST);
		}
		verifyNoMoreInteractions(mockHolder);
	}

	private static final class TestAdapter implements AdapterHolder.ItemAdapter {

		@Override @NonNull public Object getItem(final int position) {
			return "Item at: " + position;
		}
	}

	private static class TestHolder extends ViewHolder implements AdapterBindingHolder<AdapterHolder.ItemAdapter> {

		TestHolder(@NonNull final View itemView) {
			super(itemView);
		}

		@Override public void bind(@NonNull final ItemAdapter adapter, final int position, @Nullable final List<Object> payloads) {
			// See holder mock.
		}
	}
}