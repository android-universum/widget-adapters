/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class ViewHolderFactoriesTest extends AndroidTestCase {

	private ViewGroup container;

	@Before public void beforeTest() {
		this.container = new FrameLayout(getContext());
	}

	@After public void afterTest() {
		this.container = null;
	}

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		ViewHolderFactories.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<ViewHolderFactories> constructor = ViewHolderFactories.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testSimple() {
		// Arrange:
		final AdapterHolder.Factory<ViewHolder> factory = ViewHolderFactories.simple();
		// Act + Assert:
		assertThat(factory, is(notNullValue()));
		assertThat(factory, is(not(ViewHolderFactories.simple())));
	}

	@Test public void testSimpleCreateHolder() {
		// Arrange:
		final AdapterHolder.Factory<ViewHolder> factory = ViewHolderFactories.simple();
		// Act + Assert:
		final ViewHolder holder = factory.createHolder(container, 0);
		assertThat(holder, is(notNullValue()));
		assertThat(holder.itemView, is(notNullValue()));
	}

	@Test public void testResource() {
		// Arrange:
		final AdapterHolder.Factory<ViewHolder> factory = ViewHolderFactories.resource(android.R.layout.simple_list_item_2);
		// Act + Assert:
		assertThat(factory, is(notNullValue()));
		assertThat(factory, is(not(ViewHolderFactories.resource(android.R.layout.simple_list_item_2))));
	}

	@Test public void testResourceCreateHolder() {
		// Arrange:
		final AdapterHolder.Factory<ViewHolder> factory = ViewHolderFactories.resource(android.R.layout.simple_list_item_2);
		// Act + Assert:
		final ViewHolder holder = factory.createHolder(container, 0);
		assertThat(holder, is(notNullValue()));
		assertThat(holder.itemView, is(notNullValue()));
	}
}