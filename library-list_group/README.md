@Widget-Adapters-List
===============

This module groups the following modules into one **single group**:

- [List-Base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list-base)
- [List-Simple](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list-simple)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-list:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core),
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state)