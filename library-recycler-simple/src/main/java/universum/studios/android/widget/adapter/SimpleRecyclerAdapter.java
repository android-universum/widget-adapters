/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.widget.adapter.holder.AdapterHolder;

/**
 * A {@link BaseRecyclerAdapter} and {@link SimpleAdapter} implementation which provides API for
 * simple management of items data set.
 *
 * <h3>Implementation</h3>
 * Implementation requirements are same as for {@link BaseRecyclerAdapter}.
 *
 * <h3>Default Holder Factory and Binder</h3>
 * This adapter does not use any default {@link AdapterHolder.Factory} nor default {@link AdapterHolder.Binder}
 * due to presence of {@link #onBindViewHolder(RecyclerView.ViewHolder, int, List)} method. A desired
 * factory along with desired binder may be however specified via {@link #setHolderFactory(AdapterHolder.Factory)}
 * and {@link #setHolderBinder(AdapterHolder.Binder)}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SimpleRecyclerAdapter<A extends SimpleRecyclerAdapter, VH extends RecyclerView.ViewHolder & AdapterHolder, I>
		extends BaseRecyclerAdapter<A, VH, I>
		implements
		SimpleAdapter<I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleRecyclerFragment";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Data set containing current data of this adapter.
	 */
	private final SimpleAdapterDataSet<I> dataSet;

	/**
	 * Registry with {@link OnDataSetSwapListener OnDataSetSwapListeners} that are attached to this
	 * adapter.
	 */
	private SwappableDataSetAdapterListeners<List<I>> listeners;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleRecyclerAdapter with empty data set.
	 *
	 * @param context Context in which will be this adapter used.
	 *
	 * @see #SimpleRecyclerAdapter(Context, Object[])
	 * @see #SimpleRecyclerAdapter(Context, List)
	 */
	public SimpleRecyclerAdapter(@NonNull final Context context) {
		this(context, new SimpleAdapterDataSet<I>());
	}

	/**
	 * Same as {@link #SimpleRecyclerAdapter(Context, List)} for array of initial <var>items</var>
	 * data set.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param items Array of items to be used as initial data set for this adapter.
	 */
	public SimpleRecyclerAdapter(@NonNull final Context context, @NonNull final I[] items) {
		this(context, Arrays.asList(items));
	}

	/**
	 * Creates a new instance of SimpleRecyclerAdapter with the given initial <var>items</var> data
	 * set.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param items   List of items to be used as initial data set for this adapter.
	 */
	public SimpleRecyclerAdapter(@NonNull final Context context, @NonNull final List<I> items) {
		this(context, new SimpleAdapterDataSet<>(items));
	}

	/**
	 * Creates a new instance of SimpleListAdapter with the specified data set.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param dataSet The data set that will contain items of the new adapter.
	 */
	@VisibleForTesting SimpleRecyclerAdapter(final Context context, final SimpleAdapterDataSet<I> dataSet) {
		super(context);
		this.dataSet = dataSet;
		this.dataSet.setItemsCallback(new SimpleAdapterDataSet.ItemsCallback() {

			/**
			 */
			@Override public void onItemRangeInserted(final int positionStart, final int itemCount) {
				notifyItemRangeInserted(positionStart, itemCount);
			}

			/**
			 */
			@Override public void onItemRangeChanged(final int positionStart, final int itemCount) {
				notifyItemRangeChanged(positionStart, itemCount);
			}

			/**
			 */
			@Override public void onItemMoved(final int fromPosition, final int toPosition) {
				notifyItemMoved(fromPosition, toPosition);
			}

			/**
			 */
			@Override public void onItemRangeRemoved(final int positionStart, final int itemCount) {
				notifyItemRangeRemoved(positionStart, itemCount);
			}
		});
		this.dataSet.setItemPositionResolver(new DataSetItemPositionResolver(this));
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void registerOnDataSetSwapListener(@NonNull final OnDataSetSwapListener<List<I>> listener) {
		if (listeners == null) {
			this.listeners = new SwappableDataSetAdapterListeners<>();
		}
		this.listeners.registerOnDataSetSwapListener(listener);
	}

	/**
	 */
	@Override public void unregisterOnDataSetSwapListener(@NonNull final OnDataSetSwapListener<List<I>> listener) {
		if (listeners != null) this.listeners.unregisterOnDataSetSwapListener(listener);
	}

	/**
	 */
	@Override public void changeItems(@Nullable final List<I> items) {
		swapItems(items);
	}

	/**
	 */
	@Override @Nullable public List<I> swapItems(@Nullable final List<I> items) {
		notifyItemsSwapStarted(items);
		final List<I> oldItems = onSwapItems(items);
		notifyItemsSwapFinished(items);
		notifyDataSetChanged();
		return oldItems;
	}

	/**
	 * Notifies all registered {@link OnDataSetSwapListener OnDataSetSwapListeners} that swapping
	 * of items for this adapter has been started.
	 *
	 * @param items The items that are about to be swapped for this adapter.
	 *
	 * @see #notifyItemsSwapFinished(List)
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	protected final void notifyItemsSwapStarted(@Nullable final List<I> items) {
		if (listeners != null) this.listeners.notifyDataSetSwapStarted(items);
	}

	/**
	 * Called from {@link #swapItems(List)} in order to swap data set of items of this adapter to
	 * the given <var>items</var>.
	 *
	 * @param items The new items data set to be changed for this adapter.
	 * @return Old items data set that has been attached to this adapter before. May be {@code null}
	 * if there were no items attached.
	 */
	@Nullable protected List<I> onSwapItems(@Nullable final List<I> items) {
		return dataSet.swapItems(items);
	}

	/**
	 * Notifies all registered {@link OnDataSetSwapListener OnDataSetSwapListeners} that swapping
	 * of items for this adapter has been finished.
	 *
	 * @param items The items that has been swapped for this adapter.
	 *
	 * @see #notifyItemsSwapStarted(List)
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	protected final void notifyItemsSwapFinished(@Nullable final List<I> items) {
		if (listeners != null) this.listeners.notifyDataSetSwapFinished(items);
	}

	/**
	 */
	@Override public void insertItem(@NonNull final I item) {
		this.dataSet.insertItem(item);
	}

	/**
	 */
	@Override public void insertItem(final int position, @NonNull final I item) {
		this.dataSet.insertItem(position, item);
	}

	/**
	 */
	@Override public void insertItems(@NonNull final List<I> items) {
		this.dataSet.insertItems(items);
	}

	/**
	 */
	@Override public void insertItems(final int positionStart, @NonNull final List<I> items) {
		this.dataSet.insertItems(positionStart, items);
	}

	/**
	 */
	@Override @NonNull public I swapItem(final int position, @NonNull final I item) {
		return dataSet.swapItem(position, item);
	}

	/**
	 */
	@Override @NonNull public I swapItemById(final long itemId, @NonNull final I item) {
		return dataSet.swapItemById(itemId, item);
	}

	/**
	 */
	@Override public void moveItem(final int fromPosition, final int toPosition) {
		this.dataSet.moveItem(fromPosition, toPosition);
	}

	/**
	 */
	@Override @NonNull public I removeItem(final int position) {
		return dataSet.removeItem(position);
	}

	/**
	 */
	@Override public int removeItem(@NonNull final I item) {
		return dataSet.removeItem(item);
	}

	/**
	 */
	@Override @NonNull public I removeItemById(final long itemId) {
		return dataSet.removeItemById(itemId);
	}

	/**
	 */
	@Override @NonNull public List<I> removeItems(final int positionStart, final int itemCount) {
		return dataSet.removeItems(positionStart, itemCount);
	}

	/**
	 */
	@Override @Nullable public List<I> getItems() {
		return dataSet.getItems();
	}

	/**
	 */
	@Override public int getItemCount() {
		return dataSet.getItemCount();
	}

	/**
	 */
	@Override public boolean hasItemAt(final int position) {
		return dataSet.hasItemAt(position);
	}

	/**
	 */
	@Override @NonNull public I getItem(final int position) {
		return dataSet.getItem(position);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}