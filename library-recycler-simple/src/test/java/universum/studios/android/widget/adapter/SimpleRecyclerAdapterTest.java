/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.testing.AndroidTestCase;
import universum.studios.android.widget.adapter.holder.RecyclerViewHolder;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SimpleRecyclerAdapterTest extends AndroidTestCase {

	@Test public void testInstantiationWithoutInitialItems() {
		// Act:
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext());
		// Assert:
		assertThat(adapter.getContext(), is(getContext()));
		assertThat(adapter.getItems(), is(nullValue()));
		assertThat(adapter.getHolderFactory(), is(nullValue()));
		assertThat(adapter.getHolderBinder(), is(nullValue()));
	}

	@Test public void testInstantiationWithInitialItems() {
		// Arrange:
		final List<String> items = new ArrayList<>(0);
		// Act:
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), items);
		// Assert:
		assertThat(adapter.getContext(), is(getContext()));
		assertThat(adapter.getItems(), is(items));
		assertThat(adapter.getHolderFactory(), is(nullValue()));
		assertThat(adapter.getHolderBinder(), is(nullValue()));
	}

	@Test public void testInstantiationWithInitialItemsFromArray() {
		// Act:
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), new String[0]);
		// Assert:
		assertThat(adapter.getContext(), is(getContext()));
		assertThat(adapter.getItems(), is(Arrays.asList(new String[0])));
		assertThat(adapter.getHolderFactory(), is(nullValue()));
		assertThat(adapter.getHolderBinder(), is(nullValue()));
	}

	@Test public void testInstantiationWithDataSet() {
		// Act:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Assert:
		assertThat(adapter.getContext(), is(getContext()));
		assertThat(adapter.getHolderFactory(), is(nullValue()));
		assertThat(adapter.getHolderBinder(), is(nullValue()));
		verify(mockDataSet).setItemsCallback(any(SimpleAdapterDataSet.ItemsCallback.class));
		verify(mockDataSet).setItemPositionResolver(any(ItemPositionResolver.class));
		verifyNoMoreInteractions(mockDataSet);
	}

	@Test public void testDataSetItemsCallbackOnItemsChanged() {
		// Arrange:
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext());
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		adapter.registerOnDataSetListener(mockListener);
		// Act:
		adapter.insertItem("Item");
		// Assert:
		verify(mockListener).onDataSetChanged();
		verifyNoMoreInteractions(mockListener);
	}

	@SuppressWarnings("unchecked")
	@Test public void testRegisterOnDataSetSwapListener() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final OnDataSetSwapListener mockListener = mock(OnDataSetSwapListener.class);
		final List<String> items = Collections.emptyList();
		// Act:
		adapter.registerOnDataSetSwapListener(mockListener);
		adapter.registerOnDataSetSwapListener(mockListener);
		// Assert:
		adapter.notifyItemsSwapFinished(items);
		verify(mockListener).onDataSetSwapFinished(items);
		verifyNoMoreInteractions(mockListener);
	}

	@SuppressWarnings("unchecked")
	@Test public void testUnregisterOnDataSetSwapListener() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final OnDataSetSwapListener mockListener = mock(OnDataSetSwapListener.class);
		final List<String> items = Collections.emptyList();
		adapter.registerOnDataSetSwapListener(mockListener);
		adapter.registerOnDataSetSwapListener(mockListener);
		// Act:
		adapter.unregisterOnDataSetSwapListener(mockListener);
		// Assert:
		adapter.notifyItemsSwapFinished(items);
		verifyNoInteractions(mockListener);
	}

	@SuppressWarnings("unchecked")
	@Test public void testUnregisterOnDataSetSwapListenerNotRegistered() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		adapter.unregisterOnDataSetSwapListener(mock(OnDataSetSwapListener.class));
	}

	@Test public void testChangeItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final List<String> items = Collections.emptyList();
		// Act:
		adapter.changeItems(items);
		// Assert:
		verify(mockDataSet).swapItems(items);
		assertThat(adapter.getItems(), is(items));
	}

	@SuppressWarnings("unchecked")
	@Test public void testSwapItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final OnDataSetSwapListener mockListener = mock(OnDataSetSwapListener.class);
		final List<String> items = Collections.emptyList();
		adapter.registerOnDataSetSwapListener(mockListener);
		when(mockDataSet.swapItems(items)).thenReturn(null);
		// Act + Assert:
		assertThat(adapter.swapItems(items), is(nullValue()));
		when(mockDataSet.getItems()).thenReturn(items);
		assertThat(adapter.getItems(), is(items));
		verify(mockDataSet).swapItems(items);
		verify(mockListener).onDataSetSwapStarted(items);
		verify(mockListener).onDataSetSwapFinished(items);
		verifyNoMoreInteractions(mockListener);
	}

	@SuppressWarnings("unchecked")
	@Test public void testSwapOldItemsForNewItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final OnDataSetSwapListener mockListener = mock(OnDataSetSwapListener.class);
		final List<String> oldItems = Collections.singletonList("Old Item");
		final List<String> newItems = Collections.singletonList("New Item");
		adapter.registerOnDataSetSwapListener(mockListener);
		when(mockDataSet.swapItems(newItems)).thenReturn(oldItems);
		// Act + Assert:
		assertThat(adapter.swapItems(newItems), is(oldItems));
		when(mockDataSet.getItems()).thenReturn(newItems);
		assertThat(adapter.getItems(), is(newItems));
		verify(mockDataSet).swapItems(newItems);
		verify(mockListener).onDataSetSwapStarted(newItems);
		verify(mockListener).onDataSetSwapFinished(newItems);
		verifyNoMoreInteractions(mockListener);
	}

	@SuppressWarnings("unchecked")
	@Test public void testNotifyItemsSwapStarted() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final OnDataSetSwapListener mockListener = mock(OnDataSetSwapListener.class);
		final List<String> items = Collections.emptyList();
		adapter.registerOnDataSetSwapListener(mockListener);
		// Act:
		adapter.notifyItemsSwapStarted(items);
		// Assert:
		verify(mockListener).onDataSetSwapStarted(items);
		verify(mockListener, times(0)).onDataSetSwapFinished(any());
	}

	@Test public void testNotifyItemsSwapStartedWithoutRegisteredListeners() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		adapter.notifyItemsSwapStarted(Collections.emptyList());
	}

	@Test public void testOnSwapItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		adapter.onSwapItems(Collections.emptyList());
		// Assert:
		verify(mockDataSet).swapItems(Collections.emptyList());
	}

	@SuppressWarnings("unchecked")
	@Test public void testNotifyItemsSwapFinished() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		final OnDataSetSwapListener mockListener = mock(OnDataSetSwapListener.class);
		final List<String> items = Collections.emptyList();
		adapter.registerOnDataSetSwapListener(mockListener);
		// Act:
		adapter.notifyItemsSwapFinished(items);
		// Assert:
		verify(mockListener).onDataSetSwapFinished(items);
		verify(mockListener, times(0)).onDataSetSwapStarted(any());
	}

	@Test public void testNotifyItemsSwapFinishedWithoutRegisteredListeners() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		adapter.notifyItemsSwapFinished(Collections.emptyList());
	}

	@Test public void testInsertItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.insertItem("Item at: " + i);
			verify(mockDataSet).insertItem("Item at: " + i);
		}
	}

	@Test public void testInsertItemAtPosition() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.insertItem(i, "Item at: " + i);
			verify(mockDataSet).insertItem(i, "Item at: " + i);
		}
	}

	@Test public void testInsertItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		for (int i = 0; i < 5; i++) {
			adapter.insertItems(Collections.emptyList());
		}
		// Assert:
		verify(mockDataSet, times(5)).insertItems(Collections.emptyList());
	}

	@Test public void testInsertItemsAtPosition() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 5; i++) {
			adapter.insertItems(i, Collections.emptyList());
			verify(mockDataSet).insertItems(i, Collections.emptyList());
		}
	}

	@Test public void testSwapItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.swapItem(i, "Item at: " + i);
			verify(mockDataSet).swapItem(i, "Item at: " + i);
		}
	}

	@Test public void testSwapItemById() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.swapItemById(i + 1L, "Item at: " + i);
			verify(mockDataSet).swapItemById(i + 1L, "Item at: " + i);
		}
	}

	@Test public void testMoveItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.moveItem(i, i + 1);
			verify(mockDataSet).moveItem(i, i + 1);
		}
	}

	@Test public void testRemoveItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.removeItem("Item at: " + i);
			verify(mockDataSet).removeItem("Item at: " + i);
		}
	}

	@Test public void testRemoveItemAtPosition() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.removeItem(i);
			verify(mockDataSet).removeItem(i);
		}
	}

	@Test public void testRemoveItemById() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.removeItemById(i + 1L);
			verify(mockDataSet).removeItemById(i + 1L);
		}
	}

	@Test public void testRemoveItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.removeItems(i, 1);
			verify(mockDataSet).removeItems(i, 1);
		}
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	@Test public void testGetItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		adapter.getItems();
		// Assert:
		verify(mockDataSet).getItems();
	}

	@Test public void testGetItemCount() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act:
		adapter.getItemCount();
		// Assert:
		verify(mockDataSet).getItemCount();
	}

	@Test public void testHasItemAt() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.hasItemAt(i);
			verify(mockDataSet).hasItemAt(i);
		}
	}

	@Test public void testGetItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> mockDataSet = mock(TestDataSet.class);
		final SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> adapter = new TestAdapter(getContext(), mockDataSet);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			adapter.getItem(i);
			verify(mockDataSet).getItem(i);
		}
	}

	private static class TestDataSet extends SimpleAdapterDataSet<String> {}

	private static class TestAdapter extends SimpleRecyclerAdapter<TestAdapter, RecyclerViewHolder, String> {

		TestAdapter(@NonNull final Context context) { super(context); }
		TestAdapter(@NonNull final Context context, @NonNull final String[] items) { super(context, items); }
		TestAdapter(@NonNull final Context context, @NonNull final List<String> items) { super(context, items); }
		TestAdapter(@NonNull final Context context, @NonNull final SimpleAdapterDataSet<String> dataSet) {
			super(context, dataSet);
		}

		@Override @NonNull public RecyclerViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
			return new RecyclerViewHolder(inflateView(android.R.layout.simple_list_item_1, parent));
		}

		@Override public void onBindViewHolder(@NonNull RecyclerViewHolder viewHolder, int position) {}
	}
}