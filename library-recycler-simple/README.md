Widget-Adapters-Recycler-Simple
===============

This module contains **simple** adapter implementation for `RecyclerView` widget. This adapter class
requires only the **creation** and **binding** logic of its corresponding item holders to be implemented.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-recycler-simple:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core),
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state),
[widget-adapters-recycler-base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SimpleRecyclerAdapter](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler-simple/src/main/java/universum/studios/android/widget/adapter/SimpleRecyclerAdapter.java)