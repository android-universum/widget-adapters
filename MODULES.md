Modules
===============

**!!! This feature is no longer available. !!!**

---

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/widget-adapters/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core)**
- **[Simple](https://bitbucket.org/android-universum/widget-adapters/src/main/library-simple)**
- **[Holder](https://bitbucket.org/android-universum/widget-adapters/src/main/library-holder)**
- **[State](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state)**
- **[@Recycler](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler_group)**
- **[Recycler-Base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler-base)**
- **[Recycler-Simple](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler-simple)**
- **[@List](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list_group)**
- **[List-Base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list-base)**
- **[List-Simple](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list-simple)**
- **[@Spinner](https://bitbucket.org/android-universum/widget-adapters/src/main/library-spinner_group)**
- **[Spinner-Base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-spinner-base)**
- **[Spinner-Simple](https://bitbucket.org/android-universum/widget-adapters/src/main/library-spinner-simple)**
- **[@Module](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module_group)**
- **[Module-Core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-core)**
- **[Module-Header](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-header)**
- **[Module-Selection](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-selection)**
- **[Wrapper](https://bitbucket.org/android-universum/widget-adapters/src/main/library-wrapper)**
