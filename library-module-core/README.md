Widget-Adapters-Module-Core
===============

This module contains core elements for **modules** that may be used within widget adapter implementations
to extend/decorate theirs provided functionality.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-module-core:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [AdapterModule](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-core/src/main/java/universum/studios/android/widget/adapter/module/AdapterModule.java)