/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.os.*;

import org.junit.*;

import androidx.annotation.*;
import universum.studios.android.testing.*;
import universum.studios.android.widget.adapter.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.mockito.Mockito.*;

/**
 * @author Martin Albedinsky
 */
public final class AdapterModuleTest extends AndroidTestCase {

	@Test public void initialInstance() {
		// Arrange:
		final AdapterModule module = new TestModule();

		// Assert:
		assertThat(module.isAdapterNotificationEnabled(), is(true));
		assertThat(module.requiresStateSaving(), is(false));
		assertThat(module.saveInstanceState(), is(AdapterSavedState.EMPTY_STATE));
	}

	@Test public void testAttachToAdapter() {
		// Arrange:
		final AdapterModule.ModuleAdapter mockAdapter = mock(AdapterModule.ModuleAdapter.class);
		final TestModule module = new TestModule();

		// Act:
		module.attachToAdapter(mockAdapter);

		// Assert:
		assertThat(module.attachedToAdapterCalledTimes, is(1));
	}

	@Test public void testSetAdapterNotificationEnabled() {
		// Arrange:
		final AdapterModule.ModuleAdapter mockAdapter = mock(AdapterModule.ModuleAdapter.class);
		final AdapterModule module = new TestModule();

		module.attachToAdapter(mockAdapter);

		// Act + Assert:

		// 1)
		module.setAdapterNotificationEnabled(false);
		assertThat(module.isAdapterNotificationEnabled(), is(false));

		// 2)
		module.setAdapterNotificationEnabled(true);
		assertThat(module.isAdapterNotificationEnabled(), is(true));
	}

	@Test public void testNotifyAdapter() {
		// Arrange:
		final AdapterModule.ModuleAdapter mockAdapter = mock(AdapterModule.ModuleAdapter.class);
		final AdapterModule module = new TestModule();

		module.attachToAdapter(mockAdapter);

		// Act:
		module.notifyAdapter();

		// Assert:
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testNotifyAdapterMultiple() {
		// Arrange:
		final AdapterModule.ModuleAdapter mockAdapter = mock(AdapterModule.ModuleAdapter.class);
		final AdapterModule module = new TestModule();

		module.attachToAdapter(mockAdapter);

		// Act:
		final int notificationCount = 5;
		for (int i = 0; i < notificationCount; i++) {
			module.notifyAdapter();
		}

		// Assert:
		verify(mockAdapter, times(notificationCount)).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testNotifyAdapterWithDisabledNotification() {
		// Arrange:
		final AdapterModule.ModuleAdapter mockAdapter = mock(AdapterModule.ModuleAdapter.class);
		final AdapterModule module = new TestModule();

		module.attachToAdapter(mockAdapter);
		module.setAdapterNotificationEnabled(false);

		// Act:
		module.notifyAdapter();

		// Assert:
		verifyNoInteractions(mockAdapter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testRestoreInstanceState() {
		// Arrange:
		// Only ensure that restoring of state for base class does not cause any troubles.
		final AdapterModule module = new TestModule();

		// Act:
		module.restoreInstanceState(Bundle.EMPTY);
		module.restoreInstanceState(null);
	}

	private static final class TestModule extends AdapterModule {

		int attachedToAdapterCalledTimes = 0;

		@Override
		protected void onAttachedToAdapter(@NonNull ModuleAdapter adapter) {
			attachedToAdapterCalledTimes++;
		}
	}
}