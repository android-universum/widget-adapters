Widget-Adapters-Module-Header
===============

This module contains `AdapterModule` implementations that may be used within a desired widget adapter
to provide along with the primary adapter's data set also data set of **headers** created from the
primary data set.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-module-header:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state),
[widget-adapters-module-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [HeadersModule](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-header/src/main/java/universum/studios/android/widget/adapter/module/HeadersModule.java)