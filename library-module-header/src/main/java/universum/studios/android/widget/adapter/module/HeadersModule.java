/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.util.SparseArray;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * An {@link AdapterModule} implementation that specifies API for modules that may provide additional
 * data set of headers for theirs associated adapter. To ensure that the associated adapter works
 * properly, it is necessary to call some of HeadersModule API methods from within such adapter as
 * it is shown below:
 * <pre>
 * public class SampleAdapter extends BaseListAdapter&lt;SampleAdapter, ViewHolder, String&gt; implements ModuleAdapter {
 *
 *      // Flag indicating item view type.
 *      private static final int VIEW_TYPE_ITEM = 0x00;
 *
 *      // Flag indicating header view type.
 *      private static final int VIEW_TYPE_HEADER = 0x01;
 *
 *      // Headers module holding the header items data set.
 *      private HeadersModule&lt;ViewHolder&gt; mHeadersModule;
 *
 *      // ...
 *
 *      public SampleAdapter(&#64;NonNull Context context) {
 *          super(context);
 *          this.mHeadersModule = new HeadersModule&lt;&gt;() { ... };
 *          this.mHeadersModule.attachToAdapter(this);
 *      }
 *
 *      &#64;NonNull
 *      public int getItemCount() {
 *          // Expand current data set size with also size of headers data set.
 *          return super.getItemCount() + mHeadersModule.size();
 *      }
 *
 *      &#64;NonNull
 *      public Item getItem(int position) {
 *          // This is necessary because this adapter does not hold the entire data set but only
 *          // its items, so without this correction this call can throw IndexOutOfBoundsException
 *          // for the positions above super.getItemCount() because of implementation of getItemCount().
 *          return super.getItem(mHeadersModule.correctPosition(position));
 *      }
 *
 *      &#64;Override
 *      public int getItemViewType(int position) {
 *          return mHeadersModule.isHeaderAt(position) ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
 *      }
 *
 *      &#64;NonNull
 *      protected ViewHolder onCreateViewHolder(&#64;NonNull ViewGroup parent, int viewType) {
 *          switch (viewType) {
 *              case VIEW_TYPE_HEADER:
 *                  return mHeadersModule.createViewHolder(parent, viewType);
 *              case VIEW_TYPE_ITEM:
 *              default:
 *                  return new ViewHolder(new TextView(parent.getContext()));
 *          }
 *      }
 *
 *      &#64;NonNull
 *      protected void onBindViewHolder(&#64;NonNull ViewHolder viewHolder, int position) {
 *          switch (viewHolder.getItemViewType()) {
 *              case VIEW_TYPE_HEADER:
 *                  mHeadersModule.bindViewHolder(viewHolder, position);
 *                  break;
 *              case VIEW_TYPE_ITEM:
 *              default:
 *                  ((TextView) viewHolder.itemView).setText(getItem(position));
 *                  break;
 *          }
 *      }
 *
 *      // ...
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <VH> Type of the view holder created by the module for its headers.
 * @param <H>  Type of the header items presented within data set of a subclass of this HeadersModule.
 */
public abstract class HeadersModule<VH, H extends HeadersModule.Header> extends AdapterModule {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "HeadersModule";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Required interface for header items of {@link HeadersModule}.
	 *
	 * @author Martin Albedinsky
	 */
	public interface Header {

		/**
		 * Returns the text to be displayed for this header instance.
		 *
		 * @return This header's text.
		 */
		@NonNull CharSequence getText();
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Array of headers managed by this module mapped to theirs positions.
	 */
	private final SparseArray<H> headers = new SparseArray<>(10);

	/**
	 * An attribute from the current theme, which should contain a resource of the style for the
	 * header view.
	 */
	private int headerStyleAttr = android.R.attr.textViewStyle;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Checks whether this module has some headers or not.
	 *
	 * @return {@code True} if this module does not have any headers, {@code false} otherwise.
	 *
	 * @see #size()
	 */
	public boolean isEmpty() {
		return headers.size() == 0;
	}

	/**
	 * Returns the count of headers within this module's data set.
	 *
	 * @return Headers count.
	 *
	 * @see #isEmpty()
	 */
	public int size() {
		return headers.size();
	}

	/**
	 * Puts the given <var>header</var> for the specified <var>position</var> into the data set of
	 * this module. If there is already header for the specified position, the current one will be
	 * replaced by the given one.
	 *
	 * @param header   The desired header to put.
	 * @param position The position for which should be the header mapped.
	 *
	 * @see #getHeader(int)
	 * @see #removeHeader(int)
	 */
	protected void putHeader(final int position, @NonNull final H header) {
		this.headers.append(position, header);
	}

	/**
	 * Checks whether there is a header at the specified <var>position</var> or not.
	 *
	 * @param position The position to check.
	 * @return {@code True} if there is a header item at the specified position, {@code false} otherwise.
	 *
	 * @see #getHeader(int)
	 */
	public boolean isHeaderAt(final int position) {
		return headers.get(position) != null;
	}

	/**
	 * Returns the header associated with the specified <var>position</var> from the current data
	 * set of this module.
	 *
	 * @param position Position of the desired header to obtain.
	 * @return Instance of the requested header at the specified position or {@code null} if there
	 * is no header item at the requested position.
	 *
	 * @see #getHeaders()
	 * @see #isEmpty()
	 */
	@Nullable public H getHeader(final int position) {
		return headers.get(position);
	}

	/**
	 * Returns the current headers data set of this module.
	 *
	 * @return Array of headers mapped to theirs positions.
	 *
	 * @see #getHeader(int)
	 * @see #isEmpty()
	 */
	@NonNull public SparseArray<H> getHeaders() {
		return headers;
	}

	/**
	 * Removes and returns a header at the specified position from the current data set of this module.
	 *
	 * @param position The position for which should be header removed.
	 * @return The removed header or {@code null} if there is no header mapped for the specified position.
	 *
	 * @see #putHeader(int, Header)
	 * @see #clearHeaders()
	 */
	@Nullable protected H removeHeader(final int position) {
		final H header = headers.get(position);
		this.headers.remove(position);
		return header;
	}

	/**
	 * Clears the current headers data set of this module.
	 */
	public void clearHeaders() {
		this.headers.clear();
	}

	/**
	 * Corrects the given <var>position</var> passed from the related adapter. Position will be
	 * corrected (decreased) by count of the headers counted by {@link #countHeadersBeforePosition(int)}.
	 * <p>
	 * This should be used within the associated adapter's {@link android.widget.Adapter#getItem(int) Adapter#getItem(int)}.
	 *
	 * @param position The position to correct.
	 * @return Corrected position which can be used in the associated adapter to properly/safely access
	 * items from its data set.
	 */
	public int correctPosition(final int position) {
		return position - countHeadersBeforePosition(position);
	}

	/**
	 * Counts headers that are present in the current data set before the specified <var>position</var>.
	 *
	 * @param position The position to which should be the headers counted.
	 * @return Count of headers before the specified position.
	 */
	public int countHeadersBeforePosition(final int position) {
		int count = 0;
		for (int i = 0; i < headers.size(); i++) {
			if (headers.keyAt(i) < position) {
				count++;
			} else {
				break;
			}
		}
		return count;
	}

	/**
	 * Sets an Xml attribute from the current theme, which contains a resource of style with attributes
	 * for header view which may be created along with its holder via {@link #createSimpleHeaderHolder(ViewGroup)}.
	 * <p>
	 * Default value: <b>{@link android.R.attr#textViewStyle}</b>
	 *
	 * @param styleAttr Xml style attribute.
	 *
	 * @see #getHeaderStyleAttr()
	 */
	public void setHeaderStyleAttr(@AttrRes final int styleAttr) {
		this.headerStyleAttr = styleAttr;
	}

	/**
	 * Returns the Xml style attribute specified via {@link #setHeaderStyleAttr(int)}.
	 *
	 * @return Xml style attribute.
	 *
	 * @see #setHeaderStyleAttr(int)
	 */
	@AttrRes public int getHeaderStyleAttr() {
		return headerStyleAttr;
	}

	/**
	 * Creates a new view holder along with its corresponding header view specific for this headers
	 * module.
	 *
	 * @param parent   A parent view, to resolve correct layout params in case when the header view
	 *                 will be inflated from an Xml layout.
	 * @param viewType Type of the header view to be created with the holder.
	 * @return New view holder with the header view of the requested type.
	 *
	 * @see #createSimpleHeaderHolder(ViewGroup)
	 */
	@NonNull public abstract VH createViewHolder(@NonNull ViewGroup parent, int viewType);

	/**
	 * Creates a new instance of SimpleHeaderHolder with single {@link TextView} as its item view
	 * with default style attribute specified via {@link #setHeaderStyleAttr(int)}.
	 *
	 * @param parent A parent view, to resolve correct layout params in case when the header view
	 *               will be inflated from an Xml layout.
	 * @return Instance of SimpleHeaderHolder ready to be used.
	 */
	@NonNull public final SimpleHeaderHolder createSimpleHeaderHolder(@NonNull ViewGroup parent) {
		return new SimpleHeaderHolder(new TextView(parent.getContext(), null, headerStyleAttr));
	}

	/**
	 * Binds the given <var>viewHolder</var> with data of header that is at the specified <var>position</var>.
	 *
	 * @param viewHolder The view holder created via {@link #createViewHolder(ViewGroup, int)} with
	 *                   its corresponding header view to be bound with data.
	 * @param position   Position of the header from the current data set of which data should be bound
	 *                   to the view holder.
	 *
	 * @see SimpleHeaderHolder#bind(Header)
	 */
	public abstract void bindViewHolder(@NonNull VH viewHolder, int position);

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Simple implementation of {@link Header} item for {@link HeadersModule}.
	 *
	 * @author Martin Albedinsky
	 */
	public static class SimpleHeader implements Header {

		/**
		 * Text value of this header.
		 */
		private final CharSequence text;

		/**
		 * Creates a new instance of SimpleHeader with the given text value.
		 *
		 * @param text Text value for header.
		 */
		public SimpleHeader(@NonNull final CharSequence text) {
			this.text = text;
		}

		/**
		 */
		@Override @NonNull public CharSequence getText() {
			return text;
		}

		/**
		 */
		@Override public String toString() {
			return text.toString();
		}
	}

	/**
	 * Simple holder for header view.
	 *
	 * @author Martin Albedinsky
	 */
	public static final class SimpleHeaderHolder {

		/**
		 * View presenting text of header.
		 */
		final TextView headerView;

		/**
		 * Creates a new instance of HeaderHolder for the given <var>headerView</var>.
		 *
		 * @param headerView The header view for which to create new holder.
		 */
		SimpleHeaderHolder(final TextView headerView) {
			this.headerView = headerView;
		}

		/**
		 * Binds view of this holder with text of the specified <var>header</var>.
		 *
		 * @param header The header of which text to bind to this holder's view.
		 *
		 * @see Header#getText()
		 */
		public void bind(@NonNull final Header header) {
			headerView.setText(header.getText());
		}
	}
}