/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.database.Cursor;
import android.text.TextUtils;

import java.util.Collection;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

/**
 * A {@link HeadersModule} implementation which may be used to provide set of alphabetic headers for
 * data set of a specific adapter to which is this module attached.
 * <p>
 * Alphabetic headers are generated whenever one of {@link #fromAlphabeticCollection(Collection)}
 * and {@link #fromAlphabeticCursor(Cursor)} methods is called.
 * <p>
 * <b>Note that this module assumes, that the data set to be processed is already ordered alphabetically.</b>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class AlphabeticHeaders<VH> extends HeadersModule<VH, HeadersModule.SimpleHeader> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "AlphabeticHeaders";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Required interface for item of which data set can be processed via {@link #fromAlphabeticCollection(Collection)}
	 * or {@link #fromAlphabeticCursor(Cursor)}.
	 *
	 * @author Martin Albedinsky
	 */
	public interface AlphabeticItem {

		/**
		 * Returns text of this alphabetic item.
		 *
		 * @return This item's text.
		 */
		@NonNull CharSequence getText();
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Letter that was lastly processed from the current alphabetic data set.
	 * <p>
	 * This is variable holds only temporary value used in {@link #onProcessItem(int, AlphabeticItem)}.
	 */
	private String lastProcessedLetter = "";

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Like {@link #fromAlphabeticCollection(Collection)} for the given <var>cursor</var> of which
	 * content will be iterated to obtain all the item names.
	 *
	 * @param cursor The desired cursor with content for alphabetic items to process.
	 * @param <C>    Type of the alphabetic cursor.
	 */
	public <C extends Cursor & AlphabeticItem> void fromAlphabeticCursor(@NonNull final C cursor) {
		clearHeaders();
		if (cursor.getCount() > 0 && cursor.moveToFirst()) {
			do {
				onProcessItem(cursor.getPosition(), cursor);
			} while (cursor.moveToNext());
		}
		notifyAdapter();
	}

	/**
	 * Processes the given <var>collection</var> of alphabetic items. Entire collection will be
	 * iterated and for each of its items will be checked the first char of name provided via
	 * {@link AlphabeticHeaders.AlphabeticItem#getText()}, so the created headers data set will
	 * contain all different first characters of the obtained item names.
	 * <p>
	 * Also, the adapter to which is this module attached will be notified about occurred data set
	 * change via {@link #notifyAdapter()}.
	 * <p>
	 * Note that items in the given collection should be already alphabetically sorted, otherwise
	 * the resulting headers data set may contain duplicates.
	 *
	 * @param collection The desired collection of alphabetic items to process.
	 * @param <Item>     Type of the alphabetic items in the given collection.
	 *
	 * @see #fromAlphabeticCursor(Cursor)
	 */
	public <Item extends AlphabeticItem> void fromAlphabeticCollection(@NonNull final Collection<Item> collection) {
		clearHeaders();
		if (!collection.isEmpty()) {
			int position = 0;
			for (final Item item : collection) {
				onProcessItem(position++, item);
			}
		}
		notifyAdapter();
	}

	/**
	 */
	@Override @CallSuper public void clearHeaders() {
		super.clearHeaders();
		this.lastProcessedLetter = "";
	}

	/**
	 * Processes the given alphabetic <var>item</var> and creates a new header for it.
	 *
	 * @param position The position of the given item in the associated adapter's data set.
	 * @param item     Alphabetic item to be processed.
	 */
	protected final void onProcessItem(final int position, @NonNull final AlphabeticItem item) {
		final String name = item.getText().toString();
		if (!TextUtils.isEmpty(name)) {
			final String firstLetter = name.substring(0, 1);
			if (!firstLetter.equals(lastProcessedLetter)) {
				putHeader(size() + position, new SimpleHeader(firstLetter));
			}
			this.lastProcessedLetter = firstLetter;
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}