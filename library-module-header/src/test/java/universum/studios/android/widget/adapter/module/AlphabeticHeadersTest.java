/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.view.ViewGroup;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class AlphabeticHeadersTest extends AndroidTestCase {

	private static HeadersModule.SimpleHeader createTestHeader() {
		return new HeadersModule.SimpleHeader("Test Header");
	}

	private static TestMatrixCursor createCursor() {
		final List<AlphabeticHeaders.AlphabeticItem> items = createItems();
		final TestMatrixCursor cursor = new TestMatrixCursor();
		for (AlphabeticHeaders.AlphabeticItem item : items) {
			cursor.addRowWithName(item.getText().toString());
		}
		return cursor;
	}

	private static List<AlphabeticHeaders.AlphabeticItem> createItems() {
		// There are 10 unique entries in the items collection.
		final List<AlphabeticHeaders.AlphabeticItem> items = new ArrayList<>();
		items.add(createItem("Alpha"));
		items.add(createItem("Alpha"));
		items.add(createItem("Beta"));
		items.add(createItem("Gamma"));
		items.add(createItem("Delta"));
		items.add(createItem("Delta"));
		items.add(createItem("Delta"));
		items.add(createItem("Epsilon"));
		items.add(createItem("Zeta"));
		items.add(createItem("Eta"));
		items.add(createItem("Eta"));
		items.add(createItem("Theta"));
		items.add(createItem("Iota"));
		items.add(createItem("Iota"));
		items.add(createItem("Iota"));
		items.add(createItem("Iota"));
		items.add(createItem("Iota"));
		items.add(createItem("Kappa"));
		items.add(createItem("Lamda"));
		Collections.sort(items, (first, second) -> first.getText().toString().compareTo(second.getText().toString()));
		return items;
	}

	private static AlphabeticHeaders.AlphabeticItem createItem(final String text) {
		return () -> text;
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testFromAlphabeticCursor() {
		// Arrange:
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		// Act:
		module.fromAlphabeticCursor(createCursor());
		// Assert:
		// There are 10 unique entries in the items cursor.
		assertThat(module.size(), is(10));
		final List<String> headerTexts = new ArrayList<>(module.size());
		final List<AlphabeticHeaders.AlphabeticItem> items = createItems();
		for (int i = 0; i < items.size() + module.size(); i++) {
			if (module.isHeaderAt(i)) {
				final HeadersModule.Header header = module.getHeader(i);
				final String headerText = header.toString();
				assertThat(headerTexts.contains(headerText), is(false));
				headerTexts.add(headerText);
			}
		}
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testFromAlphabeticCursorWithPreviousHeaders() {
		// Arrange:
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		for (int i = 0; i < 20; i++) {
			module.putHeader(i, createTestHeader());
		}
		// Act:
		module.fromAlphabeticCursor(createCursor());
		// Assert:
		// There are 10 unique entries in the items cursor.
		assertThat(module.size(), is(10));
		final List<String> headerTexts = new ArrayList<>(module.size());
		final List<AlphabeticHeaders.AlphabeticItem> items = createItems();
		for (int i = 0; i < items.size() + module.size(); i++) {
			if (module.isHeaderAt(i)) {
				final HeadersModule.Header header = module.getHeader(i);
				final String headerText = header.toString();
				assertThat(headerTexts.contains(headerText), is(false));
				headerTexts.add(headerText);
			}
		}
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testFromAlphabeticCursorWithoutData() {
		// Arrange:
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		// Act:
		module.fromAlphabeticCursor(new TestMatrixCursor());
		// Assert:
		assertThat(module.size(), is(0));
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testFromAlphabeticCursorThatCannotBeMovedToFirstPosition() {
		// Arrange:
		final TestCursor mockCursor = mock(TestCursor.class);
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		when(mockCursor.getCount()).thenReturn(10);
		when(mockCursor.moveToFirst()).thenReturn(false);
		// Act:
		module.fromAlphabeticCursor(mockCursor);
		// Assert:
		assertThat(module.size(), is(0));
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testFromAlphabeticCollection() {
		// Arrange:
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		final List<AlphabeticHeaders.AlphabeticItem> items = createItems();
		// Act:
		module.fromAlphabeticCollection(items);
		// Assert:
		// There are 10 unique entries in the items collection.
		assertThat(module.size(), is(10));
		final List<String> headerTexts = new ArrayList<>(module.size());
		for (int i = 0; i < items.size() + module.size(); i++) {
			if (module.isHeaderAt(i)) {
				final HeadersModule.Header header = module.getHeader(i);
				final String headerText = header.toString();
				assertThat(headerTexts.contains(headerText), is(false));
				headerTexts.add(headerText);
			}
		}
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testFromAlphabeticCollectionWithPreviousHeaders() {
		// Arrange:
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		for (int i = 0; i < 20; i++) {
			module.putHeader(i, createTestHeader());
		}
		final List<AlphabeticHeaders.AlphabeticItem> items = createItems();
		// Act:
		module.fromAlphabeticCollection(items);
		// Assert:
		// There are 10 unique entries in the items collection.
		assertThat(module.size(), is(10));
		final List<String> headerTexts = new ArrayList<>(module.size());
		for (int i = 0; i < items.size() + module.size(); i++) {
			if (module.isHeaderAt(i)) {
				final HeadersModule.Header header = module.getHeader(i);
				final String headerText = header.toString();
				assertThat(headerTexts.contains(headerText), is(false));
				headerTexts.add(headerText);
			}
		}
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testFromAlphabeticCollectionWithoutData() {
		// Arrange:
		final AlphabeticHeaders.ModuleAdapter mockAdapter = mock(AlphabeticHeaders.ModuleAdapter.class);
		final Module module = new Module();
		module.attachToAdapter(mockAdapter);
		final Collection<AlphabeticHeaders.AlphabeticItem> items = new ArrayList<>(0);
		// Act:
		module.fromAlphabeticCollection(items);
		// Assert:
		assertThat(module.size(), is(0));
		verify(mockAdapter).notifyDataSetChanged();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testClearHeaders() {
		// Arrange:
		final Module module = new Module();
		module.putHeader(0, new HeadersModule.SimpleHeader("Test Header"));
		// Act:
		module.clearHeaders();
		// Assert:
		assertThat(module.size(), is(0));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testOnProcessItem() {
		// Arrange:
		final Module module = new Module();
		// Act + Assert:
		module.onProcessItem(0, createItem("Alpha"));
		assertThat(module.size(), is(1));
		assertThat(module.getHeader(0).getText(), is("A"));
		module.onProcessItem(1, createItem("Alpha"));
		assertThat(module.size(), is(1));
		module.onProcessItem(2, createItem("Beta"));
		assertThat(module.size(), is(2));
		assertThat(module.getHeader(0).getText(), is("A"));
		assertThat(module.getHeader(3).getText(), is("B"));
	}

	@Test public void testOnProcessItemWithEmptyText() {
		// Arrange:
		final Module module = new Module();
		// Act:
		module.onProcessItem(0, createItem(""));
		// Assert:
		assertThat(module.size(), is(0));
	}

	private static final class Module extends AlphabeticHeaders<AlphabeticHeaders.SimpleHeaderHolder> {

		@Override @NonNull public SimpleHeaderHolder createViewHolder(@NonNull final ViewGroup parent, final int viewType) {
			return createSimpleHeaderHolder(parent);
		}

		@SuppressWarnings("ConstantConditions")
		@Override public void bindViewHolder(@NonNull final SimpleHeaderHolder viewHolder, final int position) {
			viewHolder.bind(getHeader(position));
		}
	}

	private interface TestCursor extends Cursor, AlphabeticHeaders.AlphabeticItem {}

	private static class TestMatrixCursor extends MatrixCursor implements AlphabeticHeaders.AlphabeticItem {

		static final String COLUMN_NAME = "name";

		TestMatrixCursor() {
			this(new String[]{COLUMN_NAME});
		}

		TestMatrixCursor(final String[] columnNames) {
			super(columnNames);
		}

		TestMatrixCursor addRowWithName(@NonNull final String name) {
			addRow(new Object[]{name});
			return this;
		}

		@Override @NonNull public CharSequence getText() {
			return getString(getColumnIndex(COLUMN_NAME));
		}
	}
}