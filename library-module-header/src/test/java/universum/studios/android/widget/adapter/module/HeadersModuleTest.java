/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.util.SparseArray;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.junit.Test;

import androidx.annotation.NonNull;
import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class HeadersModuleTest extends AndroidTestCase {

	private static HeadersModule.SimpleHeader createTestHeader() {
		return createTestHeader("Test Header");
	}

	private static HeadersModule.SimpleHeader createTestHeader(final String text) {
		return new HeadersModule.SimpleHeader(text);
	}

	@Test public void testInstantiation() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act + Assert:
		assertThat(module.getHeaderStyleAttr(), is(android.R.attr.textViewStyle));
		assertThat(module.isEmpty(), is(true));
		assertThat(module.size(), is(0));
		final SparseArray<HeadersModule.SimpleHeader> headers = module.getHeaders();
		assertThat(headers, is(notNullValue()));
		assertThat(headers.size(), is(0));
	}

	@Test public void testIsEmpty() {
		// Arrange:
		final TestModule module = new TestModule();
		module.putHeader(0, createTestHeader());
		// Act + Assert:
		assertThat(module.isEmpty(), is(false));
	}

	@Test public void testSize() {
		// Arrange:
		final TestModule module = new TestModule();
		final int headersCount = 10;
		for (int i = 0; i < headersCount; i++) {
			module.putHeader(i, createTestHeader("Header for: " + i));
		}
		// Act + Assert:
		assertThat(module.size(), is(headersCount));
	}

	@Test public void testPutHeader() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act:
		final int headersCount = 10;
		for (int i = 0; i < headersCount; i++) {
			module.putHeader(i, createTestHeader("Header for: " + i));
		}
		// Assert:
		assertThat(module.size(), is(headersCount));
	}

	@Test public void testPutHeaderSame() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act:
		final int headersCount = 10;
		for (int i = 0; i < headersCount; i++) {
			module.putHeader(i, createTestHeader("Header for: " + i));
			module.putHeader(i, createTestHeader("Header for: " + i));
		}
		// Assert:
		assertThat(module.size(), is(headersCount));
	}

	@Test public void testIsHeaderAt() {
		// Arrange:
		final TestModule module = new TestModule();
		final int itemsCount = 10;
		for (int i = 0; i < itemsCount; i++) {
			if (i % 2 == 0) module.putHeader(i, createTestHeader());
		}
		// Act + Assert:
		for (int i = 0; i < itemsCount; i++) {
			assertThat(module.isHeaderAt(i), is(i % 2 == 0));
		}
	}

	@Test public void testIsHeaderAtOnEmptyHeaders() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(module.isHeaderAt(i), is(false));
		}
	}

	@Test public void testGetHeader() {
		// Arrange:
		final TestModule module = new TestModule();
		final int headersCount = 10;
		for (int i = 0; i < headersCount; i++) {
			module.putHeader(i, createTestHeader("Header for: " + i));
		}
		for (int i = 0; i < headersCount; i++) {
			// Act:
			final HeadersModule.Header header = module.getHeader(i);
			// Assert:
			assertThat(header, is(not(nullValue())));
			assertThat(header.getText(), is("Header for: " + i));
		}
	}

	@Test public void testGetHeaderOnEmptyHeaders() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(module.getHeader(i), is(nullValue()));
		}
	}

	@Test public void testGetHeaders() {
		// Arrange:
		final TestModule module = new TestModule();
		final int headersCount = 10;
		for (int i = 0; i < headersCount; i++) {
			module.putHeader(i, createTestHeader("Header for: " + i));
		}
		// Act:
		final SparseArray<HeadersModule.SimpleHeader> headers = module.getHeaders();
		// Assert:
		assertThat(headers, is(not(nullValue())));
		assertThat(headers.size(), is(headersCount));
		for (int i = 0; i < headersCount; i++) {
			final HeadersModule.Header header = headers.get(i);
			assertThat(header, is(not(nullValue())));
			assertThat(header.getText(), is("Header for: " + i));
		}
	}

	@Test public void testRemoveHeader() {
		// Arrange:
		final TestModule module = new TestModule();
		for (int i = 0; i < 10; i++) {
			module.putHeader(i, createTestHeader("Header for: " + i));
		}
		for (int i = 9; i >= 0; i--) {
			// Act:
			final HeadersModule.Header header = module.removeHeader(i);
			// Assert:
			assertThat(header, is(notNullValue()));
			assertThat(header.getText(), is("Header for: " + i));
			assertThat(module.getHeader(i), is(nullValue()));
		}
		assertThat(module.isEmpty(), is(true));
		assertThat(module.size(), is(0));
	}

	@Test public void testRemoveHeaderOnEmptyHeaders() {
		// Arrange:
		// Only ensure that removing of not existing header does not cause any troubles.
		final TestModule module = new TestModule();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(module.removeHeader(i), is(nullValue()));
		}
	}

	@Test public void testClearHeaders() {
		// Arrange:
		final TestModule module = new TestModule();
		for (int i = 0; i < 10; i++) {
			module.putHeader(i, createTestHeader());
		}
		// Act:
		module.clearHeaders();
		// Assert:
		assertThat(module.isEmpty(), is(true));
		assertThat(module.size(), is(0));
	}

	@Test public void testCorrectPosition() {
		// Arrange:
		final TestModule module = new TestModule();
		final int headersCount = 5;
		final int itemsCount = 10;
		final int dataSetSize = itemsCount + headersCount;
		for (int i = 0; i < dataSetSize; i++) {
			if (i % 2 == 0) module.putHeader(i, createTestHeader());
		}
		// Act + Assert:
		for (int i = 0; i < dataSetSize; i++) {
			assertThat(module.correctPosition(i), is(i - (i / 2 + (i % 2 == 1 ? 1 : 0))));
		}
	}

	@Test public void testCorrectPositionOnEmptyHeaders() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(module.correctPosition(i), is(i));
		}
	}

	@Test public void testCountHeadersBeforePosition() {
		// Arrange:
		final TestModule module = new TestModule();
		final int dataSetSize = 10;
		for (int i = 0; i < dataSetSize; i++) {
			if (i % 2 == 0) module.putHeader(i, createTestHeader());
		}
		// Act + Assert:
		for (int i = 0; i < dataSetSize; i++) {
			assertThat(module.countHeadersBeforePosition(i), is(i / 2 + (i % 2 == 1 ? 1 : 0)));
		}
	}

	@Test public void testCountHeadersBeforePositionOnEmptyHeaders() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(module.countHeadersBeforePosition(i), is(0));
		}
	}

	@Test public void testHeaderStyleAttr() {
		// Arrange:
		final TestModule module = new TestModule();
		// Act:
		module.setHeaderStyleAttr(android.R.attr.buttonStyle);
		// Assert:
		assertThat(module.getHeaderStyleAttr(), is(android.R.attr.buttonStyle));
	}

	@Test public void testCreateSimpleHeaderHolder() {
		// Arrange:
		final HeadersModule module = new TestModule();
		// Act: + Assert:
		assertThat(module.createSimpleHeaderHolder(new FrameLayout(getContext())), is(not(nullValue())));
	}

	@Test public void testSimpleHeaderInstantiation() {
		// Arrange:
		final HeadersModule.Header header = createTestHeader();
		// Act + Assert:
		assertThat(header.getText(), is("Test Header"));
	}

	@Test public void testSimpleHeaderToString() {
		// Arrange:
		final HeadersModule.Header header = createTestHeader();
		// Act + Assert:
		assertThat(header.toString(), is("Test Header"));
	}

	@Test public void testSimpleHeaderHolderInstantiation() {
		// Act:
		final HeadersModule.SimpleHeaderHolder holder = new HeadersModule.SimpleHeaderHolder(new TextView(getContext()));
		// Assert:
		assertThat(holder.headerView, is(notNullValue()));
	}

	@Test public void testSimpleHeaderHolderBind() {
		// Arrange:
		final TextView textView = new TextView(getContext());
		final HeadersModule.SimpleHeaderHolder holder = new HeadersModule.SimpleHeaderHolder(textView);
		// Act:
		holder.bind(createTestHeader());
		// Assert:
		assertThat(textView.getText(), is(createTestHeader().getText()));
	}

	private static final class TestModule extends HeadersModule<HeadersModule.SimpleHeaderHolder, HeadersModule.SimpleHeader> {

		@Override @NonNull public SimpleHeaderHolder createViewHolder(@NonNull final ViewGroup parent, final int viewType) {
			return createSimpleHeaderHolder(parent);
		}

		@SuppressWarnings("ConstantConditions")
		@Override public void bindViewHolder(@NonNull final SimpleHeaderHolder viewHolder, final int position) {
			viewHolder.bind(getHeader(position));
		}
	}
}