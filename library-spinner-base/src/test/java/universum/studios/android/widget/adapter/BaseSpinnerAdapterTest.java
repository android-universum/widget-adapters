/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import androidx.annotation.NonNull;
import universum.studios.android.testing.AndroidTestCase;
import universum.studios.android.widget.adapter.holder.AdapterHolder;
import universum.studios.android.widget.adapter.holder.ViewHolder;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class BaseSpinnerAdapterTest extends AndroidTestCase {

	private static final AdapterHolder.Factory<ViewHolder> HOLDER_FACTORY = (parent, viewType) ->
			new ViewHolder(new TextView(parent.getContext()));

	private static final AdapterHolder.Binder<TestAdapter, ViewHolder> HOLDER_BINDER = (adapter, holder, position, payloads) ->
			((TextView) holder.itemView).setText(adapter.getItem(position));

	private ViewGroup container;

	@Before public void beforeTest() {
		this.container = new FrameLayout(getContext());
	}

	@After public void afterTest() {
		this.container = null;
	}

	@Test public void testInstantiation() {
		// Act:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Assert:
		assertThat(adapter.getHolderFactory(), is(nullValue()));
		assertThat(adapter.getHolderBinder(), is(nullValue()));
	}

	@Test public void testDropDownHolderFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		adapter.setDropDownHolderFactory(HOLDER_FACTORY);
		assertThat(adapter.getDropDownHolderFactory(), is(HOLDER_FACTORY));
	}

	@Test public void testDropDownHolderBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		adapter.setDropDownHolderBinder(HOLDER_BINDER);
		assertThat(adapter.getDropDownHolderBinder(), is(HOLDER_BINDER));
	}

	@Test public void testGetSelectedItem() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderFactory(HOLDER_FACTORY);
		adapter.setHolderBinder(HOLDER_BINDER);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			adapter.getView(i, null, container);
			assertThat(adapter.getSelectedItem(), is(adapter.getItem(i)));
		}
	}

	@Test  public void testGetSelectedItemDefault() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.getSelectedItem(), is(nullValue()));
	}

	@Test public void testGetSelectedPosition() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderFactory(HOLDER_FACTORY);
		adapter.setHolderBinder(HOLDER_BINDER);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			adapter.getView(i, null, container);
			assertThat(adapter.getSelectedPosition(), is(i));
		}
	}

	@Test public void testGetSelectedPositionDefault() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.getSelectedPosition(), is(TestAdapter.NO_POSITION));
	}

	@Test public void testGetDropDownViewWithoutConvertView() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setDropDownHolderFactory(HOLDER_FACTORY);
		adapter.setDropDownHolderBinder(HOLDER_BINDER);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			final View view = adapter.getDropDownView(i, null, container);
			assertThat(view, is(not(nullValue())));
			assertThat(view, instanceOf(TextView.class));
			assertThat(((TextView) view).getText().toString(), is("Item at: " + i));
			final Object tag = view.getTag();
			assertThat(tag, instanceOf(ViewHolder.class));
			assertThat(((ViewHolder) tag).getBindingAdapterPosition(), is(i));
		}
	}

	@Test public void testGetDropDownViewWithConvertView() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setDropDownHolderFactory(HOLDER_FACTORY);
		adapter.setDropDownHolderBinder(HOLDER_BINDER);
		final View convertView = adapter.getDropDownView(0, null, container);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			final View view = adapter.getDropDownView(i, convertView, container);
			assertThat(view, is(convertView));
			assertThat(((TextView) view).getText().toString(), is("Item at: " + i));
			final Object tag = view.getTag();
			assertThat(tag, instanceOf(ViewHolder.class));
			assertThat(((ViewHolder) tag).getBindingAdapterPosition(), is(i));
		}
	}

	@Test public void testOnCreateDropDownViewHolderWithDropDownHolderFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setDropDownHolderFactory(HOLDER_FACTORY);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			final ViewHolder viewHolder = adapter.onCreateDropDownViewHolder(container, i);
			assertThat(viewHolder, is(not(nullValue())));
			assertThat(viewHolder.itemView, instanceOf(TextView.class));
		}
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testOnCreateDropDownViewHolderWithoutDropDownHolderFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.onCreateDropDownViewHolder(container, 0);
	}

	@Test public void testOnCreateDropDownViewHolderWithHolderFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderFactory(HOLDER_FACTORY);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			final ViewHolder viewHolder = adapter.onCreateDropDownViewHolder(container, i);
			assertThat(viewHolder, is(not(nullValue())));
			assertThat(viewHolder.itemView, instanceOf(TextView.class));
		}
	}

	@Test public void testOnBindDropDownViewHolderWithDropDownHolderBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setDropDownHolderBinder(HOLDER_BINDER);
		final TextView itemView = new TextView(getContext());
		final ViewHolder viewHolder = new ViewHolder(itemView);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			adapter.onBindDropDownViewHolder(viewHolder, i);
			assertThat(itemView.getText().toString(), is("Item at: " + i));
		}
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testOnBindDropDownViewHolderWithoutDropDownHolderBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.onBindDropDownViewHolder(new ViewHolder(new View(getContext())), 0);
	}

	@Test public void testOnBindDropDownViewHolderWithHolderBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderBinder(HOLDER_BINDER);
		final TextView itemView = new TextView(getContext());
		final ViewHolder viewHolder = new ViewHolder(itemView);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			adapter.onBindDropDownViewHolder(viewHolder, i);
			assertThat(itemView.getText().toString(), is("Item at: " + i));
		}
	}

	private static final class TestAdapter extends BaseSpinnerAdapter<TestAdapter, ViewHolder, ViewHolder, String> {

		TestAdapter(@NonNull final Context context) { super(context); }

		@Override public int getItemCount() { return 10; }

		@Override @NonNull public String getItem(final int position) { return "Item at: " + position; }
	}
}