/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.widget.adapter.holder.AdapterHolder;
import universum.studios.android.widget.adapter.holder.ViewHolder;

/**
 * A {@link BaseListAdapter} implementation that may be used to provide optimized adapter for
 * {@link android.widget.Spinner Spinner} widget. This "spinner based" adapter also supports API to
 * store current selected item and its corresponding position. The position of the selected item is
 * stored whenever {@link #getView(int, View, ViewGroup)} is called. The selected item position may
 * be obtained via {@link #getSelectedPosition()}.
 *
 * <h3>Implementation</h3>
 * Inheritance hierarchies of this adapter class are required to implement following methods:
 * <ul>
 * <li>{@link #getItemCount()}</li>
 * <li>{@link #getItem(int)}</li>
 * <li>{@link #onCreateViewHolder(ViewGroup, int)}</li>
 * <li>{@link #onBindViewHolder(ViewHolder, int)}</li>
 * </ul>
 * See also configuration related features available for this adapter class which may possibly reduce
 * implementation requirements.
 *
 * <h3>Drop Down Holder Factory</h3>
 * A desired implementation of {@link AdapterHolder.Factory} for drop down holders may be attached
 * to {@link BaseSpinnerAdapter} via {@link #setDropDownHolderFactory(AdapterHolder.Factory)}. Such
 * factory will be then used by the adapter to create drop down view holders whenever
 * {@link #onCreateDropDownViewHolder(ViewGroup, int)} is invoked for the adapter.
 *
 * <h3>Drop Down Holder Binder</h3>
 * A desired implementation of {@link AdapterHolder.Binder} for drop down holders may be attached to
 * {@link BaseSpinnerAdapter} via {@link #setDropDownHolderBinder(AdapterHolder.Binder)}. Such binder
 * will be then used by the adapter to perform binding logic for its drop down view holders whenever
 * {@link #onBindDropDownViewHolder(ViewHolder, int)} is invoked for the adapter.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <A>   Type of the subclass of BaseSpinnerAdapter.
 * @param <VH>  Type of the view holder used by the adapter.
 * @param <DVH> Type of the drop down view holder used by the adapter.
 * @param <I>   Type of items presented within the adapter's data set.
 */
public abstract class BaseSpinnerAdapter<A extends BaseSpinnerAdapter, VH extends ViewHolder, DVH extends ViewHolder, I>
		extends BaseListAdapter<A, VH, I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseSpinnerAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Currently selected position.
	 */
	private int selectedPosition = NO_POSITION;

	/**
	 * Factory that is used by this adapter to create drop down view holders.
	 *
	 * @see #onCreateDropDownViewHolder(ViewGroup, int)
	 */
	private AdapterHolder.Factory<DVH> dropDownHolderFactory;

	/**
	 * Binder that is used by this adapter to bind its drop down view holders with data.
	 *
	 * @see #onBindDropDownViewHolder(ViewHolder, int)
	 */
	private AdapterHolder.Binder<A, DVH> dropDownHolderBinder;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseSpinnerAdapter within the given <var>context</var>.
	 *
	 * @param context Context in which will be this adapter used.
	 */
	public BaseSpinnerAdapter(@NonNull final Context context) {
		super(context);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a factory that should be used by this adapter to create holders for its drop down views.
	 *
	 * @param factory The desired factory. May be {@code null} to indicate that it is the adapter's
	 *                responsibility to handle creation of holders.
	 * @see AdapterHolder.Factory#createHolder(ViewGroup, int)
	 *
	 * @see #getDropDownHolderFactory()
	 */
	public void setDropDownHolderFactory(@Nullable final AdapterHolder.Factory<DVH> factory) {
		this.dropDownHolderFactory = factory;
	}

	/**
	 * Returns the drop down holder factory associated with this adapter.
	 *
	 * @return Factory used by this adapter to create drop down holder instances or {@code null} if
	 * no factory has been specified.
	 *
	 * @see #setDropDownHolderFactory(AdapterHolder.Factory)
	 */
	@Nullable public AdapterHolder.Factory<DVH> getDropDownHolderFactory() {
		return dropDownHolderFactory;
	}

	/**
	 * Sets a binder that should be used by this adapter to perform binding of data from its
	 * data set to its corresponding drop down holders.
	 *
	 * @param binder The desired binder. May be {@code null} to indicate that it is the adapter's
	 *               responsibility to handle binding logic for holders.
	 * @see AdapterHolder.Binder#bindHolder(Object, AdapterHolder, int, java.util.List)
	 *
	 * @see #getDropDownHolderBinder()
	 */
	public void setDropDownHolderBinder(@Nullable final AdapterHolder.Binder<A, DVH> binder) {
		this.dropDownHolderBinder = binder;
	}

	/**
	 * Returns the drop down holder binder associated with this adapter.
	 *
	 * @return Binder used by this adapter to perform binding of data to drop down holders or {@code null}
	 * if no binder has been specified.
	 *
	 * @see #setDropDownHolderBinder(AdapterHolder.Binder)
	 */
	@Nullable public AdapterHolder.Binder<A, DVH> getDropDownHolderBinder() {
		return dropDownHolderBinder;
	}

	/**
	 * Returns the item model with data of this adapter for the current selected position.
	 *
	 * @return Item obtained via {@link #getItem(int)} for the current selected position or
	 * {@code null} if the selected position is unspecified at the time.
	 *
	 * @see #getSelectedPosition()
	 */
	@Nullable public I getSelectedItem() {
		return selectedPosition == NO_POSITION ? null : getItem(selectedPosition);
	}

	/**
	 * Returns the position of the currently selected item.
	 *
	 * @return Position of item that is currently selected within a related Spinner widget or
	 * {@link #NO_POSITION} if this adapter does not have its data set specified or it is not
	 * attached to its associated Spinner widget.
	 *
	 * @see #getSelectedItem()
	 */
	public int getSelectedPosition() {
		return selectedPosition;
	}

	/**
	 */
	@Override public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
		return super.getView(selectedPosition = position, convertView, parent);
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override public View getDropDownView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
		View view = convertView;
		DVH viewHolder;
		if (view == null) {
			viewHolder = onCreateDropDownViewHolder(parent, getItemViewType(position));
			view = viewHolder.itemView;
			view.setTag(viewHolder);
		} else {
			viewHolder = (DVH) view.getTag();
		}
		// Ensure that the view holder has always the actual adapter position specified.
		viewHolder.updateBindingAdapterPosition(position);
		onBindDropDownViewHolder(viewHolder, position);
		return view;
	}

	/**
	 * Invoked from {@link #getDropDownView(int, View, ViewGroup)} in order to create a view holder
	 * along with its corresponding drop down item view for the specified <var>viewType</var>.
	 * <p>
	 * Default implementation of this method uses {@link AdapterHolder.Factory} attached to this
	 * adapter via {@link #setDropDownHolderFactory(AdapterHolder.Factory)}. If such factory is
	 * available it will be used to create the requested drop down view holder otherwise the default
	 * implementation assumes that both, ViewHolder and DropDownViewHolder, are of same type so it
	 * simply delegates this request to {@link #onCreateViewHolder(ViewGroup, int)}.
	 *
	 * @param parent   A parent view, to resolve correct layout params in case when the drop down
	 *                 item view will be inflated from an Xml layout.
	 * @param viewType Type of the drop down item view to be created with the holder. This is the
	 *                 same identifier as obtained via {@link #getItemViewType(int)} for the position
	 *                 passed to {@link #getDropDownView(int, View, ViewGroup)} method.
	 * @return New view holder with the drop down item view of the requested type.
	 */
	@SuppressWarnings("unchecked")
	@NonNull protected DVH onCreateDropDownViewHolder(@NonNull final ViewGroup parent, final int viewType) {
		return dropDownHolderFactory == null ?
				(DVH) onCreateViewHolder(parent, viewType) :
				dropDownHolderFactory.createHolder(parent, viewType);
	}

	/**
	 * Invoked from {@link #getDropDownView(int, View, ViewGroup)} in order to perform binding of the
	 * given drop down <var>viewHolder</var> with data of the item from this adapter's data set at
	 * the specified <var>position</var>.
	 * <p>
	 * Default implementation of this method uses {@link AdapterHolder.Binder} attached to this
	 * adapter via {@link #setDropDownHolderBinder(AdapterHolder.Binder)}. If such  binder is available
	 * it will be used to perform binding logic otherwise the default implementation assumes that both,
	 * ViewHolder and DropDownViewHolder, are of same type so it simply delegates this request to
	 * {@link #onBindViewHolder(ViewHolder, int)}.
	 *
	 * @param viewHolder The view holder created via {@link #onCreateDropDownViewHolder(ViewGroup, int)}
	 *                   with its corresponding drop down item view to be bound with data.
	 * @param position   Position of the item from the current data set of which data should be bound
	 *                   to the view holder.
	 */
	@SuppressWarnings("unchecked")
	protected void onBindDropDownViewHolder(@NonNull final DVH viewHolder, final int position) {
		if (dropDownHolderBinder == null) {
			onBindViewHolder((VH) viewHolder, position);
		} else {
			this.dropDownHolderBinder.bindHolder((A) this, viewHolder, position, Collections.EMPTY_LIST);
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}