@Widget-Adapters-Recycler
===============

This module groups the following modules into one **single group**:

- [Recycler-Base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler-base)
- [Recycler-Simple](https://bitbucket.org/android-universum/widget-adapters/src/main/library-recycler-simple)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-recycler:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core),
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state)