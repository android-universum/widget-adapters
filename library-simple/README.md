Widget-Adapters-Simple
===============

This module contains core elements for **simple** adapter implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-simple:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SimpleDataSet](https://bitbucket.org/android-universum/widget-adapters/src/main/library-simple/src/main/java/universum/studios/android/widget/adapter/SimpleDataSet.java)
- [SimpleAdapter](https://bitbucket.org/android-universum/widget-adapters/src/main/library-simple/src/main/java/universum/studios/android/widget/adapter/SimpleAdapter.java)
- [SimpleAdapterDataSet](https://bitbucket.org/android-universum/widget-adapters/src/main/library-simple/src/main/java/universum/studios/android/widget/adapter/SimpleAdapterDataSet.java)