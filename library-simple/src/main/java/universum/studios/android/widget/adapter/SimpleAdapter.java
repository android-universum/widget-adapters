/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Groups {@link DataSetAdapter} and {@link SimpleDataSet} interfaces into one unified interface for
 * <b>simple</b> adapter implementations.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface SimpleAdapter<I> extends SimpleDataSet<I> {

	/**
	 * Registers a callback to be invoked whenever swap of items data set for this adapter starts
	 * and finishes.
	 *
	 * @param listener The desired listener to register.
	 *
	 * @see #unregisterOnDataSetSwapListener(OnDataSetSwapListener)
	 * @see #swapItems(List)
	 */
	void registerOnDataSetSwapListener(@NonNull OnDataSetSwapListener<List<I>> listener);

	/**
	 * Unregisters the given <var>listener</var> from swap listeners, so it will not receive any
	 * callbacks further.
	 *
	 * @param listener The desired listener to unregister.
	 *
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	void unregisterOnDataSetSwapListener(@NonNull OnDataSetSwapListener<List<I>> listener);

	/**
	 * Same as {@link #swapItems(List)} without returning the old data set of items.
	 *
	 * @param items The desired items to be changed. May be {@code null} to clear the current ones.
	 *
	 * @see #getItems()
	 */
	void changeItems(@Nullable List<I> items);

	/**
	 * Changes items data set of this adapter and returns the old items data set.
	 *
	 * @param items The desired items to be changed. May be {@code null} to clear the current ones.
	 * @return The old items data set or {@code null} if this adapter does not have items data set
	 * specified.
	 *
	 * @see #getItems()
	 */
	@Nullable List<I> swapItems(@Nullable List<I> items);

	/**
	 * Returns the current items data set of this adapter.
	 *
	 * @return This adapter's items data set or {@code null} if there is no items data set provided
	 * by this adapter.
	 *
	 * @see #changeItems(List)
	 * @see #swapItems(List)
	 */
	@Nullable List<I> getItems();
}