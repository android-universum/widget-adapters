/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link SimpleDataSet} implementation that may be used to wrap a set of items and use it as data
 * set in simple adapter implementations.
 * <p>
 * <b>Note that this class has not been made final on purpose so it may be easily mocked in tests,
 * thought it should not been extended.</b>
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
class SimpleAdapterDataSet<I> implements SimpleDataSet<I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "SimpleAdapterDataSet";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Interface which may be used to receive callbacks for <b>changed</b>, <b>inserted</b>,
	 * <b>moved</b> or <b>removed</b> items within associated {@link SimpleAdapterDataSet}.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 */
	interface ItemsCallback {

		/**
		 * Invoked whenever a group of items has been inserted to the associated {@link SimpleAdapterDataSet}.
		 *
		 * @param positionStart Position of the first item that has been inserted.
		 * @param itemCount     Number of items inserted from the starting position.
		 */
		void onItemRangeInserted(int positionStart, int itemCount);

		/**
		 * Invoked whenever a group of items has been changed within the associated {@link SimpleAdapterDataSet}.
		 *
		 * @param positionStart Position of the first item that has been changed.
		 * @param itemCount     Number of items changed from the starting position.
		 */
		void onItemRangeChanged(int positionStart, int itemCount);

		/**
		 * Invoked whenever a single item has been moved within the associated {@link SimpleAdapterDataSet}.
		 *
		 * @param fromPosition The position from which has been item moved.
		 * @param toPosition   The position to which has been item moved.
		 */
		void onItemMoved(int fromPosition, int toPosition);

		/**
		 * Invoked whenever a group of items has been removed from the associated {@link SimpleAdapterDataSet}.
		 *
		 * @param positionStart Old position of the first item that has been removed.
		 * @param itemCount     Number of items removed from the starting position.
		 */
		void onItemRangeRemoved(int positionStart, int itemCount);
	}

	/**
	 * Simple implementation of ItemsCallback which may be used to handle all callbacks via single
	 * {@link SimpleItemsCallback#onItemsChanged()} callback.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 */
	static class SimpleItemsCallback implements ItemsCallback {

		/**
		 */
		@Override public void onItemRangeInserted(final int positionStart, final int itemCount) {
			onItemsChanged();
		}

		/**
		 */
		@Override public void onItemRangeChanged(final int positionStart, final int itemCount) {
			onItemsChanged();
		}

		/**
		 */
		@Override public void onItemMoved(final int fromPosition, final int toPosition) {
			onItemsChanged();
		}

		/**
		 */
		@Override public void onItemRangeRemoved(final int positionStart, final int itemCount) {
			onItemsChanged();
		}

		/**
		 * Invoked whenever {@link #onItemMoved(int, int)} or one of {@code onItemRange...(...)}
		 * callbacks is received.
		 */
		void onItemsChanged() {
			// May be implemented by the inheritance hierarchies to handle change in data set of
			// items in a simple way.
		}
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * List of items managed by this data set.
	 */
	private List<I> items;

	/**
	 * Callback fired whenever a change occurs in the items data set.
	 */
	private ItemsCallback itemsCallback;

	/**
	 * Resolver instance which is used by this data set to resolve position of a specific item by
	 * its corresponding id.
	 */
	private ItemPositionResolver itemPositionResolver;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleAdapterDataSet without initial items.
	 */
	SimpleAdapterDataSet() {
		this(null);
	}

	/**
	 * Creates a new instance of SimpleAdapterDataSet with the given initial list of <var>items</var>.
	 *
	 * @param items The initial items for the new data set.
	 */
	SimpleAdapterDataSet(@Nullable final List<I> items) {
		this.items = items;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Registers a callback to be invoked whenever items managed by this data set change.
	 *
	 * @param callback The desired callback to register. May be {@code null} to clear the current one.
	 */
	void setItemsCallback(final ItemsCallback callback) {
		this.itemsCallback = callback;
	}

	/**
	 * Notifies the registered {@link ItemsCallback} that some of items of this data set has been
	 * inserted.
	 *
	 * @param positionStart Position of the first item that has been inserted.
	 * @param itemCount     Number of items that has been inserted from the starting position.
	 */
	private void notifyItemRangeInserted(final int positionStart, final int itemCount) {
		if (itemsCallback != null) itemsCallback.onItemRangeInserted(positionStart, itemCount);
	}

	/**
	 * Notifies the registered {@link ItemsCallback} that some of items of this data set has been
	 * changed.
	 *
	 * @param positionStart Position of the first item that has been changed.
	 * @param itemCount     Number of items that has been changed from the starting position.
	 */
	private void notifyItemRangeChanged(final int positionStart, final int itemCount) {
		if (itemsCallback != null) itemsCallback.onItemRangeChanged(positionStart, itemCount);
	}

	/**
	 * Notifies the registered {@link ItemsCallback} that a single item of this data set has been
	 * moved.
	 *
	 * @param fromPosition Position from which has been item moved.
	 * @param toPosition   Position to which has been item moved.
	 */
	private void notifyItemMoved(final int fromPosition, final int toPosition) {
		if (itemsCallback != null) itemsCallback.onItemMoved(fromPosition, toPosition);
	}

	/**
	 * Notifies the registered {@link ItemsCallback} that some of items of this data set has been
	 * removed.
	 *
	 * @param positionStart Old position of the first item that has been removed.
	 * @param itemCount     Number of items that has been removed from the starting position.
	 */
	private void notifyItemRangeRemoved(final int positionStart, final int itemCount) {
		if (itemsCallback != null) itemsCallback.onItemRangeRemoved(positionStart, itemCount);
	}

	/**
	 * Sets a resolver that will be used by this data set to resolve position of a specific item
	 * by its associated id.
	 * <p>
	 * <b>Note that this resolver is mainly used when there are called methods upon this data set
	 * taking as argument an item id like {@link #removeItemById(long)}</b>.
	 *
	 * @param resolver The desired resolver. May be {@code null} if no resolving should be done.
	 */
	void setItemPositionResolver(final ItemPositionResolver resolver) {
		this.itemPositionResolver = resolver;
	}

	/**
	 * Resolves position of an item associated with the specified <var>itemId</var>.
	 *
	 * @param itemId Id of the item of which position to resolve.
	 * @return Resolved position or {@link ItemPositionResolver#NO_POSITION} if there is no item
	 * position resolver attached or the attached resolver could not properly resolve the position.
	 *
	 * @see #setItemPositionResolver(ItemPositionResolver)
	 */
	private int resolvePositionForItemId(final long itemId) {
		if (itemPositionResolver == null) {
			throw new IllegalStateException("No position resolver for item id specified!");
		}
		return itemPositionResolver.resolveItemPosition(itemId);
	}

	/**
	 * Sets a new list of <var>items</var> to be managed by this data set and returns the old one.
	 *
	 * @param items The desired items to attach to this data set. May be {@code null}.
	 * @return Old list of items that has been attached to this data set before or {@code null} if
	 * there were no items attached.
	 *
	 * @see #getItems()
	 */
	List<I> swapItems(final List<I> items) {
		final List<I> oldItems = this.items;
		this.items = items;
		return oldItems;
	}

	/**
	 * Returns the current list of items presented within this data set.
	 *
	 * @return Current items or {@code null} if there were no items attached nor added yet.
	 */
	List<I> getItems() {
		return items;
	}

	/**
	 */
	@Override public boolean isEmpty() {
		return getItemCount() == 0;
	}

	/**
	 */
	@Override public int getItemCount() {
		return items == null ? 0 : items.size();
	}

	/**
	 */
	@Override public void insertItem(@NonNull final I item) {
		insertItem(getItemCount(), item);
	}

	/**
	 */
	@Override public void insertItem(final int position, @NonNull final I item) {
		if (items == null) {
			this.items = new ArrayList<>();
			this.items.add(item);
			AdaptersLogging.d(TAG, "Inserted item at position(0).");
			this.notifyItemRangeInserted(0, 1);
		} else {
			this.items.add(position, item);
			AdaptersLogging.d(TAG, "Inserted item at position(" + position + ").");
			this.notifyItemRangeInserted(position, 1);
		}
	}

	/**
	 */
	@Override public void insertItems(@NonNull final List<I> items) {
		insertItems(getItemCount(), items);
	}

	/**
	 */
	@Override public void insertItems(final int positionStart, @NonNull final List<I> items) {
		final int itemsSize = items.size();
		if (this.items == null) {
			this.items = new ArrayList<>(items);
			AdaptersLogging.d(TAG, "Inserted items from start position(0) in count(" + itemsSize + ").");
			this.notifyItemRangeInserted(0, itemsSize);
		} else {
			this.items.addAll(positionStart, items);
			AdaptersLogging.d(TAG, "Inserted items from start position(" + positionStart + ") in count(" + itemsSize + ").");
			this.notifyItemRangeInserted(positionStart, itemsSize);
		}
	}

	/**
	 */
	@Override @NonNull public I swapItemById(final long itemId, @NonNull final I item) {
		final int position = resolvePositionForItemId(itemId);
		if (position == NO_POSITION) {
			throw new AssertionError("No item to be swapped with id(" + itemId + ") found!");
		}
		return swapItem(position, item);
	}

	/**
	 */
	@Override @NonNull public I swapItem(final int position, @NonNull final I item) {
		if (!hasItemAt(position)) {
			throw new AssertionError("No item to be swapped at position(" + position + ") found!");
		}
		final I oldItem = items.set(position, item);
		AdaptersLogging.d(TAG, "Swapped item at position(" + position + ").");
		this.notifyItemRangeChanged(position, 1);
		return oldItem;
	}

	/**
	 */
	@Override public void moveItem(final int fromPosition, final int toPosition) {
		if (!hasItemAt(fromPosition)) {
			throw new AssertionError("No item to be moved from position(" + fromPosition + ") to position(" + toPosition + ") found!");
		}
		if (!hasItemAt(toPosition)) {
			throw new AssertionError("No item to be moved from position(" + toPosition + ") to position(" + fromPosition + ") found!");
		}
		this.items.set(fromPosition, items.set(toPosition, items.get(fromPosition)));
		AdaptersLogging.d(TAG, "Moved item from position(" + fromPosition + ") to position(" + toPosition + ").");
		this.notifyItemMoved(fromPosition, toPosition);
	}

	/**
	 */
	@Override public int removeItem(@NonNull final I item) {
		final int position = items == null ? NO_POSITION : items.indexOf(item);
		if (position == NO_POSITION) {
			throw new AssertionError("No item(" + item + ") to be removed found!");
		}
		removeItem(position);
		return position;
	}

	/**
	 */
	@Override @NonNull public I removeItemById(final long itemId) {
		final int position = resolvePositionForItemId(itemId);
		if (position == NO_POSITION) {
			throw new AssertionError("No item to be removed with id(" + itemId + ") found!");
		}
		return removeItem(position);
	}

	/**
	 */
	@Override @NonNull public I removeItem(final int position) {
		if (hasItemAt(position)) {
			final I item = items.remove(position);
			AdaptersLogging.d(TAG, "Removed item at position(" + position + ").");
			this.notifyItemRangeRemoved(position, 1);
			return item;
		}
		throw new AssertionError("No item to be removed at position(" + position + ") found!");
	}

	/**
	 */
	@Override @NonNull public List<I> removeItems(final int positionStart, final int itemCount) {
		final int itemsSize = items.size();
		if (positionStart < 0 || positionStart + itemCount > itemsSize) {
			throw new IndexOutOfBoundsException(
					"Starting position(" + positionStart + ") and/or item count(" + itemCount + ") are out of range (positionStart < 0 || positionStart + itemCount > " + itemsSize + ")!"
			);
		}
		final List<I> items = new ArrayList<>(itemCount);
		for (int i = positionStart + itemCount - 1; i >= positionStart; i--) {
			items.add(0, this.items.remove(i));
		}
		AdaptersLogging.d(TAG, "Removed items from start position(" + positionStart + ") in count(" + itemCount + ").");
		this.notifyItemRangeRemoved(positionStart, itemCount);
		return items;
	}

	/**
	 */
	@Override public boolean hasItemAt(final int position) {
		return position >= 0 && position < getItemCount();
	}

	/**
	 */
	@Override public boolean isEnabled(final int position) {
		return true;
	}

	/**
	 */
	@Override public boolean hasStableIds() {
		return false;
	}

	/**
	 */
	@Override public long getItemId(final int position) {
		return hasItemAt(position) ? position : NO_POSITION;
	}

	/**
	 */
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	@Override @NonNull public I getItem(int position) {
		if (!hasItemAt(position)) {
			throw new IndexOutOfBoundsException(
					"Requested item at invalid position(" + position + "). " +
							"Data set has items in count of(" + getItemCount() + ")."
			);
		}
		return items.get(position);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}