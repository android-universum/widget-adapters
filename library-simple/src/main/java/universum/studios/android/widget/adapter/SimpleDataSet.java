/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * Interface which extends {@link DataSet} interface and provides additional API in order to support
 * <b>inserting</b>, <b>swapping</b>, <b>moving</b> and <b>removing</b> of desired items within the
 * data set.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface SimpleDataSet<I> extends DataSet<I> {

	/**
	 * Inserts the given <var>item</var> at the end of this data set.
	 *
	 * @param item The item to be inserted into this data set.
	 *
	 * @see #insertItem(int, Object)
	 * @see #insertItems(List)
	 */
	void insertItem(@NonNull I item);

	/**
	 * Inserts the given <var>item</var> at the specified <var>position</var>.
	 *
	 * @param position The position at which to insert the item.
	 * @param item     The item to be inserted into this data set.
	 * @throws IndexOutOfBoundsException If the specified position if out of range
	 *                                   ({@code position < 0 || position >} {@link #getItemCount()}).
	 *
	 * @see #insertItem(Object)
	 * @see #insertItems(int, List)
	 */
	void insertItem(int position, @NonNull I item);

	/**
	 * Inserts the given list of <var>items</var> at the end of this data set.
	 *
	 * @param items The desired list of items to be inserted into this data set.
	 *
	 * @see #insertItems(int, List)
	 * @see #insertItem(Object)
	 */
	void insertItems(@NonNull List<I> items);

	/**
	 * Inserts the given list of <var>items</var> from the specified <var>positionStart</var> into
	 * this data set.
	 *
	 * @param positionStart The position at which to insert first item.
	 * @param items         The desired list of items to be inserted.
	 * @throws IndexOutOfBoundsException If the specified starting position if out of range
	 *                                   ({@code position < 0 || position >} {@link #getItemCount()}).
	 * @see #insertItems(List)
	 * @see #insertItem(int, Object)
	 */
	void insertItems(int positionStart, @NonNull List<I> items);

	/**
	 * Swaps item at the specified <var>position</var> for the given <var>item</var> within this
	 * data set.
	 *
	 * @param position The position at which to swap an old item for the new one.
	 * @param item     The item to be set for the specified position.
	 * @return Old item that has been replaced by the new one.
	 * @throws AssertionError If there is no item found at the specified <var>position</var>.
	 *
	 * @see #swapItemById(long, Object)
	 * @see #moveItem(int, int)
	 */
	@NonNull I swapItem(int position, @NonNull I item);

	/**
	 * Swaps item associated with the specified <var>itemId</var> for the given <var>item</var> within
	 * this data set.
	 *
	 * @param itemId Id of the item. The id is used to resolve proper position at which to swap an
	 *               old item for the new one.
	 * @param item   The item to be set for position associated with the specified item id.
	 * @return Old item that has been replaced by the new one.
	 * @throws AssertionError If there is no item found for the specified <var>itemId</var>.
	 * @see #swapItem(int, Object)
	 * @see #moveItem(int, int)
	 */
	@NonNull
	I swapItemById(long itemId, @NonNull I item);

	/**
	 * Moves item from the specified <var>fromPosition</var> to the specified <var>toPosition</var>
	 * within this data set.
	 *
	 * @param fromPosition The position from which to move the item.
	 * @param toPosition   The position to which to move the item.
	 * @throws AssertionError If there is no item found for the <var>fromPosition</var> nor for the
	 *                        <var>toPosition</var>.
	 * @see #swapItem(int, Object)
	 */
	void moveItem(int fromPosition, int toPosition);

	/**
	 * Removes item at the specified <var>position</var> from this data set.
	 *
	 * @param position The position at which to remove item.
	 * @return Item that has been removed from the position.
	 * @throws AssertionError If there is no item found at the specified <var>position</var>.
	 *
	 * @see #removeItem(Object)
	 * @see #removeItemById(long)
	 * @see #removeItems(int, int)
	 */
	@NonNull I removeItem(int position);

	/**
	 * Removes the given <var>item</var> from this data set.
	 *
	 * @param item The item to be removed.
	 * @return Position from which has been the item removed.
	 * @throws AssertionError If there is no such <var>item</var> found.
	 *
	 * @see #removeItem(int)
	 * @see #removeItems(int, int)
	 */
	int removeItem(@NonNull I item);

	/**
	 * Removes item associated with the specified <var>itemId</var> from this data set.
	 *
	 * @param itemId Id of the item to be removed. The id is used to resolve proper position at which
	 *               to remove item.
	 * @return Item that has been removed.
	 * @throws AssertionError If there is no item found for the specified <var>itemId</var>.
	 * @see #removeItem(int)
	 * @see #removeItem(Object)
	 */
	@NonNull
	I removeItemById(long itemId);

	/**
	 * Removes a group of items starting at the specified <var>positionStart</var> in the specified
	 * <var>itemCount</var> from this data set.
	 *
	 * @param positionStart The position at which to remove first item.
	 * @param itemCount     Number of items to be removed from the starting position.
	 * @return List containing items that has been removed.
	 * @throws IndexOutOfBoundsException If the specified starting position and/or item count are out
	 *                                   of range ({@code position < 0 || positionStart + itemCount >} {@link #getItemCount()}).
	 * @see #removeItem(int)
	 */
	@NonNull List<I> removeItems(int positionStart, int itemCount);
}