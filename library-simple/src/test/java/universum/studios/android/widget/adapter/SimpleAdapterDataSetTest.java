/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class SimpleAdapterDataSetTest extends AndroidTestCase {

	private static List<String> createItems(final int count) {
		final List<String> items = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			items.add("Test item no. " + (i + 1));
		}
		return items;
	}

	@Test public void testInstantiation() {
		// Act:
		final SimpleAdapterDataSet dataSet = new SimpleAdapterDataSet();
		// Assert:
		assertThat(dataSet.hasStableIds(), is(false));
		assertThat(dataSet.isEmpty(), is(true));
		assertThat(dataSet.getItemCount(), is(0));
	}

	@Test public void testSimpleItemsCallbackOnItemRangeInserted() {
		// Arrange:
		final int mockCount = 20;
		final SimpleAdapterDataSet.SimpleItemsCallback mockCallback = mock(SimpleAdapterDataSet.SimpleItemsCallback.class);
		final SimpleItemsCallbackWrapper callbackWrapper = new SimpleItemsCallbackWrapper(mockCallback);
		// Act:
		for (int i = 0; i < mockCount; i++) {
			callbackWrapper.onItemRangeInserted(i, 1);
		}
		// Assert:
		verify(mockCallback, times(mockCount)).onItemsChanged();
		verifyNoMoreInteractions(mockCallback);
	}

	@Test public void testSimpleItemsCallbackOnItemRangeChanged() {
		// Arrange:
		final int mockCount = 20;
		final SimpleAdapterDataSet.SimpleItemsCallback mockCallback = mock(SimpleAdapterDataSet.SimpleItemsCallback.class);
		final SimpleItemsCallbackWrapper callbackWrapper = new SimpleItemsCallbackWrapper(mockCallback);
		// Act:
		for (int i = 0; i < mockCount; i++) {
			callbackWrapper.onItemRangeChanged(i, 1);
		}
		// Assert:
		verify(mockCallback, times(mockCount)).onItemsChanged();
		verifyNoMoreInteractions(mockCallback);
	}

	@Test public void testSimpleItemsCallbackOnItemMoved() {
		// Arrange:
		final int mockCount = 20;
		final SimpleAdapterDataSet.SimpleItemsCallback mockCallback = mock(SimpleAdapterDataSet.SimpleItemsCallback.class);
		final SimpleItemsCallbackWrapper callbackWrapper = new SimpleItemsCallbackWrapper(mockCallback);
		// Act:
		for (int i = 0; i < mockCount; i++) {
			callbackWrapper.onItemMoved(i, i + 1);
		}
		// Assert:
		verify(mockCallback, times(mockCount)).onItemsChanged();
		verifyNoMoreInteractions(mockCallback);
	}

	@Test public void testSimpleItemsCallbackOnItemRangeRemoved() {
		// Arrange:
		final int mockCount = 20;
		final SimpleAdapterDataSet.SimpleItemsCallback mockCallback = mock(SimpleAdapterDataSet.SimpleItemsCallback.class);
		final SimpleItemsCallbackWrapper callbackWrapper = new SimpleItemsCallbackWrapper(mockCallback);
		// Act:
		for (int i = 0; i < mockCount; i++) {
			callbackWrapper.onItemRangeRemoved(i, 1);
		}
		// Assert:
		verify(mockCallback, times(mockCount)).onItemsChanged();
		verifyNoMoreInteractions(mockCallback);
	}

	@Test public void testSimpleItemsCallbackOnItemsChanged() {
		// Arrange:
		final SimpleAdapterDataSet.SimpleItemsCallback callback = new SimpleAdapterDataSet.SimpleItemsCallback();
		// Act:
		callback.onItemsChanged();
	}

	@Test public void testSwapItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		// Act + Assert:
		dataSet.swapItems(items);
		assertThat(dataSet.getItems(), is(not(nullValue())));
		assertThat(dataSet.isEmpty(), is(false));
		assertThat(dataSet.getItemCount(), is(items.size()));
		dataSet.swapItems(null);
		assertThat(dataSet.getItems(), is(nullValue()));
		assertThat(dataSet.isEmpty(), is(true));
		assertThat(dataSet.getItemCount(), is(0));
	}

	@Test public void testGetItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		dataSet.swapItems(items);
		// Act + Assert:
		assertThat(dataSet.getItems(), is(items));
	}

	@Test public void testGetItemsOnEmptyDataSet() {
		// Arrange:
		final SimpleAdapterDataSet dataSet = new SimpleAdapterDataSet();
		// Act + Assert:
		assertThat(dataSet.getItems(), is(nullValue()));
	}

	@Test public void testIsEmpty() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.swapItems(createItems(10));
		// Act + Assert:
		assertThat(dataSet.isEmpty(), is(false));
	}

	@Test public void testGetItemCount() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		dataSet.swapItems(createItems(10));
		// Act + Assert:
		assertThat(dataSet.getItemCount(), is(items.size()));
	}

	@Test public void testInsertItemWithoutAttachedCallback() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		// Act:
		// Only ensure that the data set does not throw a NullPointerException exception.
		dataSet.insertItem("Item");
	}

	@Test public void testInsertItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		for (int i = 0; i < 25; i++) {
			// Act:
			dataSet.insertItem("Item at: " + i);
			// Assert:
			assertThat(dataSet.getItemCount(), is(i + 1));
			assertThat(dataSet.getItem(i), is("Item at: " + i));
			assertThat(dataSet.getItemId(i), is((long) i));
			verify(mockCallback).onItemRangeInserted(i, 1);
		}
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test public void testInsertItemAtPosition() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		for (int i = 0; i < 15; i++) {
			// Act:
			dataSet.insertItem(i, "Item at: " + i);
			// Assert:
			assertThat(dataSet.getItemCount(), is(i + 1));
			assertThat(dataSet.getItem(i), is("Item at: " + i));
			assertThat(dataSet.getItemId(i), is((long) i));
			verify(mockCallback).onItemRangeInserted(i, 1);
		}
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testInsertItemAtPositionOutOfRangeUnder() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.insertItem(-1, "Item at invalid position");
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testInsertItemAtPositionOutOfRangeOver() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.insertItem(2, "Item at invalid position");
	}

	@Test public void testInsertItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.swapItems(createItems(20));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act:
		dataSet.insertItems(createItems(10));
		// Assert:
		assertThat(dataSet.getItemCount(), is(30));
		verify(mockCallback).onItemRangeInserted(20, 10);
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test public void testInsertItemsIntoEmptyDataSet() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act:
		dataSet.insertItems(createItems(10));
		// Assert:
		assertThat(dataSet.getItemCount(), is(10));
		verify(mockCallback).onItemRangeInserted(0, 10);
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test public void testInsertItemsAtPosition() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.swapItems(createItems(10));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act:
		dataSet.insertItems(4, createItems(5));
		// Assert:
		assertThat(dataSet.getItemCount(), is(15));
		verify(mockCallback).onItemRangeInserted(4, 5);
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testInsertItemsAtPositionOutOfRangeUnder() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.insertItems(-1, createItems(1));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testInsertItemsAtPositionOutOfRangeOver() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.insertItems(2, createItems(1));
	}

	@Test public void testSwapItemWithoutAttachedCallback() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item 1");
		// Act:
		// Only ensure that the data set does not throw a NullPointerException exception.
		dataSet.swapItem(0, "UpdatedItem 1");
	}

	@Test public void testSwapItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.swapItems(createItems(10));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act + Assert:
		for (int i = 0; i < dataSet.getItemCount(); i++) {
			assertThat(dataSet.swapItem(i, "SwappedItem at: " + i), is("Test item no. " + (i + 1)));
			assertThat(dataSet.getItem(i), is("SwappedItem at: " + i));
			verify(mockCallback, times(1)).onItemRangeChanged(i, 1);
		}
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test(expected = AssertionError.class)
	public void testSwapItemOutOfRange() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.swapItem(1, "UpdatedItem at invalid position");
	}

	@Test public void testSwapItemById() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		final DataSet mockDataSet = mock(DataSet.class);
		when(mockDataSet.getItemCount()).thenReturn(items.size());
		dataSet.swapItems(items);
		dataSet.setItemPositionResolver(new DataSetItemPositionResolver(mockDataSet));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act + Assert:
		for (int i = 0; i < dataSet.getItemCount(); i++) {
			when(mockDataSet.getItemId(i)).thenReturn(i + 1L);
			assertThat(dataSet.swapItemById(i + 1L, "SwappedItem at: " + i), is("Test item no. " + (i + 1)));
			assertThat(dataSet.getItem(i), is("SwappedItem at: " + i));
		}
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test(expected = AssertionError.class)
	public void testSwapItemByIdNotInDataSet() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.setItemPositionResolver(itemId -> ItemPositionResolver.NO_POSITION);
		// Act:
		dataSet.swapItemById(1, "Item");
	}

	@Test public void testMoveItemWithoutAttachedCallback() {
		// Arrange:
		// Only ensure that the data set does not throw a NullPointerException exception.
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item 1");
		dataSet.insertItem("Item 2");
		// Act:
		dataSet.moveItem(0, 1);
	}

	@Test public void testMoveItemUp() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem(0, "Item 1");
		dataSet.insertItem(1, "Item 2");
		dataSet.insertItem(2, "Item 3");
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act:
		dataSet.moveItem(0, 2);
		// Assert:
		assertThat(dataSet.getItem(0), is("Item 3"));
		assertThat(dataSet.getItem(1), is("Item 2"));
		assertThat(dataSet.getItem(2), is("Item 1"));
		verify(mockCallback).onItemMoved(0, 2);
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test public void testMoveItemDown() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem(0, "Item 1");
		dataSet.insertItem(1, "Item 2");
		dataSet.insertItem(2, "Item 3");
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act:
		dataSet.moveItem(1, 0);
		// Assert:
		assertThat(dataSet.getItem(0), is("Item 2"));
		assertThat(dataSet.getItem(1), is("Item 1"));
		assertThat(dataSet.getItem(2), is("Item 3"));
		verify(mockCallback, times(1)).onItemMoved(1, 0);
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeRemoved(anyInt(), anyInt());
	}

	@Test(expected = AssertionError.class)
	public void testMoveItemFromPositionOutOfRange() {
		// Arrange:
		final SimpleAdapterDataSet dataSet = new SimpleAdapterDataSet<>();
		// Act:
		dataSet.moveItem(0, 1);
	}

	@Test(expected = AssertionError.class)
	public void testMoveItemToPositionOutOfRange() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.moveItem(0, 2);
	}

	@Test public void testRemoveItemWithoutAttachedCallback() {
		// Arrange:
		// Only ensure that the data set does not throw a NullPointerException exception.
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item 1");
		// Act:
		dataSet.removeItem("Item 1");
	}

	@Test public void testRemoveItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		dataSet.swapItems(items);
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act + Assert:
		for (int i = dataSet.getItemCount() - 1; i >= 0; i--) {
			assertThat(dataSet.removeItem(items.get(i)), is(i));
			assertThat(dataSet.hasItemAt(i), is(false));
			verify(mockCallback).onItemRangeRemoved(i, 1);
		}
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
	}

	@Test(expected = AssertionError.class)
	public void testRemoveItemNotInDataSet() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		// Act:
		dataSet.removeItem("Item not in data set");
	}

	@Test public void testRemoveItemAtPosition() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.swapItems(createItems(10));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act + Assert:
		for (int i = dataSet.getItemCount() - 1; i >= 0; i--) {
			assertThat(dataSet.removeItem(i), is("Test item no. " + (i + 1)));
			assertThat(dataSet.hasItemAt(i), is(false));
			verify(mockCallback).onItemRangeRemoved(i, 1);
		}
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
	}

	@Test(expected = AssertionError.class)
	public void testRemoveItemAtPositionNotInDataSet() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.removeItem(1);
	}

	@Test public void testRemoveItemById() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		final DataSet mockDataSet = mock(DataSet.class);
		when(mockDataSet.getItemCount()).thenReturn(items.size());
		dataSet.swapItems(items);
		dataSet.setItemPositionResolver(new DataSetItemPositionResolver(mockDataSet));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act + Assert:
		for (int i = dataSet.getItemCount() - 1; i >= 0; i--) {
			when(mockDataSet.getItemId(i)).thenReturn(i + 1L);
			assertThat(dataSet.removeItemById(i + 1L), is("Test item no. " + (i + 1)));
			assertThat(dataSet.hasItemAt(i), is(false));
			verify(mockCallback).onItemRangeRemoved(i, 1);
		}
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
	}

	@Test(expected = AssertionError.class)
	public void testRemoveItemByIdNotInDataSet() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.setItemPositionResolver(itemId -> ItemPositionResolver.NO_POSITION);
		// Act:
		dataSet.removeItemById(1);
	}

	@Test public void testRemoveItems() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.swapItems(createItems(10));
		final SimpleAdapterDataSet.ItemsCallback mockCallback = mock(SimpleAdapterDataSet.ItemsCallback.class);
		dataSet.setItemsCallback(mockCallback);
		// Act:
		final List<String> removedItems = dataSet.removeItems(4, 5);
		// Assert:
		assertThat(removedItems, is(notNullValue()));
		assertThat(removedItems.size(), is(5));
		for (int i = 0; i < removedItems.size(); i++) {
			assertThat(removedItems.get(i), is("Test item no. " + (i + 5)));
		}
		assertThat(dataSet.getItemCount(), is(5));
		verify(mockCallback).onItemRangeRemoved(4, 5);
		verify(mockCallback, times(0)).onItemRangeInserted(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemRangeChanged(anyInt(), anyInt());
		verify(mockCallback, times(0)).onItemMoved(anyInt(), anyInt());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveItemsOutOfRangeUnder() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.removeItems(-1, 1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveItemsOutOfRangeOver() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		dataSet.insertItem("Item");
		// Act:
		dataSet.removeItems(2, 1);
	}

	@Test public void testHasItemAt() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		dataSet.swapItems(items);
		// Act + Assert:
		assertThat(dataSet.hasItemAt(-1), is(false));
		for (int i = 0; i < items.size(); i++) {
			assertThat(dataSet.hasItemAt(i), is(true));
		}
		assertThat(dataSet.hasItemAt(items.size()), is(false));
	}

	@Test public void testHasItemAtOnEmptyDataSet() {
		// Arrange:
		final DataSet<String> dataSet = new SimpleAdapterDataSet<>();
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			assertThat(dataSet.hasItemAt(i), is(false));
		}
	}

	@Test public void testIsEnabled() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		// Act + Assert:
		for (int i = 0; i < 15; i++) {
			assertThat(dataSet.isEnabled(i), is(true));
		}
	}

	@Test public void testGetItemId() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		dataSet.swapItems(items);
		// Act + Assert:
		for (int i = 0; i < items.size(); i++) {
			assertThat(dataSet.getItemId(i), is((long) i));
		}
	}

	@Test public void testGetItemIdOnEmptyDataSet() {
		// Arrange:
		final DataSet<String> dataSet = new SimpleAdapterDataSet<>();
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			assertThat(dataSet.getItemId(i), is(DataSet.NO_ID));
		}
	}

	@Test public void testGetItem() {
		// Arrange:
		final SimpleAdapterDataSet<String> dataSet = new SimpleAdapterDataSet<>();
		final List<String> items = createItems(10);
		dataSet.swapItems(items);
		// Act + Assert:
		for (int i = 0; i < items.size(); i++) {
			assertThat(dataSet.getItem(i), is(items.get(i)));
		}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetItemOnEmptyDataSet() {
		// Arrange:
		final SimpleAdapterDataSet dataSet = new SimpleAdapterDataSet<>();
		// Act:
		dataSet.getItem(0);
	}

	@Test(expected = IllegalStateException.class)
	public void testByIdWithoutPositionResolverAttached() {
		// Arrange:
		final SimpleAdapterDataSet dataSet = new SimpleAdapterDataSet<>();
		// Act:
		dataSet.removeItemById(1);
	}

	private static class SimpleItemsCallbackWrapper extends SimpleAdapterDataSet.SimpleItemsCallback {

		private final SimpleAdapterDataSet.SimpleItemsCallback callback;

		SimpleItemsCallbackWrapper(final SimpleAdapterDataSet.SimpleItemsCallback callback) {
			this.callback = callback;
		}

		@Override void onItemsChanged() {
			this.callback.onItemsChanged();
		}
	}
}