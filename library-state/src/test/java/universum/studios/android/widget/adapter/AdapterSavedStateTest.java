/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.os.Parcel;
import android.os.Parcelable;

import org.junit.Test;

import androidx.annotation.NonNull;
import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class AdapterSavedStateTest extends AndroidTestCase {

	@Test public void testInstantiationWithSuperState() {
		// Act:
		final TestState superState = new TestState(AdapterSavedState.EMPTY_STATE);
		final AdapterSavedState savedState = new AdapterSavedState(superState) {};
		// Assert:
		assertThat(savedState.getSuperState(), is(superState));
	}

	@Test public void testInstantiationWithEmptySuperState() {
		// Act:
		final AdapterSavedState savedState = new AdapterSavedState(AdapterSavedState.EMPTY_STATE) {};
		// Assert:
		assertThat(savedState.getSuperState(), is(AdapterSavedState.EMPTY_STATE));
	}

	@Test public void testInstantiationFromParcelWithSuperState() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		final TestState superState = new TestState(AdapterSavedState.EMPTY_STATE);
		superState.count = 1000;
		parcel.writeParcelable(superState, 0);
		parcel.setDataPosition(0);
		// Act:
		final AdapterSavedState savedState = new AdapterSavedState(parcel) {};
		final TestState restoredSuperState = (TestState) savedState.getSuperState();
		// Assert:
		assertThat(restoredSuperState, is(notNullValue()));
		assertThat(restoredSuperState.count, is(1000));
		parcel.recycle();
	}

	@Test public void testInstantiationFromParcelWithoutSuperState() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		parcel.writeParcelable(null, 0);
		parcel.setDataPosition(0);
		// Act:
		final AdapterSavedState savedState = new AdapterSavedState(parcel) {};
		// Assert:
		assertThat(savedState.getSuperState(), is(AdapterSavedState.EMPTY_STATE));
		parcel.recycle();
	}

	@Test public void testEmptyState() {
		// Assert:
		assertThat(AdapterSavedState.EMPTY_STATE, is(notNullValue()));
	}

	@Test public void testCreatorCreateFromParcel() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		// Act:
		final AdapterSavedState savedState = AdapterSavedState.CREATOR.createFromParcel(parcel);
		// Assert:
		assertThat(savedState, is(AdapterSavedState.EMPTY_STATE));
		assertThat(savedState.getSuperState(), is(AdapterSavedState.EMPTY_STATE));
		parcel.recycle();
	}

	@Test(expected = IllegalStateException.class)
	public void testCreatorCreateFromParcelWithSuperState() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		parcel.writeParcelable(new TestParcelable(), 0);
		parcel.setDataPosition(0);
		// Act:
		AdapterSavedState.CREATOR.createFromParcel(parcel);
	}

	@Test public void testCreatorNewArray() {
		// Act:
		final AdapterSavedState[] savedStates = AdapterSavedState.CREATOR.newArray(5);
		// Assert:
		assertThat(savedStates.length, is(5));
		for (final AdapterSavedState savedState : savedStates) {
			assertThat(savedState, is(nullValue()));
		}
	}

	@Test public void testWriteToParcel() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		final TestState savedState = new TestState(TestState.EMPTY_STATE);
		savedState.count = 10;
		// Act:
		savedState.writeToParcel(parcel, 0);
		// Assert:
		parcel.setDataPosition(0);
		assertThat(parcel.readParcelable(TestState.class.getClassLoader()), is(nullValue()));
		assertThat(parcel.readInt(), is(10));
		parcel.recycle();
	}

	@Test public void testDescribeContents() {
		// Arrange:
		final AdapterSavedState savedState = new AdapterSavedState(TestState.EMPTY_STATE) {};
		// Act + Assert:
		assertThat(savedState.describeContents(), is(0));
	}

	private static final class TestState extends AdapterSavedState {

		public static final Creator<TestState> CREATOR = new Creator<TestState>() {

			@Override public TestState createFromParcel(@NonNull final Parcel source) {
				return new TestState(source);
			}

			@Override public TestState[] newArray(final int size) {
				return new TestState[size];
			}
		};

		private int count;

		TestState(@NonNull final Parcelable superState) {
			super(superState);
		}

		TestState(@NonNull final Parcel source) {
			super(source);
			this.count = source.readInt();
		}

		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(count);
		}
	}

	private static final class TestParcelable implements Parcelable {

		public static final Creator<TestParcelable> CREATOR = new Creator<TestParcelable>() {

			@Override public TestParcelable createFromParcel(@NonNull final Parcel source) {
				return new TestParcelable(source);
			}

			@Override public TestParcelable[] newArray(final int size) {
				return new TestParcelable[size];
			}
		};

		TestParcelable() {}
		TestParcelable(@NonNull Parcel source) {}

		@Override public int describeContents() { return 0; }

		@Override public void writeToParcel(@NonNull Parcel dest, int flags) {}
	}
}