Widget-Adapters-State
===============

This module contains **parcelable** state implementation that may be used by adapters to save
theirs internal state.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-state:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [AdapterState](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state/src/main/java/universum/studios/android/widget/adapter/AdapterState.java)
