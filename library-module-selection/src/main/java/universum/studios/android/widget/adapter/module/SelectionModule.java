/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.CallSuper;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.widget.adapter.AdapterSavedState;

/**
 * An {@link AdapterModule AdapterModule} implementation that specifies API to support selection
 * feature for the associated adapter.
 * <p>
 * This module supports both, {@link #SINGLE} and {@link #MULTIPLE} selection modes. The desired
 * selection mode may be changed via {@link #setMode(int)} and obtained via {@link #getMode()}.
 * <p>
 * The associated adapter is required to dispatch to this module desired selection changes either via
 * {@link #toggleSelection(long)} or via {@link #setSelected(long, boolean)} methods. Initial selection
 * may be specified via {@link #setSelection(List)}. The current selection (selected ids) may be obtained
 * via {@link #getSelection()} where in {@link #SINGLE} selection mode the returned array will contain
 * only single item. The code snippet below shows basic usage of this module within an adapter:
 * <pre>
 * public class SampleAdapter extends BaseAdapter implements ModuleAdapter {
 *
 *      // Selection module providing selection feature for this adapter.
 *      private SelectionModule mSelectionModule;
 *
 *      // ...
 *
 *      public SampleAdapter(&#64;NonNull Context context) {
 *          super(context);
 *          this.mSelectionModule = new SelectionModule();
 *          this.mSelectionModule.setMode(SelectionModule.MULTIPLE);
 *          this.mSelectionModule.attachToAdapter(this);
 *      }
 *
 *      public void toggleItemSelection(long id) {
 *          mSelectionModule.toggleSelection(id);
 *          // no need to call notifyDataSetChanged() if adapter auto notification is enabled
 *      }
 *
 *      &#64;NonNull
 *      public List&lt;Long&gt; getSelection() {
 *          return mSelectionModule.getSelection();
 *      }
 *
 *      &#64;NonNull
 *      protected void onBindViewHolder(&#64;NonNull Object viewHolder, int position) {
 *          // Use mSelectionModule.isSelected(getItemId(position)) to check if item is selected or not.
 *      }
 *
 *      // ...
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SelectionModule extends AdapterModule {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SelectionModule";

	/**
	 * Mode that allows only a single item to be selected.
	 *
	 * @see #getSelection()
	 */
	public static final int SINGLE = 0x01;

	/**
	 * Mode that allows multiple items to be selected.
	 *
	 * @see #getSelection()
	 */
	public static final int MULTIPLE = 0x02;

	/**
	 * Defines an annotation for determining set of allowed modes for {@link SelectionModule}.
	 *
	 * <h3>Modes</h3>
	 * <ul>
	 * <li>{@link #SINGLE}</li>
	 * <li>{@link #MULTIPLE}</li>
	 * </ul>
	 */
	@IntDef({SINGLE, MULTIPLE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface SelectionMode {}

	/**
	 * Initial capacity for list containing selected ids in <b>single</b> selection mode.
	 */
	private static final int INITIAL_CAPACITY_SINGLE = 1;

	/**
	 * Initial capacity for list containing selected ids in <b>multi</b> selection mode.
	 */
	private static final int INITIAL_CAPACITY_MULTI = 10;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Current selection mode of this module.
	 */
	private int mode = SINGLE;

	/**
	 * List which contains all currently selected ids.
	 */
	private List<Long> selection;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets the current selection mode of this module to the specified one.
	 * <p>
	 * <b>Note that changing of selection mode also clears the current selection of this module
	 * and notifies adapter.</b>
	 * <p>
	 * Default value: <b>{@link #SINGLE}</b>
	 *
	 * @param mode The desired selection mode. One of {@link #SINGLE} or {@link #MULTIPLE}.
	 *
	 * @see #getMode()
	 * @see #getSelection()
	 */
	public void setMode(@SelectionMode final int mode) {
		if (this.mode != mode) {
			this.mode = mode;
			if (selection != null && !selection.isEmpty()) {
				this.selection = null;
				notifyAdapter();
			}
		}
	}

	/**
	 * Returns the current selection mode of this module.
	 *
	 * @return Current selection mode. One of {@link #SINGLE} or {@link #MULTIPLE}.
	 *
	 * @see #setMode(int)
	 */
	@SelectionMode public int getMode() {
		return mode;
	}

	/**
	 * Asserts that this module is in {@link #MULTIPLE} selection mode, if not an exception is thrown.
	 *
	 * @throws IllegalStateException If the current selection mode is not {@link #MULTIPLE}.
	 */
	private void assertInMultipleSelectionModeOrThrow() {
		if (mode != MULTIPLE) throw new IllegalStateException("Not in MULTIPLE selection mode.");
	}

	/**
	 * Changes selection state of the specified <var>id</var> to the opposite one
	 * ({@code selected -> unselected; unselected -> selected}) and <b>notifies adapter</b>.
	 *
	 * @param id The id of an item of which selection state to toggle.
	 * @return Size of the current selection.
	 *
	 * @see #setSelected(long, boolean)
	 * @see #isAdapterNotificationEnabled()
	 */
	public int toggleSelection(final long id) {
		setSelected(id, !isSelected(id));
		return getSelectionSize();
	}

	/**
	 * Changes selection state of the specified <var>id</var> to the desired one and <b>notifies adapter</b>.
	 *
	 * @param id       The id of an item of which selection state to change.
	 * @param selected New selection state. {@code True} to be selected, {@code false} otherwise,
	 *
	 * @see #toggleSelection(long)
	 * @see #selectRange(int, int)
	 * @see #selectAll()
	 * @see #isAdapterNotificationEnabled()
	 */
	public void setSelected(final long id, final boolean selected) {
		if (selected) {
			if (mode == SINGLE) {
				clearSelection(false);
			}
			select(id);
		} else {
			deselect(id);
		}
		notifyAdapter();
	}

	/**
	 * Same as {@link #selectRange(int, int)} with parameters {@code (0, ModuleAdapter.getItemCount())}.
	 * <p>
	 * <b>Note that calling of this method for mode other than {@link #MULTIPLE} will throw an
	 * exception.</b>
	 *
	 * @throws IllegalStateException If the current mode is not {@link #MULTIPLE}.
	 *
	 * @see #setSelected(long, boolean)
	 */
	public void selectAll() {
		assertAttachedToAdapterOrThrow();
		selectRange(0, adapter.getItemCount());
	}

	/**
	 * Selects all ids in the {@code [startPosition, startPosition + count)} range and <b>notifies adapter</b>.
	 * <p>
	 * All previously selected ids will remain selected.
	 * <p>
	 * <b>Note that calling of this method for mode other than {@link #MULTIPLE} will throw an
	 * exception.</b>
	 *
	 * @param startPosition The position from which to start selection.
	 * @param count         Count of items to select from the start position.
	 * @throws IllegalStateException     If the current mode is not {@link #MULTIPLE}.
	 * @throws IndexOutOfBoundsException If {@code startPosition + count > n}.
	 *
	 * @see #selectAll()
	 * @see #setSelected(long, boolean)
	 * @see #isAdapterNotificationEnabled()
	 */
	public void selectRange(final int startPosition, final int count) {
		assertInMultipleSelectionModeOrThrow();
		assertAttachedToAdapterOrThrow();
		// Check correct index.
		final int n = adapter.getItemCount();
		if (startPosition + count > n) {
			throw new IndexOutOfBoundsException("Incorrect count(" + count + ") for start position(" + startPosition + "). Adapter has only " + n + " items.");
		}
		// Select all ids in the requested range.
		for (int i = startPosition; i < startPosition + count; i++) {
			select(adapter.getItemId(i));
		}
		notifyAdapter();
	}

	/**
	 * Sets a selection for this module and <b>notifies adapter</b>. The specified selection will
	 * override any current selection of this module.
	 *
	 * @param selection The desired selection. May be {@code null} to clear the current selection.
	 *
	 * @see #getSelection()
	 * @see #isAdapterNotificationEnabled()
	 */
	public void setSelection(@Nullable final List<Long> selection) {
		this.selection = selection == null ? null : new ArrayList<>(selection);
	}

	/**
	 * Checks whether the specified <var>id</var> is currently selected or not.
	 *
	 * @param id The id of an item of which selection state to check.
	 * @return {@code True} if item with the specified id is selected, {@code false} otherwise.
	 *
	 * @see #setSelected(long, boolean)
	 * @see #toggleSelection(long)
	 */
	public boolean isSelected(final long id) {
		return selection != null && selection.contains(id);
	}

	/**
	 * Returns list containing ids that are at this time selected within this module.
	 * <p>
	 * In {@link #SINGLE} selection mode the returned list will contain maximum of 1 selected id,
	 * in {@link #MULTIPLE} selection mode all selected ids will be contained in the returned list.
	 *
	 * @return {@link List} with selected ids or <b>immutable</b> empty list if there is no selection.
	 */
	@NonNull public List<Long> getSelection() {
		return selection == null ? Collections.<Long>emptyList() : new ArrayList<>(selection);
	}

	/**
	 * Returns size of the current selection.
	 *
	 * @return Count of the currently selected ids.
	 *
	 * @see #getSelection()
	 */
	public int getSelectionSize() {
		return selection == null ? 0 : selection.size();
	}

	/**
	 * Deselects all currently selected ids and <b>notifies adapter</b>.
	 *
	 * @see #clearSelectionInRange(int, int)
	 * @see #isAdapterNotificationEnabled()
	 */
	public void clearSelection() {
		clearSelection(true);
	}

	/**
	 * Removes all ids form the set of the currently selected ids.
	 *
	 * @param notify {@code True} to notify the associated adapter via {@link #notifyAdapter()},
	 *               {@code false} otherwise.
	 */
	protected final void clearSelection(final boolean notify) {
		if (selection != null) {
			selection.clear();
			if (notify) {
				notifyAdapter();
			}
		}
	}

	/**
	 * Deselects all currently selected ids in the {@code [startPosition, startPosition + count)}
	 * range and <b>notifies adapter</b>.
	 * <p>
	 * <b>Note that calling of this method for mode other than {@link #MULTIPLE} will throw an
	 * exception.</b>
	 *
	 * @param startPosition The position from which to start deselection.
	 * @param count         Count of items to deselect from the start position.
	 * @throws IllegalStateException     If the current mode is not {@link #MULTIPLE}.
	 * @throws IndexOutOfBoundsException If {@code startPosition + count > n}.
	 *
	 * @see #clearSelection()
	 * @see #isAdapterNotificationEnabled()
	 */
	public void clearSelectionInRange(final int startPosition, final int count) {
		assertInMultipleSelectionModeOrThrow();
		assertAttachedToAdapterOrThrow();
		// Check correct index.
		final int n = adapter.getItemCount();
		if (startPosition + count > n) {
			throw new IndexOutOfBoundsException("Incorrect count(" + count + ") for start position(" + startPosition + "). Adapter has only " + n + " items.");
		}
		// Deselect all ids in the requested range.
		for (int i = startPosition; i < startPosition + count; i++) {
			deselect(adapter.getItemId(i));
		}
		notifyAdapter();
	}

	/**
	 * Adds the specified <var>id</var> into the set of the currently selected ids. If there is already
	 * same id presented, addition of the given one will be ignored.
	 *
	 * @param id The id to add into the selected ones.
	 *
	 * @see #deselect(long)
	 */
	protected final void select(final long id) {
		if (selection == null) selection = new ArrayList<>(mode == SINGLE ? INITIAL_CAPACITY_SINGLE : INITIAL_CAPACITY_MULTI);
		if (!selection.contains(id)) selection.add(id);
	}

	/**
	 * Removes the specified <var>id</var> form the set of the currently selected ids.
	 *
	 * @param id The id to remove from the selected ones.
	 *
	 * @see #select(long)
	 */
	protected final void deselect(final long id) {
		if (selection != null) selection.remove(id);
	}

	/**
	 * @return {@code True} whether this module have some selection to save, {@code false} otherwise.
	 */
	@Override public boolean requiresStateSaving() {
		return selection != null && !selection.isEmpty();
	}

	/**
	 */
	@Override @CallSuper @NonNull public Parcelable saveInstanceState() {
		final SavedState state = new SavedState(super.saveInstanceState());
		state.mode = mode;
		if (selection != null && !selection.isEmpty()) {
			final long[] selectionArray = new long[selection.size()];
			for (int i = 0; i < selection.size(); i++) {
				selectionArray[i] = selection.get(i);
			}
			state.selection = selectionArray;
		}
		return state;
	}

	/**
	 */
	@Override @CallSuper public void restoreInstanceState(@NonNull final Parcelable savedState) {
		if (!(savedState instanceof SavedState)) {
			super.restoreInstanceState(savedState);
			return;
		}
		final SavedState state = (SavedState) savedState;
		super.restoreInstanceState(state.getSuperState());
		this.mode = state.mode;
		if (state.selection != null) {
			if (selection == null) {
				this.selection = new ArrayList<>(state.selection.length);
			}
			for (final long id : state.selection) {
				this.selection.add(id);
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * An {@link AdapterSavedState} implementation used to ensure that the state of {@link SelectionModule}
	 * is properly saved.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	@SuppressWarnings("WeakerAccess")
	public static class SavedState extends AdapterSavedState {

		/**
		 * Creator used to create an instance or array of instances of SavedState from {@link Parcel}.
		 */
		public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {

			/**
			 */
			@Override public SavedState createFromParcel(@NonNull final Parcel source) {
				return new SavedState(source);
			}

			/**
			 */
			@Override public SavedState[] newArray(final int size) {
				return new SavedState[size];
			}
		};

		/**
		 */
		int mode = SINGLE;

		/**
		 */
		long[] selection;

		/**
		 * Creates a new instance of SavedState with the given <var>superState</var> to allow chaining
		 * of saved states in {@link #saveInstanceState()} and also in {@link #restoreInstanceState(Parcelable)}.
		 *
		 * @param superState The super state obtained from {@code super.saveInstanceState()} within
		 *                   {@link #saveInstanceState()}.
		 */
		protected SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		/**
		 * Called from {@link #CREATOR} to create an instance of SavedState form the given parcel
		 * <var>source</var>.
		 *
		 * @param source Parcel with data for the new instance.
		 */
		protected SavedState(@NonNull final Parcel source) {
			super(source);
			this.mode = source.readInt();
			this.selection = source.createLongArray();
		}

		/**
		 */
		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(mode);
			dest.writeLongArray(selection);
		}
	}
}