/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.module;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import universum.studios.android.testing.AndroidTestCase;
import universum.studios.android.widget.adapter.SimpleListAdapter;
import universum.studios.android.widget.adapter.holder.ViewHolder;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author Martin Albedinsky
 */
public final class SelectionModuleTest extends AndroidTestCase {

	private static final List<Long> SELECTION_LIST = Arrays.asList(1L, 8L, 9L, 10L, 15L);
	private static final long[] SELECTION_ARRAY = new long[SELECTION_LIST.size()];

	static {
		for (int i = 0; i < SELECTION_LIST.size(); i++) {
			SELECTION_ARRAY[i] = SELECTION_LIST.get(i);
		}
	}

	@Test public void testInstantiation() {
		// Act:
		final SelectionModule module = new SelectionModule();
		// Assert:
		assertThat(module.getMode(), is(SelectionModule.SINGLE));
		assertThat(module.getSelection(), is(notNullValue()));
		assertThat(module.getSelection().isEmpty(), is(true));
		assertThat(module.requiresStateSaving(), is(false));
	}

	@Test public void testSetModeWithoutInitialSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		// Act:
		module.setMode(SelectionModule.MULTIPLE);
		// Assert:
		assertThat(module.getMode(), is(SelectionModule.MULTIPLE));
		assertThat(adapter.notifyDataSetChangedReceived, is(false));
	}

	@Test public void testSetModeOnNotEmptySelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.select(1L);
		module.select(2L);
		// Act:
		module.setMode(SelectionModule.MULTIPLE);
		// Assert:
		assertThat(module.getSelectionSize(), is(0));
		assertThat(adapter.notifyDataSetChangedReceived, is(true));
	}

	@Test public void testSetModeOnEmptySelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.select(1L);
		module.deselect(1L);
		// Act:
		module.setMode(SelectionModule.MULTIPLE);
		// Assert:
		assertThat(adapter.notifyDataSetChangedReceived, is(false));
	}

	@Test public void testSetModeSame() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		// Act:
		module.setMode(SelectionModule.MULTIPLE);
		// Assert:
		assertThat(module.getMode(), is(SelectionModule.MULTIPLE));
	}

	@Test public void testToggleSelectionInSingleMode() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act + Assert:
		// Toggle with the same id.
		module.toggleSelection(0L);
		assertThat(module.isSelected(0L), is(true));
		module.toggleSelection(0L);
		assertThat(module.isSelected(0L), is(false));
		// Toggle with different ids.
		module.toggleSelection(1L);
		assertThat(module.isSelected(1L), is(true));
		module.toggleSelection(2L);
		assertThat(module.isSelected(2L), is(true));
		assertThat(module.isSelected(1L), is(false));
		module.toggleSelection(2L);
		assertThat(module.isSelected(2L), is(false));
		assertThat(module.isSelected(1L), is(false));
	}

	@Test public void testToggleSelectionInMultipleMode() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		// Act + Assert:
		module.toggleSelection(0L);
		assertThat(module.isSelected(0L), is(true));
		module.toggleSelection(0L);
		assertThat(module.isSelected(0L), is(false));
		module.toggleSelection(0L);
		assertThat(module.isSelected(0L), is(true));
		module.toggleSelection(1L);
		assertThat(module.isSelected(0L), is(true));
		assertThat(module.isSelected(1L), is(true));
		module.toggleSelection(2L);
		assertThat(module.isSelected(0L), is(true));
		assertThat(module.isSelected(1L), is(true));
		assertThat(module.isSelected(2L), is(true));
		module.toggleSelection(1L);
		assertThat(module.isSelected(0L), is(true));
		assertThat(module.isSelected(1L), is(false));
		assertThat(module.isSelected(2L), is(true));
	}

	@Test public void testSetSelectedInSingleMode() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act + Assert:
		assertThat(module.isSelected(0L), is(false));
		module.setSelected(0L, true);
		assertThat(module.isSelected(0L), is(true));
		assertThat(module.getSelectionSize(), is(1));
		module.setSelected(1L, true);
		assertThat(module.isSelected(0L), is(false));
		assertThat(module.isSelected(1L), is(true));
		assertThat(module.getSelectionSize(), is(1));
		module.setSelected(0L, false);
		assertThat(module.isSelected(0L), is(false));
		assertThat(module.isSelected(1L), is(true));
		assertThat(module.getSelectionSize(), is(1));
		module.setSelected(1L, false);
		assertThat(module.isSelected(1L), is(false));
		assertThat(module.getSelectionSize(), is(0));
	}

	@Test public void testSetSelectedInMultipleMode() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		// Act + Assert:
		assertThat(module.isSelected(0L), is(false));
		module.setSelected(0L, true);
		assertThat(module.isSelected(0L), is(true));
		assertThat(module.getSelectionSize(), is(1));
		module.setSelected(1L, true);
		assertThat(module.isSelected(0L), is(true));
		assertThat(module.isSelected(1L), is(true));
		assertThat(module.getSelectionSize(), is(2));
		module.setSelected(0L, false);
		assertThat(module.isSelected(0L), is(false));
		assertThat(module.isSelected(1L), is(true));
		assertThat(module.getSelectionSize(), is(1));
	}

	@Test public void testSelectRange() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.setMode(SelectionModule.MULTIPLE);
		// Act:
		module.selectRange(0, 5);
		// Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			assertThat(module.isSelected(adapter.getItemId(i)), is(i < 5));
		}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSelectRangeOutOfBounds() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.setMode(SelectionModule.MULTIPLE);
		// Act:
		module.selectRange(0, adapter.getItemCount() + 10);
	}

	@Test(expected = IllegalStateException.class)
	public void testSelectRangeInSingleMode() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.selectRange(0, 1);
	}

	@Test(expected = IllegalStateException.class)
	public void testSelectRangeWithoutAttachedAdapter() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		// Act:
		module.selectRange(0, 10);
	}

	@Test(expected = IllegalStateException.class)
	public void testClearSelectionInRangeInSingleMode() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.clearSelectionInRange(0, 1);
	}

	@Test(expected = IllegalStateException.class)
	public void testClearSelectionInRangeWithoutAttachedAdapter() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		module.setSelected(0L, true);
		// Act:
		module.clearSelectionInRange(0, 10);
	}

	@Test public void testSetSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.setSelection(SELECTION_LIST);
		final List<Long> selection = module.getSelection();
		// Assert:
		assertThat(selection, is(notNullValue()));
		assertThat(selection.size(), is(SELECTION_ARRAY.length));
		for (int i = 0; i < SELECTION_ARRAY.length; i++) {
			assertThat(selection.get(i), is(SELECTION_ARRAY[i]));
		}
	}

	@Test public void testSetNullSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.setSelection(null);
		// Assert:
		assertThat(module.getSelection(), is(Collections.EMPTY_LIST));
	}

	@Test public void testClearSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setSelection(SELECTION_LIST);
		// Act:
		module.clearSelection();
		// Assert:
		assertThat(module.getSelectionSize(), is(0));
		assertThat(module.getSelection(), is(not(nullValue())));
	}

	@Test public void testClearSelectionInRange() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.setMode(SelectionModule.MULTIPLE);
		module.selectRange(0, adapter.getItemCount());
		// Act:
		module.clearSelectionInRange(adapter.getItemCount() - 5, 5);
		// Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			assertThat(module.isSelected(adapter.getItemId(i)), is(i < 5));
		}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testClearSelectionInRangeOutOfBounds() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.setMode(SelectionModule.MULTIPLE);
		module.setSelected(0L, true);
		// Act:
		module.clearSelectionInRange(0, adapter.getItemCount() + 10);
	}

	@Test public void testAdapterNotifications() {
		testAdapterNotifications(true);
		testAdapterNotifications(false);
	}

	private void testAdapterNotifications(boolean enabled) {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final Adapter adapter = new Adapter(getContext());
		module.attachToAdapter(adapter);
		module.setMode(SelectionModule.MULTIPLE);
		module.setAdapterNotificationEnabled(enabled);
		adapter.resetReceivedCallbacks();
		// Act + Assert:
		module.toggleSelection(0L);
		assertThat(adapter.notifyDataSetChangedReceived, is(enabled));
		module.clearSelection();
		// Set selected.
		adapter.resetReceivedCallbacks();
		module.setSelected(0L, true);
		assertThat(adapter.notifyDataSetChangedReceived, is(enabled));
		module.clearSelection();
		// Select all.
		adapter.resetReceivedCallbacks();
		module.selectAll();
		assertThat(adapter.notifyDataSetChangedReceived, is(enabled));
		module.clearSelection();
		// Select range.
		adapter.resetReceivedCallbacks();
		module.selectRange(0, 5);
		assertThat(adapter.notifyDataSetChangedReceived, is(enabled));
		module.clearSelection();
		// Clear selection.
		module.setSelected(0L, true);
		adapter.resetReceivedCallbacks();
		module.clearSelection();
		assertThat(adapter.notifyDataSetChangedReceived, is(enabled));
		module.clearSelection();
		// Clear selection in range.
		module.setSelection(SELECTION_LIST);
		adapter.resetReceivedCallbacks();
		module.clearSelectionInRange(0, SELECTION_LIST.size() - 1);
		assertThat(adapter.notifyDataSetChangedReceived, is(enabled));
		module.clearSelection();
	}

	@Test public void testSelect() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.select(1L);
		// Assert:
		assertThat(module.isSelected(1L), is(true));
	}

	@Test public void testSelectSame() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.select(1L);
		// Act:
		module.select(1L);
		// Assert:
		assertThat(module.isSelected(1L), is(true));
	}

	@Test public void testDeselect() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.select(1L);
		// Act:
		module.deselect(1L);
		// Assert:
		assertThat(module.isSelected(1L), is(false));
	}

	@Test public void testDeselectOnEmptySelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.deselect(1L);
	}

	@Test public void testRequiresStateSaving() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		// Act:
		module.select(1L);
		// Assert:
		assertThat(module.requiresStateSaving(), is(true));
	}

	@Test public void testRequiresStateSavingOnEmptySelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.select(1L);
		module.deselect(1L);
		// Act + Assert:
		assertThat(module.requiresStateSaving(), is(false));
	}

	@Test public void testSaveInstanceStateForSingleSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setSelected(1L, true);
		module.setSelected(2L, true);
		// Act:
		final Parcelable state = module.saveInstanceState();
		// Assert:
		assertThat(state, is(not(nullValue())));
		assertThat(state, instanceOf(SelectionModule.SavedState.class));
		final SelectionModule.SavedState savedState = (SelectionModule.SavedState) state;
		assertThat(savedState.mode, is(SelectionModule.SINGLE));
		assertThat(savedState.selection, is(new long[]{2L}));
	}

	@Test public void testSaveInstanceStateForMultiSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		module.setSelection(SELECTION_LIST);
		// Act:
		final Parcelable state = module.saveInstanceState();
		// Assert:
		assertThat(state, is(not(nullValue())));
		assertThat(state, instanceOf(SelectionModule.SavedState.class));
		final SelectionModule.SavedState savedState = (SelectionModule.SavedState) state;
		assertThat(savedState.mode, is(SelectionModule.MULTIPLE));
		assertThat(savedState.selection, is(SELECTION_ARRAY));
	}

	@Test public void testSaveInstanceStateOnEmptySelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.select(1L);
		module.deselect(1L);
		// Act:
		final Parcelable state = module.saveInstanceState();
		// Assert:
		assertThat(state, is(not(nullValue())));
		assertThat(state, instanceOf(SelectionModule.SavedState.class));
	}

	@Test public void testRestoreInstanceState() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final SelectionModule.SavedState savedState = new SelectionModule.SavedState(SelectionModule.SavedState.EMPTY_STATE);
		savedState.mode = SelectionModule.MULTIPLE;
		savedState.selection = SELECTION_ARRAY;
		// Act:
		module.restoreInstanceState(savedState);
		// Assert:
		assertThat(module.getMode(), is(SelectionModule.MULTIPLE));
		assertThat(module.getSelectionSize(), is(SELECTION_ARRAY.length));
		assertThat(module.getSelection(), is(SELECTION_LIST));
		for (final long aSSelectionArray : SELECTION_ARRAY) {
			assertThat(module.isSelected(aSSelectionArray), is(true));
		}
	}

	@Test public void testRestoreInstanceStateWithoutSavedSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		final SelectionModule.SavedState savedState = new SelectionModule.SavedState(SelectionModule.SavedState.EMPTY_STATE);
		savedState.mode = SelectionModule.MULTIPLE;
		// Act:
		module.restoreInstanceState(savedState);
		// Assert:
		assertThat(module.getMode(), is(SelectionModule.MULTIPLE));
		assertThat(module.getSelectionSize(), is(0));
		assertThat(module.getSelection(), is(Collections.EMPTY_LIST));
	}

	@Test public void testRestoreInstanceStateWithInitialSelection() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.setMode(SelectionModule.MULTIPLE);
		module.setSelected(1000L, true);
		module.setSelected(2000L, true);
		final SelectionModule.SavedState savedState = new SelectionModule.SavedState(SelectionModule.SavedState.EMPTY_STATE);
		savedState.mode = SelectionModule.MULTIPLE;
		savedState.selection = SELECTION_ARRAY;
		// Act:
		module.restoreInstanceState(savedState);
		// Assert:
		assertThat(module.getMode(), is(SelectionModule.MULTIPLE));
		assertThat(module.getSelectionSize(), is(SELECTION_ARRAY.length + 2));
		assertThat(module.isSelected(1000L), is(true));
		assertThat(module.isSelected(2000L), is(true));
		for (final long aSSelectionArray : SELECTION_ARRAY) {
			assertThat(module.isSelected(aSSelectionArray), is(true));
		}
	}

	@Test public void testRestoreInstanceStateWithNotSelectionState() {
		// Arrange:
		final SelectionModule module = new SelectionModule();
		module.restoreInstanceState(mock(Parcelable.class));
		assertThat(module.getSelectionSize(), is(0));
		module.select(1L);
		// Act:
		module.restoreInstanceState(mock(Parcelable.class));
		// Assert:
		assertThat(module.getSelectionSize(), is(1));
		assertThat(module.isSelected(1L), is(true));
	}

	@Test public void testSavedStateCreatorCreateFromParcel() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		parcel.writeParcelable(null, 0);
		parcel.writeInt(SelectionModule.MULTIPLE);
		parcel.writeLongArray(SELECTION_ARRAY);
		parcel.setDataPosition(0);
		// Act:
		final SelectionModule.SavedState savedState = SelectionModule.SavedState.CREATOR.createFromParcel(parcel);
		// Assert:
		assertThat(savedState, is(not(nullValue())));
		assertThat(savedState.mode, is(SelectionModule.MULTIPLE));
		assertThat(savedState.selection, is(SELECTION_ARRAY));
		parcel.recycle();
	}

	@Test public void testSavedStateCreatorNewArray() {
		// Act:
		final SelectionModule.SavedState[] savedStates = SelectionModule.SavedState.CREATOR.newArray(5);
		// Assert:
		assertThat(savedStates.length, is(5));
		for (final SelectionModule.SavedState savedState : savedStates) {
			assertThat(savedState, is(nullValue()));
		}
	}

	@Test public void testSavedStateWriteToParcel() {
		// Arrange:
		final Parcel parcel = Parcel.obtain();
		final SelectionModule.SavedState savedState = new SelectionModule.SavedState(SelectionModule.SavedState.EMPTY_STATE);
		savedState.mode = SelectionModule.MULTIPLE;
		savedState.selection = SELECTION_ARRAY;
		// Act:
		savedState.writeToParcel(parcel, 0);
		// Assert:
		parcel.setDataPosition(0);
		parcel.readParcelable(SelectionModule.SavedState.class.getClassLoader());
		assertThat(parcel.readInt(), is(SelectionModule.MULTIPLE));
		final long[] selection = new long[SELECTION_ARRAY.length];
		parcel.readLongArray(selection);
		assertThat(selection, is(SELECTION_ARRAY));
		parcel.recycle();
	}

	@Test public void testSavedStateDescribeContents() {
		// Arrange:
		final SelectionModule.SavedState savedState = new SelectionModule.SavedState(SelectionModule.SavedState.EMPTY_STATE){};
		// Act + Assert:
		assertThat(savedState.describeContents(), is(0));
	}

	private static final class Adapter extends SimpleListAdapter<Adapter, ViewHolder, String> implements AdapterModule.ModuleAdapter {

		boolean notifyDataSetChangedReceived;

		Adapter(final Context context) {
			super(context, new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});
		}

		void resetReceivedCallbacks() {
			this.notifyDataSetChangedReceived = false;
		}

		@Override public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
			this.notifyDataSetChangedReceived = true;
		}
	}
}