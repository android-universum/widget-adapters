Widget-Adapters-Module-Selection
===============

This module contains `AdapterModule` implementation that may be used within a desired widget adapter
to add **items selection** functionality for that adapter.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-module-selection:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state),
[widget-adapters-module-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SelectionModule](https://bitbucket.org/android-universum/widget-adapters/src/main/library-module-selection/src/main/java/universum/studios/android/widget/adapter/module/SelectionModule.java)