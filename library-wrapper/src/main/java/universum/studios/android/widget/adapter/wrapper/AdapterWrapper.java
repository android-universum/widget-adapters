/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.wrapper;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.WrapperListAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link WrapperListAdapter} implementation that may be used to wrap instance of {@link ListAdapter}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class AdapterWrapper implements WrapperListAdapter {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "AdapterWrapper";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Wrapped instance of ListAdapter.
	 */
	protected final ListAdapter adapter;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of AdapterWrapper which wraps the given <var>adapter</var>.
	 *
	 * @param adapter An instance of the adapter to be wrapped. May be {@code null} to create empty
	 *                adapter wrapper.
	 */
	public AdapterWrapper(@Nullable final ListAdapter adapter) {
		this.adapter = adapter;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override @Nullable public ListAdapter getWrappedAdapter() {
		return adapter;
	}

	/**
	 */
	@Override public void registerDataSetObserver(@NonNull final DataSetObserver observer) {
		if (adapter != null) adapter.registerDataSetObserver(observer);
	}

	/**
	 */
	@Override public void unregisterDataSetObserver(@NonNull final DataSetObserver observer) {
		if (adapter != null) adapter.unregisterDataSetObserver(observer);
	}

	/**
	 */
	@Override public boolean isEmpty() {
		return adapter == null || adapter.isEmpty();
	}

	/**
	 */
	@Override public int getCount() {
		return adapter == null ? 0 : adapter.getCount();
	}

	/**
	 */
	@Override @Nullable public Object getItem(final int position) {
		return adapter == null ? null : adapter.getItem(position);
	}

	/**
	 */
	@Override public boolean hasStableIds() {
		return adapter != null && adapter.hasStableIds();
	}

	/**
	 */
	@Override public long getItemId(final int position) {
		return adapter == null ? -1 : adapter.getItemId(position);
	}

	/**
	 */
	@Override public int getViewTypeCount() {
		return adapter == null ? 0 : adapter.getViewTypeCount();
	}

	/**
	 */
	@Override public int getItemViewType(final int position) {
		return adapter == null ? 0 : adapter.getItemViewType(position);
	}

	/**
	 */
	@Override public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
		return adapter == null ? null : adapter.getView(position, convertView, parent);
	}

	/**
	 */
	@Override public boolean areAllItemsEnabled() {
		return adapter != null && adapter.areAllItemsEnabled();
	}

	/**
	 */
	@Override public boolean isEnabled(final int position) {
		return adapter != null && adapter.isEnabled(position);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}