/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.wrapper;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.junit.Test;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class AdapterWrapperTest extends AndroidTestCase {

	private ViewGroup container;

	public void beforeTest() {
		this.container = new FrameLayout(getContext());
	}

	public void afterTest() {
		this.container = null;
	}

	@Test public void testGetWrappedAdapter() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		// Act + Assert:
		assertThat(wrapper.getWrappedAdapter(), is(notNullValue()));
	}

	@Test public void testGetWrappedAdapterOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		assertThat(wrapper.getWrappedAdapter(), is(nullValue()));
	}

	@Test public void testRegisterDataSetObserver() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		final DataSetObserver dataSetObserver = new DataSetObserver() {};
		// Act:
		wrapper.registerDataSetObserver(dataSetObserver);
		// Assert:
		verify(mockAdapter).registerDataSetObserver(dataSetObserver);
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testRegisterDataSetObserverOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		final DataSetObserver dataSetObserver = new DataSetObserver() {};
		// Act:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		wrapper.registerDataSetObserver(dataSetObserver);
	}

	@Test public void testUnregisterDataSetObserver() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		final DataSetObserver dataSetObserver = new DataSetObserver() {};
		// Act:
		wrapper.unregisterDataSetObserver(dataSetObserver);
		// Assert:
		verify(mockAdapter).unregisterDataSetObserver(dataSetObserver);
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testUnregisterDataSetObserverOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		final DataSetObserver dataSetObserver = new DataSetObserver() {};
		// Act:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		wrapper.unregisterDataSetObserver(dataSetObserver);
	}

	@Test public void testIsEmptyIfFalse() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.isEmpty()).thenReturn(false);
		// Act + Assert:
		assertThat(wrapper.isEmpty(), is(false));
		verify(mockAdapter).isEmpty();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testIsEmptyIfTrue() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.isEmpty()).thenReturn(true);
		// Act + Assert:
		assertThat(wrapper.isEmpty(), is(true));
		verify(mockAdapter).isEmpty();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testIsEmptyOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		assertThat(wrapper.isEmpty(), is(true));
	}

	@Test public void testGetCount() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.getCount()).thenReturn(100);
		// Act + Assert:
		assertThat(wrapper.getCount(), is(100));
		verify(mockAdapter).getCount();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testGetCountOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		assertThat(wrapper.getCount(), is(0));
	}

	@Test public void testGetItem() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		for (int i = 0; i < 10; i++) {
			when(mockAdapter.getItem(i)).thenReturn("Item at: " + i);
			// Act + Assert:
			assertThat(wrapper.getItem(i), is("Item at: " + i));
			verify(mockAdapter).getItem(i);
		}
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testGetItemOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		for (int i = 0; i < 20; i++) {
			assertThat(wrapper.getItem(i), is(nullValue()));
		}
	}

	@Test public void testHasStableIdsWithFalse() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.hasStableIds()).thenReturn(false);
		// Act + Assert:
		assertThat(wrapper.hasStableIds(), is(false));
		verify(mockAdapter).hasStableIds();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testHasStableIdsWithTrue() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.hasStableIds()).thenReturn(true);
		// Act + Assert:
		assertThat(wrapper.hasStableIds(), is(true));
		verify(mockAdapter).hasStableIds();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testHasStableIdsOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		assertThat(wrapper.hasStableIds(), is(false));
	}

	@Test public void testGetItemId() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		for (int i = 0; i < 30; i++) {
			when(mockAdapter.getItemId(i)).thenReturn(i + 1L);
			// Act + Assert:
			assertThat(wrapper.getItemId(i), is(i + 1L));
			verify(mockAdapter).getItemId(i);
		}
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testGetItemIdOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		for (int i = 0; i < 20; i++) {
			assertThat(wrapper.getItemId(i), is(-1L));
		}
	}

	@Test public void testGetViewTypeCount() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.getViewTypeCount()).thenReturn(1);
		// Act + Assert:
		assertThat(wrapper.getViewTypeCount(), is(1));
		verify(mockAdapter).getViewTypeCount();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testGetViewTypeCountOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		assertThat(wrapper.getViewTypeCount(), is(0));
	}

	@Test public void testGetItemViewType() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		for (int i = 0; i < 36; i++) {
			when(mockAdapter.getItemViewType(i)).thenReturn(i % 3 == 0 ? 1 : 0);
			// Act + Assert:
			assertThat(wrapper.getItemViewType(i), is(i % 3 == 0 ? 1 : 0));
			verify(mockAdapter).getItemViewType(i);
		}
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testGetItemViewTypeOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		assertThat(wrapper.getItemViewType(0), is(0));
	}

	@Test public void testGetView() {
		// Arrange:
		final Context context = getContext();
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		for (int i = 0; i < 15; i++) {
			final View view = new TextView(context);
			when(mockAdapter.getView(i, null, container)).thenReturn(view);
			// Act + Assert:
			assertThat(wrapper.getView(i, null, container), is(view));
		}
	}

	@Test public void testGetViewOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		for (int i = 0; i < 20; i++) {
			assertThat(wrapper.getView(0, null, container), is(nullValue()));
		}
	}

	@Test public void testAreAllItemsEnabledWithFalse() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.areAllItemsEnabled()).thenReturn(false);
		// Act + Assert:
		assertThat(wrapper.areAllItemsEnabled(), is(false));
		verify(mockAdapter).areAllItemsEnabled();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testAreAllItemsEnabledWithTrue() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		when(mockAdapter.areAllItemsEnabled()).thenReturn(true);
		// Act + Assert:
		assertThat(wrapper.areAllItemsEnabled(), is(true));
		verify(mockAdapter).areAllItemsEnabled();
		verifyNoMoreInteractions(mockAdapter);
	}

	@Test public void testAreAllItemsEnabledOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		assertThat(wrapper.areAllItemsEnabled(), is(false));
	}

	@Test public void testIsEnabled() {
		// Arrange:
		final ListAdapter mockAdapter = mock(ListAdapter.class);
		final AdapterWrapper wrapper = new AdapterWrapper(mockAdapter);
		for (int i = 0; i < 10; i++) {
			when(mockAdapter.isEnabled(i)).thenReturn(i % 2 == 0);
			// Act + Assert:
			assertThat(wrapper.isEnabled(i), is(i % 2 == 0));
			verify(mockAdapter).isEnabled(i);
		}
	}

	@Test public void testIsEnabledOnEmptyWrapper() {
		// Arrange:
		final AdapterWrapper wrapper = new AdapterWrapper(null);
		// Act + Assert:
		// Only ensure that the wrapper does not throw a NullPointerException exception.
		for (int i = 0; i < 20; i++) {
			assertThat(wrapper.isEnabled(i), is(false));
		}
	}
}