Widget-Adapters-Wrapper
===============

This module contains implementation of `WrapperListAdapter` that may be also used as **mock** adapter
for either `ListView` or `GridView`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-wrapper:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [AdapterWrapper](https://bitbucket.org/android-universum/widget-adapters/src/main/library-wrapper/src/main/java/universum/studios/android/widget/adapter/wrapper/AdapterWrapper.java)