Widget-Adapters-Spinner-Simple
===============

This module contains **simple** adapter implementation for `Spinner` widget. This adapter class
requires only the **creation** and **binding** logic of its corresponding item views to be implemented.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-spinner-simple:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core),
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state),
[widget-adapters-list-base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list-base),
[widget-adapters-spinner-base](https://bitbucket.org/android-universum/widget-adapters/src/main/library-spinner-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SimpleSpinnerAdapter](https://bitbucket.org/android-universum/widget-adapters/src/main/library-spinner-simple/src/main/java/universum/studios/android/widget/adapter/SimpleSpinnerAdapter.java)