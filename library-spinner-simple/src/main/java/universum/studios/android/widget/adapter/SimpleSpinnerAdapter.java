/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.widget.adapter.holder.AdapterHolder;
import universum.studios.android.widget.adapter.holder.ViewHolder;
import universum.studios.android.widget.adapter.holder.ViewHolderBinders;
import universum.studios.android.widget.adapter.holder.ViewHolderFactories;

/**
 * A {@link BaseSpinnerAdapter} and {@link SimpleAdapter} implementation which provides API for simple
 * management of items data set.
 *
 * <h3>Implementation</h3>
 * Inheritance hierarchies of this adapter class are not required to implement any methods except
 * {@link #onCreateViewHolder(android.view.ViewGroup, int)} or {@link #onBindViewHolder(ViewHolder, int)}
 * along with {@link #onCreateDropDownViewHolder(android.view.ViewGroup, int)} or
 * {@link #onBindDropDownViewHolder(ViewHolder, int)} if the creation and binding logic provided by
 * default holder factories and default holder binders (see section below) are not desired and no
 * custom factories nor binders are specified.
 *
 * <h3>Default Holder Factory and Binder</h3>
 * This adapter uses implementation of {@link AdapterHolder.Factory} provided via
 * {@link ViewHolderFactories#simple()} as its default holder factory and implementation of
 * {@link AdapterHolder.Binder} provided via {@link ViewHolderBinders#simple()} as its default holder
 * binder.
 *
 * <h3>Default Drop Down Holder Factory and Binder</h3>
 * This adapter also uses implementation of {@link AdapterHolder.Factory} provided via
 * {@link ViewHolderFactories#resource(int)} with {@link android.R.layout#simple_spinner_dropdown_item android:simple_spinner_dropdown_item}
 * layout resource as its default drop down holder factory and implementation of
 * {@link AdapterHolder.Binder} provided via {@link ViewHolderBinders#simple()} as its default drop
 * down holder binder.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SimpleSpinnerAdapter<A extends SimpleSpinnerAdapter, VH extends ViewHolder, DVH extends ViewHolder, I>
		extends BaseSpinnerAdapter<A, VH, DVH, I>
		implements
		SimpleAdapter<I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleSpinnerAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Data set containing current data of this adapter.
	 */
	private final SimpleAdapterDataSet<I> dataSet;

	/**
	 * Registry with {@link OnDataSetSwapListener OnDataSetSwapListeners} that are attached to this
	 * adapter.
	 */
	private SwappableDataSetAdapterListeners<List<I>> listeners;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleSpinnerAdapter with empty data set.
	 *
	 * @param context Context in which will be this adapter used.
	 *
	 * @see #SimpleSpinnerAdapter(Context, Object[])
	 * @see #SimpleSpinnerAdapter(Context, List)
	 */
	@SuppressWarnings("unchecked")
	public SimpleSpinnerAdapter(@NonNull final Context context) {
		this(context, new SimpleAdapterDataSet<I>());
	}

	/**
	 * Same as {@link #SimpleSpinnerAdapter(Context, List)} for array of initial <var>items</var>
	 * data set.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param items Array of items to be used as initial data set for this adapter.
	 */
	public SimpleSpinnerAdapter(@NonNull Context context, @NonNull I[] items) {
		this(context, Arrays.asList(items));
	}

	/**
	 * Creates a new instance of SimpleSpinnerAdapter with the given initial <var>items</var>
	 * data set.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param items   List of items to be used as initial data set for this adapter.
	 */
	public SimpleSpinnerAdapter(@NonNull Context context, @NonNull List<I> items) {
		this(context, new SimpleAdapterDataSet<>(items));
	}

	/**
	 * Creates a new instance of SimpleSpinnerAdapter with the specified data set.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param dataSet The data set that will contain items of the new adapter.
	 */
	@VisibleForTesting
	@SuppressWarnings("unchecked")
	SimpleSpinnerAdapter(final Context context, final SimpleAdapterDataSet<I> dataSet) {
		super(context);
		this.dataSet = dataSet;
		this.dataSet.setItemsCallback(new SimpleAdapterDataSet.SimpleItemsCallback() {

			/**
			 */
			@Override void onItemsChanged() {
				notifyDataSetChanged();
			}
		});
		this.dataSet.setItemPositionResolver(new DataSetItemPositionResolver(this));
		setHolderFactory((AdapterHolder.Factory<VH>) ViewHolderFactories.simple());
		setHolderBinder(ViewHolderBinders.<A, VH>simple());
		setDropDownHolderFactory((AdapterHolder.Factory<DVH>) ViewHolderFactories.resource(android.R.layout.simple_spinner_dropdown_item));
		setDropDownHolderBinder(ViewHolderBinders.<A, DVH>simple());
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void registerOnDataSetSwapListener(@NonNull OnDataSetSwapListener<List<I>> listener) {
		if (listeners == null) {
			this.listeners = new SwappableDataSetAdapterListeners<>();
		}
		this.listeners.registerOnDataSetSwapListener(listener);
	}

	/**
	 */
	@Override public void unregisterOnDataSetSwapListener(@NonNull OnDataSetSwapListener<List<I>> listener) {
		if (listeners != null) this.listeners.unregisterOnDataSetSwapListener(listener);
	}

	/**
	 */
	@Override public void changeItems(@Nullable final List<I> items) {
		swapItems(items);
	}

	/**
	 */
	@Override @Nullable public List<I> swapItems(@Nullable final List<I> items) {
		notifyItemsSwapStarted(items);
		final List<I> oldItems = onSwapItems(items);
		notifyItemsSwapFinished(items);
		notifyDataSetChanged();
		return oldItems;
	}

	/**
	 * Notifies all registered {@link OnDataSetSwapListener OnDataSetSwapListeners} that swapping
	 * of items for this adapter has been started.
	 *
	 * @param items The items that are about to be swapped for this adapter.
	 *
	 * @see #notifyItemsSwapFinished(List)
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	protected final void notifyItemsSwapStarted(@Nullable List<I> items) {
		if (listeners != null) this.listeners.notifyDataSetSwapStarted(items);
	}

	/**
	 * Called from {@link #swapItems(List)} in order to swap data set of items of this adapter to
	 * the given <var>items</var>.
	 *
	 * @param items The new items data set to be changed for this adapter.
	 * @return Old items data set that has been attached to this adapter before. May be {@code null}
	 * if there were no items attached.
	 */
	@Nullable protected List<I> onSwapItems(@Nullable final List<I> items) {
		return dataSet.swapItems(items);
	}

	/**
	 * Notifies all registered {@link OnDataSetSwapListener OnDataSetSwapListeners} that swapping
	 * of items for this adapter has been finished.
	 *
	 * @param items The items that has been swapped for this adapter.
	 *
	 * @see #notifyItemsSwapStarted(List)
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	protected final void notifyItemsSwapFinished(@Nullable List<I> items) {
		if (listeners != null) this.listeners.notifyDataSetSwapFinished(items);
	}

	/**
	 */
	@Override public void insertItem(@NonNull final I item) {
		this.dataSet.insertItem(item);
	}

	/**
	 */
	@Override public void insertItem(final int position, @NonNull final I item) {
		this.dataSet.insertItem(position, item);
	}

	/**
	 */
	@Override public void insertItems(@NonNull final List<I> items) {
		this.dataSet.insertItems(items);
	}

	/**
	 */
	@Override public void insertItems(final int positionStart, @NonNull final List<I> items) {
		this.dataSet.insertItems(positionStart, items);
	}

	/**
	 */
	@Override @NonNull public I swapItem(final int position, @NonNull final I item) {
		return dataSet.swapItem(position, item);
	}

	/**
	 */
	@Override @NonNull public I swapItemById(final long itemId, @NonNull final I item) {
		return dataSet.swapItemById(itemId, item);
	}

	/**
	 */
	@Override public void moveItem(final int fromPosition, final int toPosition) {
		this.dataSet.moveItem(fromPosition, toPosition);
	}

	/**
	 */
	@Override @NonNull public I removeItem(final int position) {
		return dataSet.removeItem(position);
	}

	/**
	 */
	@Override public int removeItem(@NonNull final I item) {
		return dataSet.removeItem(item);
	}

	/**
	 */
	@Override @NonNull public I removeItemById(final long itemId) {
		return dataSet.removeItemById(itemId);
	}

	/**
	 */
	@Override @NonNull public List<I> removeItems(final int positionStart, final int itemCount) {
		return dataSet.removeItems(positionStart, itemCount);
	}

	/**
	 */
	@Override @Nullable public List<I> getItems() {
		return dataSet.getItems();
	}

	/**
	 */
	@Override public int getItemCount() {
		return dataSet.getItemCount();
	}

	/**
	 */
	@Override public boolean hasItemAt(final int position) {
		return dataSet.hasItemAt(position);
	}

	/**
	 */
	@Override @NonNull public I getItem(final int position) {
		return dataSet.getItem(position);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}