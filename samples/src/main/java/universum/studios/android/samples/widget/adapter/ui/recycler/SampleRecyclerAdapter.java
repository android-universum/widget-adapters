/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.recycler;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.databinding.ItemListWithActionBinding;
import universum.studios.android.widget.adapter.SimpleRecyclerAdapter;
import universum.studios.android.widget.adapter.holder.RecyclerViewHolder;

/**
 * @author Martin Albedinsky
 */
final class SampleRecyclerAdapter extends SimpleRecyclerAdapter<SampleRecyclerAdapter, SampleRecyclerAdapter.ItemHolder, String> {

	static final int CLICK = 0x00;
	static final int ACTION = 0x01;

	SampleRecyclerAdapter(@NonNull final Context context) {
		super(context);
	}

	@Override public ItemHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
		return new ItemHolder(inflateView(R.layout.item_list_with_action, parent));
	}

	@Override public void onBindViewHolder(@NonNull final ItemHolder holder, final int position) {
		holder.binding.setText(getItem(position));
		holder.binding.executePendingBindings();
	}

	final class ItemHolder extends RecyclerViewHolder implements View.OnClickListener {

		final ItemListWithActionBinding binding;

		ItemHolder(@NonNull final View itemView) {
			super(itemView);
			this.binding = ItemListWithActionBinding.bind(itemView);
			itemView.setOnClickListener(this);
			itemView.findViewById(R.id.action).setOnClickListener(this);
		}

		@Override public void onClick(@NonNull final View view) {
			switch (view.getId()) {
				case R.id.action:
					notifyDataSetActionSelected(ACTION, getAdapterPosition(), null);
					break;
				default:
					if (view.equals(itemView)) {
						notifyDataSetActionSelected(CLICK, getAdapterPosition(), null);
					}
			}
		}
	}
}