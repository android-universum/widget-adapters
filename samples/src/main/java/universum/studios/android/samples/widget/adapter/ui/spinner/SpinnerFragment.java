/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.spinner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.ui.DataSetAdapterFragment;
import universum.studios.android.samples.widget.adapter.ui.DataSets;

/**
 * @author Martin Albedinsky
 */
public final class SpinnerFragment extends DataSetAdapterFragment<SampleSpinnerAdapter, List<String>>
		implements
		AdapterView.OnItemSelectedListener {

	@Override
	@Nullable
	public View onCreateView(
			@NonNull final LayoutInflater inflater,
			@Nullable final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.fragment_spinner, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final Spinner spinner = view.findViewById(R.id.spinner);
		spinner.setOnItemSelectedListener(this);

		final SampleSpinnerAdapter adapter = new SampleSpinnerAdapter(requireActivity());
		adapter.registerOnDataSetSwapListener(this);
		adapter.registerOnDataSetListener(this);
		spinner.setAdapter(adapter);
		adapter.changeItems(DataSets.textItems(20));
	}

	@Override
	public void onItemSelected(
			@NonNull final AdapterView<?> parent,
			@Nullable final View view,
			final int position,
			final long id
	) {
		Toast.makeText(getActivity(), "Selected " + parent.getAdapter().getItem(position), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNothingSelected(@NonNull final AdapterView<?> parent) {
		Toast.makeText(getActivity(), "Nothing selected", Toast.LENGTH_SHORT).show();
	}
}