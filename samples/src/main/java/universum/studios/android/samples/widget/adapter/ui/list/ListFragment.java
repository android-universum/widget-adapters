/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.list;

import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import java.util.*;

import androidx.annotation.*;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.ui.*;
import universum.studios.android.widget.adapter.*;

/**
 * @author Martin Albedinsky
 */
public final class ListFragment extends DataSetAdapterFragment<SampleListAdapter, List<String>>
		implements
		OnAdapterDataSetSwapListener<SampleListAdapter, List<String>>,
		OnAdapterDataSetListener<SampleListAdapter>,
		OnAdapterDataSetActionListener<SampleListAdapter>,
		AdapterView.OnItemClickListener {

	@SuppressWarnings("unused") private static final String TAG = "ListFragment";

	@Override
	@Nullable
	public View onCreateView(
			@NonNull final LayoutInflater inflater,
			@Nullable final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.fragment_list, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final SampleListAdapter adapter = new SampleListAdapter(requireActivity());
		adapter.registerOnDataSetSwapListener(this);
		adapter.registerOnDataSetSwapListener(OnAdapterDataSetSwapListener.Delegate.create(adapter, this));
		adapter.registerOnDataSetListener(this);
		adapter.registerOnDataSetListener(OnAdapterDataSetListener.Delegate.create(adapter, this));
		adapter.registerOnDataSetActionListener(this);
		adapter.registerOnDataSetActionListener(OnAdapterDataSetActionListener.Delegate.create(adapter, this));
		adapter.changeItems(DataSets.textItems(100));

		final View emptyView = view.findViewById(android.R.id.empty);

		final ListView listView = view.findViewById(android.R.id.list);
		listView.setEmptyView(emptyView);
		listView.setOnItemClickListener(this);
		listView.setAdapter(adapter);
	}

	@Override
	public void onItemClick(@NonNull final AdapterView<?> parent, @Nullable final View view, final int position, final long id) {
		Toast.makeText(parent.getContext(), "Clicked " + parent.getAdapter().getItem(position), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDataSetSwapStarted(@NonNull final SampleListAdapter adapter, @Nullable final List<String> data) {
		Log.d(TAG, "onDataSetSwapStarted(adapter: " + adapter.getClass() + ", data: " + data + ")");
	}

	@Override
	public void onDataSetSwapFinished(@NonNull final SampleListAdapter adapter, @Nullable final List<String> data) {
		Log.d(TAG, "onDataSetSwapFinished(adapter: " + adapter.getClass() + ", data: " + data + ")");
	}

	@Override
	public void onDataSetChanged(@NonNull final SampleListAdapter adapter) {
		Log.d(TAG, "onDataSetChanged(adapter: " + adapter.getClass() + ")");
	}

	@Override
	public void onDataSetInvalidated(@NonNull final SampleListAdapter adapter) {
		Log.d(TAG, "onDataSetInvalidated(adapter: " + adapter.getClass() + ")");
	}

	@Override
	public boolean onDataSetActionSelected(
			@NonNull final SampleListAdapter adapter,
			final int action,
			final int position,
			final long id,
			@Nullable final Object payload
	) {
		Log.d(TAG, "onDataSetActionSelected(adapter: " + adapter.getClass() + ", action: " + action + ", position: " + position + ", id: " + id + ", payload: " + payload + ")");

		if (action == SampleListAdapter.ACTION) {
			adapter.removeItem(position);

			return true;
		}

		return false;
	}
}