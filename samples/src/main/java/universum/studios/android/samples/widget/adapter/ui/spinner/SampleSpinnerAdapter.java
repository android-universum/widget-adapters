/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.spinner;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.widget.TextView;

import androidx.annotation.NonNull;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.widget.adapter.SimpleSpinnerAdapter;
import universum.studios.android.widget.adapter.holder.ViewHolder;

/**
 * @author Martin Albedinsky
 */
final class SampleSpinnerAdapter extends SimpleSpinnerAdapter<SampleSpinnerAdapter, ViewHolder, ViewHolder, String> {

	private final int selectedColor;

	@SuppressWarnings("deprecation")
	SampleSpinnerAdapter(@NonNull final Context context) {
		super(context);
		final TypedValue typedValue = new TypedValue();
		if (context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true)) {
			this.selectedColor = typedValue.data;
		} else {
			this.selectedColor = context.getResources().getColor(R.color.samples_accent);
		}
	}

	@Override protected void onBindDropDownViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
		super.onBindDropDownViewHolder(viewHolder, position);
		((TextView) viewHolder.itemView).setTextColor(getSelectedPosition() == position ? selectedColor : Color.BLACK);
	}
}