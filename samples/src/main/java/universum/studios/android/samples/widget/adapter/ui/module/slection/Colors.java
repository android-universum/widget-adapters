/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.module.slection;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * @author Martin Albedinsky
 */
final class Colors {

	private Colors() {}

	@NonNull static List<Integer> dataSet() {
		final List<Integer> colors = new ArrayList<>(10);
		colors.add(Color.parseColor("#F44336"));
		colors.add(Color.parseColor("#E91E63"));
		colors.add(Color.parseColor("#9C27B0"));
		colors.add(Color.parseColor("#673AB7"));
		colors.add(Color.parseColor("#3F51B5"));
		colors.add(Color.parseColor("#2196F3"));
		colors.add(Color.parseColor("#673AB7"));
		colors.add(Color.parseColor("#03A9F4"));
		colors.add(Color.parseColor("#00BCD4"));
		colors.add(Color.parseColor("#009688"));
		colors.add(Color.parseColor("#4CAF50"));
		colors.add(Color.parseColor("#8BC34A"));
		colors.add(Color.parseColor("#CDDC39"));
		colors.add(Color.parseColor("#FFEB3B"));
		colors.add(Color.parseColor("#FFC107"));
		colors.add(Color.parseColor("#FF9800"));
		colors.add(Color.parseColor("#FF5722"));
		colors.add(Color.parseColor("#795548"));
		colors.add(Color.parseColor("#9E9E9E"));
		colors.add(Color.parseColor("#607D8B"));
		return colors;
	}
}