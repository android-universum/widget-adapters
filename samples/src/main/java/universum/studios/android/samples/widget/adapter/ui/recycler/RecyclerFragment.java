/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.recycler;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.ui.DataSetAdapterFragment;
import universum.studios.android.samples.widget.adapter.ui.DataSets;

/**
 * @author Martin Albedinsky
 */
public final class RecyclerFragment extends DataSetAdapterFragment<SampleRecyclerAdapter, List<String>> {

	private SampleRecyclerAdapter adapter;

	@Override
	@Nullable
	public View onCreateView(
			@NonNull final LayoutInflater inflater,
			@Nullable final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.fragment_recycler, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		adapter = new SampleRecyclerAdapter(requireActivity());
		adapter.registerOnDataSetSwapListener(this);
		adapter.registerOnDataSetListener(this);
		adapter.registerOnDataSetActionListener(this);
		adapter.changeItems(DataSets.textItems(100));

		final RecyclerView recyclerView = view.findViewById(android.R.id.list);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
		recyclerView.setAdapter(adapter);
	}

	@Override
	public void onDataSetChanged() {
		final View emptyView = requireView().findViewById(android.R.id.empty);

		emptyView.setVisibility(adapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);
	}

	@Override
	public boolean onDataSetActionSelected(
			final int action,
			final int position,
			final long id,
			@Nullable final Object payload
	) {
		switch (action) {
			case SampleRecyclerAdapter.CLICK -> {
				Toast.makeText(getActivity(), "Clicked " + adapter.getItem(position), Toast.LENGTH_SHORT).show();

				return true;
			}
			case SampleRecyclerAdapter.ACTION -> {
				adapter.removeItem(position);

				return true;
			}
			default -> {
				return super.onDataSetActionSelected(action, position, id, payload);
			}
		}
	}
}