/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.module.header;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.widget.adapter.SimpleRecyclerAdapter;
import universum.studios.android.widget.adapter.holder.RecyclerViewHolder;
import universum.studios.android.widget.adapter.module.AdapterModule;
import universum.studios.android.widget.adapter.module.AlphabeticHeaders;

/**
 * @author Martin Albedinsky
 */
final class CitiesAdapter extends SimpleRecyclerAdapter<CitiesAdapter, RecyclerViewHolder, City> implements AdapterModule.ModuleAdapter {

	private static final int VIEW_TYPE_HEADER = 0;
	private static final int VIEW_TYPE_ITEM = 1;

	private final AlphabeticHeaders<RecyclerViewHolder> mHeaders;

	CitiesAdapter(@NonNull final Context context) {
		super(context);
		this.mHeaders = new AlphabeticHeaders<RecyclerViewHolder>() {

			@Override @NonNull public RecyclerViewHolder createViewHolder(@NonNull final ViewGroup parent, final int viewType) {
				return new RecyclerViewHolder(inflateView(R.layout.header_list, parent));
			}

			@SuppressWarnings("ConstantConditions")
			@Override public void bindViewHolder(@NonNull final RecyclerViewHolder viewHolder, final int position) {
				((TextView) viewHolder.itemView).setText(mHeaders.getHeader(position).getText());
			}
		};
		this.mHeaders.attachToAdapter(this);
	}

	@Override @Nullable protected List<City> onSwapItems(@Nullable final List<City> items) {
		mHeaders.clearHeaders();
		if (items != null) {
			mHeaders.fromAlphabeticCollection(items);
		}
		return super.onSwapItems(items);
	}

	@Override public int getItemCount() {
		return super.getItemCount() + mHeaders.size();
	}

	@Override @NonNull public City getItem(final int position) {
		return super.getItem(mHeaders.correctPosition(position));
	}

	@Override public int getItemViewType(final int position) {
		return mHeaders.isHeaderAt(position) ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
	}

	@Override public RecyclerViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
		switch (viewType) {
			case VIEW_TYPE_HEADER: return mHeaders.createViewHolder(parent, viewType);
			default: return new RecyclerViewHolder(inflateView(R.layout.item_list_simple, parent));
		}
	}

	@Override public void onBindViewHolder(@NonNull final RecyclerViewHolder viewHolder, final int position) {
		switch (viewHolder.getItemViewType()) {
			case VIEW_TYPE_HEADER:
				mHeaders.bindViewHolder(viewHolder, position);
				break;
			case VIEW_TYPE_ITEM:
				((TextView) viewHolder.itemView).setText(getItem(position).getText());
				break;
		}
	}
}
