/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui;

import android.util.Log;

import androidx.annotation.Nullable;
import universum.studios.android.samples.ui.SamplesFragment;
import universum.studios.android.widget.adapter.OnDataSetActionListener;
import universum.studios.android.widget.adapter.OnDataSetListener;
import universum.studios.android.widget.adapter.OnDataSetSwapListener;

/**
 * @author Martin Albedinsky
 */
public abstract class DataSetAdapterFragment<A, D> extends SamplesFragment
		implements
		OnDataSetSwapListener<D>,
		OnDataSetListener,
		OnDataSetActionListener {

	@SuppressWarnings("unused") private static final String TAG = "DataSetAdapterFragment";

	@Override public void onDataSetSwapStarted(@Nullable final D data) {
		Log.d(TAG, "onDataSetSwapStarted(data: " + data + ")");
	}

	@Override public void onDataSetSwapFinished(@Nullable final D data) {
		Log.d(TAG, "onDataSetSwapFinished(data: " + data + ")");
	}

	@Override public void onDataSetChanged() {
		Log.d(TAG, "onDataSetChanged()");
	}

	@Override public void onDataSetInvalidated() {
		Log.d(TAG, "onDataSetInvalidated()");
	}

	@Override public boolean onDataSetActionSelected(final int action, final int position, final long id, @Nullable final Object payload) {
		Log.d(TAG, "onDataSetActionSelected(action: " + action + ", position: " + position + ", id: " + id + ", payload: " + payload + ")");
		return false;
	}
}