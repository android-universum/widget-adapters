/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import universum.studios.android.fragment.manage.FragmentController;
import universum.studios.android.fragment.manage.FragmentRequest;
import universum.studios.android.fragment.manage.FragmentRequestInterceptor;
import universum.studios.android.fragment.transition.FragmentTransitions;
import universum.studios.android.logging.SimpleLogger;
import universum.studios.android.samples.ui.SamplesMainFragment;
import universum.studios.android.samples.ui.SamplesNavigationActivity;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.ui.list.ListFragment;
import universum.studios.android.samples.widget.adapter.ui.module.header.HeadersModuleFragment;
import universum.studios.android.samples.widget.adapter.ui.module.slection.SelectionModuleFragment;
import universum.studios.android.samples.widget.adapter.ui.recycler.RecyclerFragment;
import universum.studios.android.samples.widget.adapter.ui.spinner.SpinnerFragment;
import universum.studios.android.widget.adapter.AdaptersLogging;

/**
 * @author Martin Albedinsky
 */
public final class MainActivity extends SamplesNavigationActivity implements FragmentRequestInterceptor {

	static {
		AdaptersLogging.setLogger(new SimpleLogger(Log.VERBOSE));
	}

	private FragmentController fragmentController;

	@Override
	protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fragmentController = FragmentController.create(this);
		fragmentController.setViewContainerId(R.id.samples_container);
	}

	@Override
	@Nullable
	public Fragment interceptFragmentRequest(@NonNull final FragmentRequest request) {
		request.transition(FragmentTransitions.CROSS_FADE).replaceSame(true);

		return null;
	}

	@Override
	protected boolean onHandleNavigationItemSelected(@NonNull final MenuItem item) {
		int itemId = item.getItemId();

		if (itemId == R.id.navigation_item_home) {
			fragmentController.newRequest(new SamplesMainFragment()).execute();

			return true;
		} else if (itemId == R.id.navigation_item_recycler) {
			fragmentController.newRequest(new RecyclerFragment()).execute();

			return true;
		} else if (itemId == R.id.navigation_item_list) {
			fragmentController.newRequest(new ListFragment()).execute();

			return true;
		} else if (itemId == R.id.navigation_item_spinner) {
			fragmentController.newRequest(new SpinnerFragment()).execute();

			return true;
		} else if (itemId == R.id.navigation_item_module_selection) {
			fragmentController.newRequest(new SelectionModuleFragment()).execute();

			return true;
		} else if (itemId == R.id.navigation_item_module_headers) {
			fragmentController.newRequest(new HeadersModuleFragment()).execute();

			return true;
		}

		return super.onHandleNavigationItemSelected(item);
	}
}