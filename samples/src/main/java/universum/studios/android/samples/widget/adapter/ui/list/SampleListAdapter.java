/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.list;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.databinding.ItemListWithActionBinding;
import universum.studios.android.widget.adapter.SimpleListAdapter;
import universum.studios.android.widget.adapter.holder.AdapterBindingHolder;
import universum.studios.android.widget.adapter.holder.ViewHolder;

/**
 * @author Martin Albedinsky
 */
final class SampleListAdapter extends SimpleListAdapter<SampleListAdapter, SampleListAdapter.ItemHolder, String> {

	static final int ACTION = 0x01;

	SampleListAdapter(@NonNull final Context context) {
		super(context);
	}

	@NonNull
	@Override
	protected ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new ItemHolder(inflateView(R.layout.item_list_with_action, parent));
	}

	@Override
	protected void onBindViewHolder(@NonNull final ItemHolder holder, final int position) {
		holder.bind(this, position, null);
	}

	final class ItemHolder extends ViewHolder implements AdapterBindingHolder<SampleListAdapter>, View.OnClickListener {

		final ItemListWithActionBinding binding;

		ItemHolder(@NonNull final View itemView) {
			super(itemView);

			binding = ItemListWithActionBinding.bind(itemView);

			itemView.findViewById(R.id.action).setOnClickListener(this);
		}

		@Override
		public void bind(
				@NonNull final SampleListAdapter adapter,
				final int position,
				@Nullable final List<Object> payloads
		) {
			binding.setText(adapter.getItem(position));
			binding.executePendingBindings();
		}

		@Override
		public void onClick(@NonNull final View view) {
			switch (view.getId()) {
				case R.id.action:
					notifyDataSetActionSelected(ACTION, getBindingAdapterPosition(), null);
					break;
			}
		}
	}
}