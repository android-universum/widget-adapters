/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.module.slection;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.samples.widget.adapter.R;
import universum.studios.android.samples.widget.adapter.databinding.ItemListColorBinding;
import universum.studios.android.widget.adapter.AdapterSavedState;
import universum.studios.android.widget.adapter.SimpleRecyclerAdapter;
import universum.studios.android.widget.adapter.holder.RecyclerViewHolder;
import universum.studios.android.widget.adapter.module.AdapterModule;
import universum.studios.android.widget.adapter.module.SelectionModule;

/**
 * @author Martin Albedinsky
 */
final class ColorsSelectionAdapter extends SimpleRecyclerAdapter<ColorsSelectionAdapter, ColorsSelectionAdapter.ItemHolder, Integer>
		implements
		AdapterModule.ModuleAdapter {

	private static final int ACTION_CLICK = 0;

	private final SelectionModule selection;

	ColorsSelectionAdapter(@NonNull final Context context) {
		super(context);
		this.selection = new SelectionModule();
		this.selection.attachToAdapter(this);
	}

	void setSelectionMode(@SelectionModule.SelectionMode final int mode) {
		selection.setMode(mode);
	}

	@SelectionModule.SelectionMode int getSelectionMode() {
		return selection.getMode();
	}

	@Override public ItemHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
		return new ItemHolder(inflateView(R.layout.item_list_color, parent));
	}

	@Override public void onBindViewHolder(@NonNull final ItemHolder holder, final int position) {
		final int color = getItem(position);
		holder.binding.setText(colorName(color, false));
		holder.binding.setColor(color);
		holder.binding.setSelected(selection.isSelected(getItemId(position)));
		holder.binding.executePendingBindings();
	}

	private static String colorName(@ColorInt final int color, final boolean withAlpha) {
		final String aHex = Integer.toString(Color.alpha(color), 16);
		final String rHex = Integer.toString(Color.red(color), 16);
		final String gHex = Integer.toString(Color.green(color), 16);
		final String bHex = Integer.toString(Color.blue(color), 16);
		String name = (withAlpha) ? (aHex.length() == 2 ? aHex : "0" + aHex) : "";
		name += rHex.length() == 2 ? rHex : "0" + rHex;
		name += gHex.length() == 2 ? gHex : "0" + gHex;
		name += bHex.length() == 2 ? bHex : "0" + bHex;
		return ("#" + name);
	}

	@Override protected boolean onDataSetActionSelected(final int action, final int position, @Nullable final Object payload) {
		switch (action) {
			case ACTION_CLICK:
				selection.toggleSelection(getItemId(position));
				return true;
		}
		return super.onDataSetActionSelected(action, position, payload);
	}

	@Override @NonNull public Parcelable saveInstanceState() {
		final SavedState state = new SavedState(super.saveInstanceState());
		state.selectionState = selection.saveInstanceState();
		return state;
	}

	@Override public void restoreInstanceState(@NonNull final Parcelable savedState) {
		if (!(savedState instanceof SavedState)) {
			super.restoreInstanceState(savedState);
			return;
		}
		final SavedState state = (SavedState) savedState;
		super.restoreInstanceState(state.getSuperState());
		if (state.selectionState != null) {
			selection.restoreInstanceState(state.selectionState);
		}
	}

	final class ItemHolder extends RecyclerViewHolder implements View.OnClickListener {

		final ItemListColorBinding binding;

		ItemHolder(@NonNull final View itemView) {
			super(itemView);
			this.binding = ItemListColorBinding.bind(itemView);
			itemView.setOnClickListener(this);
		}

		@Override public void onClick(@NonNull final View view) {
			notifyDataSetActionSelected(ACTION_CLICK, getAdapterPosition(), null);
		}
	}

	static final class SavedState extends AdapterSavedState {

		public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {

			@Override public SavedState createFromParcel(@NonNull final Parcel source) {
				return new SavedState(source);
			}

			@Override public SavedState[] newArray(final int size) {
				return new SavedState[size];
			}
		};

		private Parcelable selectionState;

		SavedState(@NonNull final Parcelable superState) {
			super(superState);
		}

		SavedState(@NonNull final Parcel source) {
			super(source);
			this.selectionState = source.readParcelable(AdapterSavedState.class.getClassLoader());
		}

		@Override public void writeToParcel(@NonNull final Parcel dest, final int flags) {
			super.writeToParcel(dest, flags);
			dest.writeParcelable(selectionState, flags);
		}
	}
}