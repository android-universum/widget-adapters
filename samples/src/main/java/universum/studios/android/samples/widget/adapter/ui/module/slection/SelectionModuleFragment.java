/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.widget.adapter.ui.module.slection;

import android.os.*;
import android.view.*;

import androidx.annotation.*;
import androidx.recyclerview.widget.*;
import universum.studios.android.samples.ui.*;
import universum.studios.android.samples.widget.adapter.*;
import universum.studios.android.widget.adapter.module.*;

/**
 * @author Martin Albedinsky
 */
public final class SelectionModuleFragment extends SamplesFragment {

	private static final String BUNDLE_ADAPTER_STATE = SelectionModuleFragment.class.getName() + ".BUNDLE.AdapterState";

	private ColorsSelectionAdapter adapter;

	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
	}

	@Override
	@Nullable
	public View onCreateView(
			@NonNull final LayoutInflater inflater,
			@Nullable final ViewGroup container,
			@Nullable final Bundle savedInstanceState
	) {
		return inflater.inflate(R.layout.fragment_recycler, container, false);
	}

	@SuppressWarnings("ConstantConditions")
	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		adapter = new ColorsSelectionAdapter(view.getContext());
		adapter.changeItems(Colors.dataSet());

		if (savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_ADAPTER_STATE)) {
			adapter.restoreInstanceState(savedInstanceState.getParcelable(BUNDLE_ADAPTER_STATE));
		}

		final RecyclerView recyclerView = view.findViewById(android.R.id.list);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
		recyclerView.setAdapter(adapter);
	}

	@Override
	public void onCreateOptionsMenu(@NonNull final Menu menu, @NonNull final MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		inflater.inflate(R.menu.selection, menu);
	}

	@Override
	public void onPrepareOptionsMenu(@NonNull final Menu menu) {
		super.onPrepareOptionsMenu(menu);

		switch (adapter.getSelectionMode()) {
			case SelectionModule.SINGLE -> menu.findItem(R.id.selection_single).setChecked(true);
			case SelectionModule.MULTIPLE -> menu.findItem(R.id.selection_multiple).setChecked(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
		final int itemId = item.getItemId();

		if (itemId == R.id.selection_single) {
			adapter.setSelectionMode(SelectionModule.SINGLE);

			return true;
		} else if (itemId == R.id.selection_multiple) {
			adapter.setSelectionMode(SelectionModule.MULTIPLE);

			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull final Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable(BUNDLE_ADAPTER_STATE, adapter.saveInstanceState());
	}
}