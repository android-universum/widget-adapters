/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.widget.adapter.holder.AdapterHolder;
import universum.studios.android.widget.adapter.holder.ViewHolder;

/**
 * Extended version of {@link android.widget.BaseAdapter}. This version of BaseAdapter implements
 * optimized algorithm for the {@link #getView(int, View, ViewGroup)} method using the <b>holder pattern</b>.
 *
 * <h3>Implementation</h3>
 * Inheritance hierarchies of this adapter class are required to implement following methods:
 * <ul>
 * <li>{@link #getItemCount()}</li>
 * <li>{@link #getItem(int)}</li>
 * <li>{@link #onCreateViewHolder(ViewGroup, int)}</li>
 * <li>{@link #onBindViewHolder(ViewHolder, int)}</li>
 * </ul>
 * See also configuration related features available for this adapter class which may possibly reduce
 * implementation requirements.
 *
 * <h3>Holder Factory</h3>
 * A desired implementation of {@link AdapterHolder.Factory} may be attached to {@link BaseListAdapter}
 * via {@link #setHolderFactory(AdapterHolder.Factory)}. Such factory will be then used by the adapter
 * to create view holders whenever {@link #onCreateViewHolder(ViewGroup, int)} is invoked for the
 * adapter.
 *
 * <h3>Holder Binder</h3>
 * A desired implementation of {@link AdapterHolder.Binder} may be attached to {@link BaseListAdapter}
 * via {@link #setHolderBinder(AdapterHolder.Binder)}. Such binder will be then used by the adapter
 * to perform binding logic for its view holders whenever {@link #onBindViewHolder(ViewHolder, int)}
 * or {@link #onBindViewHolder(ViewHolder, int)} for that matter is invoked for the adapter.
 * <p>
 * <i>Usage of holder factories along with holder binders may reduce complexity of adapter implementations
 * and also should improve testability of such adapter classes.</i>
 *
 * <h3>Data Set Actions</h3>
 * It is a common requirement to listen for a desired actions performed within a data set, mainly to
 * listen for events that are fired by clickable views displayed in the associated adapter view.
 * In order to simplify delegation of these events to interested listeners this adapter class supports
 * registration of {@link OnDataSetActionListener} via {@link #registerOnDataSetActionListener(OnDataSetActionListener)}
 * which will be notified about selected action whenever {@link #notifyDataSetActionSelected(int, int, Object)}
 * is called. This delegate method may be for example called from within {@code onClick(View)} of
 * a specific {@code ViewHolder} instance to dispatch event for the clicked view.
 *
 * <h3>State saving</h3>
 * <pre>
 * public class SampleAdapter extends BaseListAdapter&lt;SampleAdapter, ViewHolder, String&gt; {
 *
 *     // ...
 *
 *     &#64;NonNull
 *     &#64;Override
 *     public Parcelable saveInstanceState() {
 *         final SavedState state = new SavedState(super.saveInstanceState());
 *         // ...
 *         // Pass here all data of this adapter which need to be saved to the state.
 *         // ...
 *         return state;
 *     }
 *
 *     &#64;Override
 *     public void restoreInstanceState(&#64;NonNull Parcelable savedState) {
 *          if (!(savedState instanceof SavedState)) {
 *              // Passed savedState is not our state, let super to process it.
 *              super.restoreInstanceState(savedState);
 *              return;
 *          }
 *          final SavedState state = (SavedState) savedState;
 *          // Pass superState to super to process it.
 *          super.restoreInstanceState(savedState.getSuperState());
 *          // ...
 *          // Set here all data of this adapter which need to be restored from the state.
 *          // ...
 *     }
 *
 *     // ...
 *
 *     // Implementation of AdapterSavedState for this adapter.
 *     static class SavedState extends AdapterSavedState {
 *
 *         // Each implementation of saved state need to have its own CREATOR provided.
 *         public static final Creator&lt;SavedState&gt; CREATOR = new Creator&lt;SavedState&gt;() {
 *
 *              &#64;Override
 *              public SavedState createFromParcel(&#64;NonNull Parcel source) {
 *                  return new SavedState(source);
 *              }
 *
 *              &#64;Override
 *              public SavedState[] newArray(int size) {
 *                  return new SavedState[size];
 *              }
 *         };
 *
 *         // Constructor used to chain the state of inheritance hierarchies.
 *         SavedState(&#64;NonNull Parcelable superState) {
 *              super(superState);
 *         }
 *
 *         SavedState(&#64;NonNull Parcel source) {
 *              super(source);
 *              // Restore here state's data.
 *         }
 *
 *         &#64;Override
 *         public void writeToParcel(&#64;NonNull Parcel dest, int flags) {
 *              super.writeToParcel(dest, flags);
 *              // Save here state's data.
 *         }
 *     }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <A>  Type of the subclass of BaseListAdapter.
 * @param <H>  Type of the view holder used by the adapter.
 * @param <I>  Type of items presented within the adapter's data set.
 */
public abstract class BaseListAdapter<A extends BaseListAdapter, H extends ViewHolder, I>
		extends android.widget.BaseAdapter
		implements
		DataSetAdapter<I>,
		AdapterHolder.FactoryAdapter<H>,
		AdapterHolder.BinderAdapter<A, H> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseListAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Context in which will be this adapter used.
	 */
	@NonNull
	private final Context context;

	/**
	 * Application resources that may be used to obtain strings, texts, drawables, ... and other resources.
	 */
	@NonNull
	private final Resources resources;

	/**
	 * Layout inflater used to inflateView new views for this adapter.
	 */
	private final LayoutInflater layoutInflater;

	/**
	 * Registry with listeners that are attached to this adapter.
	 */
	private DataSetListeners dataSetListeners;

	/**
	 * Factory that is used by this adapter to create view holders.
	 *
	 * @see #onCreateViewHolder(ViewGroup, int)
	 */
	private AdapterHolder.Factory<H> holderFactory;

	/**
	 * Binder that is used by this adapter to bind its view holders with data.
	 *
	 * @see #onBindViewHolder(ViewHolder, int)
	 */
	private AdapterHolder.Binder<A, H> holderBinder;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseListAdapter within the given <var>context</var>.
	 *
	 * @param context Context in which will be this adapter used.
	 *
	 * @see #getContext()
	 * @see #getResources()
	 */
	public BaseListAdapter(@NonNull final Context context) {
		super();

		this.context = context;
		this.resources = context.getResources();
		this.layoutInflater = LayoutInflater.from(context);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the context with which was this adapter created.
	 *
	 * @return This adapter's context.
	 */
	@NonNull
	public final Context getContext() {
		return context;
	}

	/**
	 * Returns the resources provided by the context with which was this adapter created.
	 *
	 * @return This adapter's resources.
	 */
	@NonNull
	public final Resources getResources() {
		return resources;
	}

	/**
	 */
	@Override
	public void registerOnDataSetListener(@NonNull final OnDataSetListener listener) {
		if (dataSetListeners == null) {
			dataSetListeners = new DataSetListeners(this);
		}

		dataSetListeners.registerOnDataSetListener(listener);
	}

	/**
	 */
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();

		if (dataSetListeners != null) {
			dataSetListeners.notifyDataSetChanged();
		}
	}

	/**
	 */
	@Override
	public void notifyDataSetInvalidated() {
		super.notifyDataSetInvalidated();

		if (dataSetListeners != null) {
			dataSetListeners.notifyDataSetInvalidated();
		}
	}

	/**
	 */
	@Override
	public void unregisterOnDataSetListener(@NonNull final OnDataSetListener listener) {
		if (dataSetListeners != null) {
			dataSetListeners.unregisterOnDataSetListener(listener);
		}
	}

	/**
	 */
	@Override
	public void registerOnDataSetActionListener(@NonNull final OnDataSetActionListener listener) {
		if (dataSetListeners == null) {
			dataSetListeners = new DataSetListeners(this);
		}

		dataSetListeners.registerOnDataSetActionListener(listener);
	}

	/**
	 * Notifies that the given <var>action</var> has been performed for the specified <var>position</var>.
	 * <p>
	 * If {@link #onDataSetActionSelected(int, int, Object)} will not process this call, the registered
	 * {@link OnDataSetActionListener OnDataSetActionListeners} will be notified.
	 * <p>
	 * <b>Note that invoking this method with 'invalid' position, out of bounds of the current data
	 * set, will be ignored.</b>
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled internally by this adapter or by one of
	 * the registers listeners, {@code false} otherwise.
	 */
	@SuppressWarnings("SimplifiableIfStatement")
	public boolean notifyDataSetActionSelected(final int action, final int position, @Nullable final Object payload) {
		// Do not notify actions for invalid (out of bounds of the current data set) positions.
		if (position < 0 || position >= getItemCount()) {
			return false;
		}

		if (onDataSetActionSelected(action, position, payload)) {
			return true;
		}

		return dataSetListeners != null && dataSetListeners.notifyDataSetActionSelected(action, position, payload);
	}

	/**
	 * Invoked immediately after {@link #notifyDataSetActionSelected(int, int, Object)} was called.
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} to indicate that this event was handled here, {@code false} to dispatch
	 * this event to the registered {@link OnDataSetActionListener OnDataSetActionListeners}.
	 */
	protected boolean onDataSetActionSelected(final int action, final int position, @Nullable final Object payload) {
		// May be implemented by the inheritance hierarchies in order to handle selected data set
		// action internally before it is (if not handled) dispatched to registered listeners.
		return false;
	}

	/**
	 */
	@Override
	public void unregisterOnDataSetActionListener(@NonNull final OnDataSetActionListener listener) {
		if (dataSetListeners != null) {
			dataSetListeners.unregisterOnDataSetActionListener(listener);
		}
	}

	/**
	 * Same as {@link #getItemCount()}.
	 */
	@Override
	public final int getCount() {
		return getItemCount();
	}

	/**
	 */
	@Override
	public boolean hasItemAt(final int position) {
		return position >= 0 && position < getItemCount();
	}

	/**
	 */
	@Override
	public long getItemId(final int position) {
		return hasItemAt(position) ? position : NO_ID;
	}

	/**
	 */
	@Override
	public void setHolderFactory(@Nullable final AdapterHolder.Factory<H> factory) {
		holderFactory = factory;
	}

	/**
	 */
	@Nullable
	@Override
	public AdapterHolder.Factory<H> getHolderFactory() {
		return holderFactory;
	}

	/**
	 */
	@Override
	public void setHolderBinder(@Nullable final AdapterHolder.Binder<A, H> binder) {
		holderBinder = binder;
	}

	/**
	 */
	@Nullable
	@Override
	public AdapterHolder.Binder<A, H> getHolderBinder() {
		return holderBinder;
	}

	/**
	 */
	@Override
	@SuppressWarnings("unchecked")
	public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
		View view = convertView;
		H viewHolder;

		if (view == null) {
			viewHolder = onCreateViewHolder(parent, getItemViewType(position));

			view = viewHolder.itemView;
			view.setTag(viewHolder);
		} else {
			viewHolder = (H) view.getTag();
		}

		// Ensure that the view holder has always the binding adapter position specified.
		viewHolder.updateBindingAdapterPosition(position);

		onBindViewHolder(viewHolder, position);

		return view;
	}

	/**
	 * Invoked from {@link #getView(int, View, ViewGroup)} in order to create a view holder along
	 * with its corresponding item view for the specified <var>viewType</var>.
	 * <p>
	 * Default implementation of this method assumes presence of {@link AdapterHolder.Factory} that
	 * has been attached to this adapter via {@link #setHolderFactory(AdapterHolder.Factory)}. If such
	 * factory is available it will be used to create the requested holder otherwise an exception is
	 * thrown.
	 *
	 * @param parent   A parent view, to resolve correct layout params in case when the item view
	 *                 will be inflated from an Xml layout.
	 * @param viewType Type of the item view to be created with the holder. This is the same identifier
	 *                 as obtained via {@link #getItemViewType(int)} for the position passed to
	 *                 {@link #getView(int, View, ViewGroup)} method.
	 * @return New view holder with the item view of the requested type.
	 * @see AdapterHolder.Factory#createHolder(ViewGroup, int)
	 *
	 * @see #inflateView(int, ViewGroup)
	 */
	@NonNull
	protected H onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
		if (holderFactory == null) {
			throw AdapterExceptions.noHolderFactoryAttached(this);
		}

		return holderFactory.createHolder(parent, viewType);
	}

	/**
	 * Inflates a new view hierarchy from the given xml resource.
	 *
	 * @param resource Resource id of a view to inflateView.
	 * @param parent   A parent view, to resolve correct layout params for the newly creating view.
	 * @return The root view of the inflated view hierarchy.
	 *
	 * @see LayoutInflater#inflate(int, ViewGroup)
	 */
	@NonNull
	public View inflateView(@LayoutRes final int resource, @Nullable final ViewGroup parent) {
		return layoutInflater.inflate(resource, parent, false);
	}

	/**
	 * Invoked from {@link #getView(int, View, ViewGroup)} in order to perform binding of the given
	 * <var>viewHolder</var> with data of the item from this adapter's data set at the specified
	 * <var>position</var>.
	 * <p>
	 * Default implementation of this method assumes presence of {@link AdapterHolder.Binder} that
	 * has been attached to this adapter via {@link #setHolderBinder(AdapterHolder.Binder)}. If such
	 * binder is available it will be used to perform binding logic otherwise an exception is thrown.
	 *
	 * @param viewHolder The view holder created via {@link #onCreateViewHolder(ViewGroup, int)}
	 *                   with its corresponding item view to be bound with data.
	 * @param position   Position of the item from the current data set of which data should be bound
	 *                   to the view holder.
	 *
	 * @see AdapterHolder.Binder#bindHolder(Object, AdapterHolder, int, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	protected void onBindViewHolder(@NonNull final H viewHolder, final int position) {
		if (holderBinder == null) {
			throw AdapterExceptions.noHolderBinderAttached(this);
		}

		holderBinder.bindHolder((A) this, viewHolder, position, Collections.emptyList());
	}

	/**
	 */
	@NonNull
	@Override
	@CallSuper
	public Parcelable saveInstanceState() {
		return AdapterSavedState.EMPTY_STATE;
	}

	/**
	 */
	@Override
	@CallSuper
	public void restoreInstanceState(@NonNull Parcelable savedState) {
		// Inheritance hierarchies may restore theirs state here.
	}

	/*
	 * Inner classes ===============================================================================
	 */
}