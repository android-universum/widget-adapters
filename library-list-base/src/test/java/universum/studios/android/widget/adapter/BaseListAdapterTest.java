/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.testing.AndroidTestCase;
import universum.studios.android.widget.adapter.holder.AdapterHolder;
import universum.studios.android.widget.adapter.holder.ViewHolder;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class BaseListAdapterTest extends AndroidTestCase {

	private static final int DATA_SET_ACTION = 0x01;

	private static final AdapterHolder.Factory<ViewHolder> HOLDER_FACTORY = (parent, viewType) ->
			new ViewHolder(new TextView(parent.getContext()));

	private static final AdapterHolder.Binder<TestAdapter, ViewHolder> HOLDER_BINDER = (adapter, holder, position, payloads) ->
			((TextView) holder.itemView).setText(adapter.getItem(position));

	private ViewGroup container;

	@Before public void beforeTest() {
		this.container = new FrameLayout(getContext());
	}

	@After public void afterTest() {
		this.container = null;
	}

	@Test public void testInstantiation() {
		// Act:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Assert:
		assertThat(adapter.getContext(), is(getContext()));
		assertThat(adapter.getResources(), is(getContext().getResources()));
		assertThat(adapter.getHolderFactory(), is(nullValue()));
		assertThat(adapter.getHolderBinder(), is(nullValue()));
	}

	@Test public void testRegisterOnDataSetListener() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		// Act:
		adapter.registerOnDataSetListener(mockListener);
		adapter.registerOnDataSetListener(mockListener);
		// Assert:
		adapter.notifyDataSetChanged();
		adapter.notifyDataSetInvalidated();
		verify(mockListener).onDataSetChanged();
		verify(mockListener).onDataSetInvalidated();
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetChanged() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		adapter.registerOnDataSetListener(mockListener);
		// Act:
		adapter.notifyDataSetChanged();
		// Assert:
		verify(mockListener).onDataSetChanged();
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetChangedWithoutRegisteredListeners() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.notifyDataSetChanged();
	}

	@Test public void testNotifyDataSetInvalidated() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		adapter.registerOnDataSetListener(mockListener);
		// Act:
		adapter.notifyDataSetInvalidated();
		// Assert:
		verify(mockListener).onDataSetInvalidated();
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetInvalidatedWithoutRegisteredListeners() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.notifyDataSetInvalidated();
	}

	@Test public void testUnregisterOnDataSetListener() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		adapter.registerOnDataSetListener(mockListener);
		adapter.registerOnDataSetListener(mockListener);
		// Act:
		adapter.unregisterOnDataSetListener(mockListener);
		// Assert:
		adapter.notifyDataSetChanged();
		adapter.notifyDataSetInvalidated();
		verifyNoInteractions(mockListener);
	}

	@Test public void testUnregisterOnDataSetListenerNotRegistered() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.unregisterOnDataSetListener(mock(OnDataSetListener.class));
	}

	@Test public void testRegisterOnDataSetActionListener() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetActionListener mockListener = mock(OnDataSetActionListener.class);
		when(mockListener.onDataSetActionSelected(DATA_SET_ACTION, 0, adapter.getItemId(0), null)).thenReturn(true);
		// Act:
		adapter.registerOnDataSetActionListener(mockListener);
		adapter.registerOnDataSetActionListener(mockListener);
		// Assert:
		assertThat(adapter.notifyDataSetActionSelected(DATA_SET_ACTION, 0, null), is(true));
		verify(mockListener).onDataSetActionSelected(DATA_SET_ACTION, 0, adapter.getItemId(0), null);
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetActionSelected() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetActionListener mockListener = mock(OnDataSetActionListener.class);
		adapter.registerOnDataSetActionListener(mockListener);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			when(mockListener.onDataSetActionSelected(DATA_SET_ACTION, i, adapter.getItemId(i), null)).thenReturn(i % 2 == 0);
			assertThat(adapter.notifyDataSetActionSelected(DATA_SET_ACTION, i, null), is(i % 2 == 0));
			verify(mockListener).onDataSetActionSelected(DATA_SET_ACTION, i, adapter.getItemId(i), null);
		}
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetActionSelectedOutOfRange() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetActionListener mockListener = mock(OnDataSetActionListener.class);
		adapter.registerOnDataSetActionListener(mockListener);
		// Act + Assert:
		assertThat(adapter.notifyDataSetActionSelected(DATA_SET_ACTION, -1, null), is(false));
		assertThat(adapter.notifyDataSetActionSelected(DATA_SET_ACTION, adapter.getItemCount(), null), is(false));
		verifyNoInteractions(mockListener);
	}

	@Test public void testNotifyDataSetActionSelectedHandledByTheAdapter() {
		// Arrange:
		final OnDataSetActionListener mockListener = mock(OnDataSetActionListener.class);
		final TestAdapter adapter = new TestAdapter(getContext()) {

			@Override protected boolean onDataSetActionSelected(final int action, final int position, @Nullable final Object payload) {
				return true;
			}
		};
		adapter.registerOnDataSetActionListener(mockListener);
		// Act:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			adapter.notifyDataSetActionSelected(DATA_SET_ACTION, i, null);
		}
		// Assert:
		verifyNoInteractions(mockListener);
	}

	@Test public void testNotifyDataSetActionSelectedWithoutRegisteredListeners() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.notifyDataSetActionSelected(0, 0, null), is(false));
	}

	@Test public void testUnregisterOnDataSetActionListener() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		final OnDataSetActionListener mockListener = mock(OnDataSetActionListener.class);
		adapter.registerOnDataSetActionListener(mockListener);
		adapter.registerOnDataSetActionListener(mockListener);
		// Act:
		adapter.unregisterOnDataSetActionListener(mockListener);
		// Assert:
		assertThat(adapter.notifyDataSetActionSelected(DATA_SET_ACTION, 0, null), is(false));
		verifyNoInteractions(mockListener);
	}

	@Test public void testUnregisterOnDataSetActionListenerNotRegistered() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.unregisterOnDataSetActionListener(mock(OnDataSetActionListener.class));
	}

	@Test public void testGetCount() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.getCount(), is(adapter.getItemCount()));
	}

	@Test public void testHasItemAt() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			assertThat(adapter.hasItemAt(i), is(true));
		}
	}

	@Test public void testHasItemAtOutOfRange() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.hasItemAt(-1), is(false));
		assertThat(adapter.hasItemAt(adapter.getItemCount()), is(false));
	}

	@Test public void testGetItemId() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			assertThat(adapter.getItemId(i), is((long) i));
		}
	}

	@Test  public void testGetItemIdOutOfRange() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.getItemId(-1), is(TestAdapter.NO_ID));
		assertThat(adapter.getItemId(adapter.getItemCount()), is(TestAdapter.NO_ID));
	}

	@Test public void testHolderFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		adapter.setHolderFactory(HOLDER_FACTORY);
		assertThat(adapter.getHolderFactory(), is(HOLDER_FACTORY));
	}

	@Test public void testHolderBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		adapter.setHolderBinder(HOLDER_BINDER);
		assertThat(adapter.getHolderBinder(), is(HOLDER_BINDER));
	}

	@Test public void testGetViewWithoutConvertView() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderFactory(HOLDER_FACTORY);
		adapter.setHolderBinder(HOLDER_BINDER);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			final View view = adapter.getView(i, null, container);
			assertThat(view, is(not(nullValue())));
			assertThat(view, instanceOf(TextView.class));
			assertThat(((TextView) view).getText().toString(), is("Item at: " + i));
			final Object tag = view.getTag();
			assertThat(tag, instanceOf(ViewHolder.class));
			assertThat(((ViewHolder) tag).getBindingAdapterPosition(), is(i));
		}
	}

	@Test public void testGetViewWithConvertView() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderFactory(HOLDER_FACTORY);
		adapter.setHolderBinder(HOLDER_BINDER);
		final View convertView = adapter.getView(0, null, container);
		// Act + Assert:
		for (int i = 0; i < adapter.getItemCount(); i++) {
			final View view = adapter.getView(i, convertView, container);
			assertThat(view, is(convertView));
			assertThat(((TextView) view).getText().toString(), is("Item at: " + i));
			final Object tag = view.getTag();
			assertThat(tag, instanceOf(ViewHolder.class));
			assertThat(((ViewHolder) tag).getBindingAdapterPosition(), is(i));
		}
	}

	@Test public void testOnCreateViewHolderWithFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderFactory(HOLDER_FACTORY);
		// Act:
		final ViewHolder viewHolder = adapter.onCreateViewHolder(container, 0);
		// Assert:
		assertThat(viewHolder, is(notNullValue()));
		assertThat(viewHolder.itemView, instanceOf(TextView.class));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testOnCreateViewHolderWithoutFactory() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.onCreateViewHolder(container, 0);
	}

	@Test public void testInflateView() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.inflateView(android.R.layout.simple_list_item_1, null), is(notNullValue()));
	}

	@Test public void testInflateViewWithNullParent() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.inflateView(android.R.layout.simple_list_item_1, container), is(notNullValue()));
	}

	@Test(expected = Resources.NotFoundException.class)
	public void testInflateViewWithInvalidResource() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.inflateView(0, container);
	}

	@Test public void testOnBindViewHolderWithBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		adapter.setHolderBinder(HOLDER_BINDER);
		final TextView itemView = new TextView(getContext());
		final ViewHolder viewHolder = new ViewHolder(itemView);
		for (int i = 0; i < adapter.getItemCount(); i++) {
			// Act:
			adapter.onBindViewHolder(viewHolder, i);
			// Assert:
			assertThat(itemView.getText().toString(), is("Item at: " + i));
		}
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testOnBindViewHolderWithoutBinder() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.onBindViewHolder(new ViewHolder(new View(getContext())), 0);
	}

	@Test public void testSaveInstanceState() {
		// Arrange:
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act + Assert:
		assertThat(adapter.saveInstanceState(), is(AdapterSavedState.EMPTY_STATE));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testRestoreInstanceState() {
		// Arrange:
		// Only ensure that restoring of empty or null state does not cause any troubles.
		final TestAdapter adapter = new TestAdapter(getContext());
		// Act:
		adapter.restoreInstanceState(AdapterSavedState.EMPTY_STATE);
		adapter.restoreInstanceState(null);
	}

	private static class TestAdapter extends BaseListAdapter<TestAdapter, ViewHolder, String> {

		TestAdapter(@NonNull final Context context) { super(context); }

		@Override public int getItemCount() { return 10; }

		@Override @NonNull public String getItem(final int position) { return "Item at: " + position; }
	}
}