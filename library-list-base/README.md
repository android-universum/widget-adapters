Widget-Adapters-List-Base
===============

This module contains **base** adapter implementation for widgets inherited from the `AdapterView`.
This adapter class requires the **creation** and **binding** of its corresponding item views to be 
implemented along with **changing** and **providing** logic for the associated data set.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-list-base:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-core](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core),
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseListAdapter](https://bitbucket.org/android-universum/widget-adapters/src/main/library-list-base/src/main/java/universum/studios/android/widget/adapter/BaseListAdapter.java)