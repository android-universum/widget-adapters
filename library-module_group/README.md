@Widget-Adapters-Module
===============

This module groups the following modules into one **single group**:

- [Module-Core](https://github.com/universum-studios/android_widget_adapters/tree/main/library-module-core)
- [Module-Selection](https://github.com/universum-studios/android_widget_adapters/tree/main/library-module-selection)
- [Module-Header](https://github.com/universum-studios/android_widget_adapters/tree/main/library-module-header)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-module:${DESIRED_VERSION}@aar"

_depends on:_
[widget-adapters-state](https://bitbucket.org/android-universum/widget-adapters/src/main/library-state)