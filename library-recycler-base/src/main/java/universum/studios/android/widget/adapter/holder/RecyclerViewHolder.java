/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A {@link RecyclerView.ViewHolder} and {@link AdapterHolder} implementation that may be used as
 * base class for recycler view associated holders.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder implements AdapterHolder {

	/**
	 * Creates a new instance of RecyclerViewHolder for the given <var>itemView</var>.
	 *
	 * @param itemView Instance of the view to be associated with new holder.
	 */
	public RecyclerViewHolder(@NonNull final View itemView) {
		super(itemView);
	}
}