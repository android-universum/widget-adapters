# Migrating from 1.x to 2.x #

## Added elements ##

- `ItemPositionResolver`
- `OnDataSetSwapListener`
- `DataSetAdapterListeners`
- `AdapterHolder`
- `AdapterBindingHolder`
- `AdaptersLogging`

## Changed elements ##

### DataSetAdapter ###

**Registration** methods for `OnDataChangeListener` has been **removed** and replaced by registration
methods for new `OnDataSetSwapListener`.

### ViewHolder ###

This class has been moved to a new library module named **widget-adapters-holder**.

### BaseAdapter ###

This adapter class has been **renamed** to `BaseListAdapter`. Also the **signature** of this class
has been changed from `BaseAdapter<I, VH>` to `BaseListAdapter<A, VH, I>` where **A** is a new 
parametrized type indicating type of subclass of the `BaseListAdapter`.

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`onCreateView(...)`|**unavailable**|**Removed** due to updated implementation of `onCreateViewHolder(...)` method.|
|`onCreateViewHolder(View, int)`|`onCreateViewHolder(ViewGroup, int)`|**Signature changed**. This method is now treated same way as in `RecyclerView.Adapter`.|
|`inflate(...)`|`inflateView(...)`|**Renamed** and visibility changed from **protected** to **public**.|
|`currentViewType()`|**unavailable**|**Removed** due to updated implementation of `ViewHolder` which provides `getItemViewType()` method for that matter.|
|`notifyDataSetActionSelected(...)`|`notifyDataSetActionSelected(...)`|Visibility changed from **protected** to **public**.|
|`BaseRecyclerAdapter.SimpleViewHolder`|`RecyclerViewHolder`|**Moved** to upper level into `holder` package.|

### SimpleAdapter ###

This adapter class has been **renamed** to `SimpleListAdapter`. Also the **signature** of this class
has been changed from `SimpleAdapter<I, VH>` to `SimpleListAdapter<A, VH, I>` where **A** is a new
parametrized type indicating type of subclass of the `SimpleListAdapter`.

**Note, that this component is still present in the library but it represents an unified interface
for simple adapter implementations.**

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`boolean onItemsChange(List<I> newItems, List<I> oldItems)`|`List<I> onSwapItems(@Nullable final List<I> items)`|**Replaced.**|

### BaseSpinnerAdapter ###

**Signature** of this class has been changed from `BaseSpinnerAdapter<I, VH, DVH>` to
`BaseSpinnerAdapter<A, VH, DVH, I>` where **A** is a new parametrized type indicating type of
subclass of the `BaseSpinnerAdapter`.

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`getSelectedItemPosition()`|`getSelectedPosition()`|**Renamed**.|
|`onUpdateViewHolder(...)`|**unavailable**|**Removed**. Each of `onBindViewHolder(...)` and `onBindDropDownViewHolder(...)` should be used respectively.|

### SimpleSpinnerAdapter ###

**Signature** of this class has been changed from `SimpleSpinnerAdapter<I, VH, DVH>` to
`SimpleSpinnerAdapter<A, VH, DVH, I>` where **A** is a new parametrized type indicating type of
subclass of the `SimpleSpinnerAdapter`.

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`boolean onItemsChange(List<I> newItems, List<I> oldItems)`|`List<I> onSwapItems(@Nullable final List<I> items)`|**Replaced.**|

### BaseRecyclerAdapter ###

**Signature** of this class has been changed from `BaseRecyclerAdapter<I, VH>` to
`BaseRecyclerAdapter<A, VH, I>` where **A** is a new parametrized type indicating type of
subclass of the `BaseRecyclerAdapter`.

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`inflate(...)`|`inflateView(...)`|**Renamed** and visibility changed from **protected** to **public**.|
|`notifyDataSetActionSelected(...)`|`notifyDataSetActionSelected(...)`|Visibility changed from **protected** to **public**.|

### SimpleRecyclerAdapter ###

**Signature** of this class has been changed from `SimpleRecyclerAdapter<I, VH>` to
`SimpleRecyclerAdapter<A, VH, I>` where **A** is a new parametrized type indicating type of subclass
 of the `SimpleRecyclerAdapter`.

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`boolean onItemsChange(List<I> newItems, List<I> oldItems)`|`List<I> onSwapItems(@Nullable final List<I> items)`|**Replaced.**|

### HeadersModule ###

**Signature** of this class has been changed from `HeadersModule<H>` to `HeadersModule<VH, H>` where
**VH** is a new parametrized type indicating type of the **view holder** used by the module.

#### Implementation changes ####

|1.x|2.x|description|
|---|---|---|
|`onCreateView(...)`|**unavailable**|**Removed** due to updated implementation of `onCreateViewHolder(...)` method.|
|`createViewHolder(View, int)`|`createViewHolder(ViewGroup, int)`|**Signature changed** and made **abstract**. This method is now treated same way as in all adapters provided by the library.|
|`bindViewHolder(Object, int)`|`bindViewHolder(VH, int)`|**Signature changed** and made **abstract**.|

### AlphabeticHeaders ###

**Signature** of this class has been changed from `AlphabeticHeaders` to `AlphabeticHeaders<VH>` where
**VH** is a new parametrized type indicating type of the **view holder** used by the module.

Also due to change of `createViewHolder(...)` and `bindViewHolder(...)` methods for the `HeadersModule`
which this module extends, both these methods are now required to be implemented also by all classes
that inherits from `AlphabeticHeaders`.

## Removed elements ##

- `AdaptersConfig`
- `ItemsAdapter` (replaced by `SimpleAdapter` interface)
- `OnDataChangeListener` (replaced by `OnDataSetSwapListener`)
