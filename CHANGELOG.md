Change-Log
===============
> Regular maintenance: _01.03.2024_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 3.x ##

### [3.0.1](https://bitbucket.org/android-universum/widget-adapters/wiki/version/3.x) ###
> 01.03.2024

- Updated **recycler-view** dependency.
- Renamed `AdapterHolder.getAdapterPosition()` to `AdapterHolder.getBindingAdapterPosition()`.

### [3.0.0](https://bitbucket.org/android-universum/widget-adapters/wiki/version/3.x) ###
> 01.03.2024

- New **group id**.

## [Version 2.x](https://bitbucket.org/android-universum/widget-adapters/wiki/version/2.x) ##
## [Version 1.x](https://bitbucket.org/android-universum/widget-adapters/wiki/version/1.x) ##
