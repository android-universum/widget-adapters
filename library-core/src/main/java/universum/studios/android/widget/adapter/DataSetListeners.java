/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * A simple registry that may be used by data set adapters in order to track registered
 * {@link OnDataSetListener OnDataSetListeners} along with {@link OnDataSetActionListener OnDataSetActionListeners}
 * and also to fire callbacks for those listeners.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class DataSetListeners {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DataSetListeners";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Adapter to which is this listeners registry attached.
	 */
	private final DataSet dataSet;

	/**
	 * List containing all {@link OnDataSetListener OnDataSetListeners} that has been registered via
	 * {@link #registerOnDataSetListener(OnDataSetListener)}.
	 */
	private List<OnDataSetListener> listeners;

	/**
	 * List containing all {@link OnDataSetActionListener OnDataSetActionListeners} that has been
	 * registered via {@link #registerOnDataSetActionListener(OnDataSetActionListener)}.
	 */
	private List<OnDataSetActionListener> actionListeners;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new DataSetListeners registry for the given <var>dataSet</var>.
	 *
	 * @param dataSet The data set for which to create the new listeners registry.
	 */
	public DataSetListeners(@NonNull final DataSet dataSet) {
		this.dataSet = dataSet;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Registers a callback to be invoked whenever {@link #notifyDataSetChanged()} or {@link #notifyDataSetInvalidated()}
	 * is called upon this listeners registry.
	 *
	 * @param listener The desired listener to register.
	 *
	 * @see #unregisterOnDataSetListener(OnDataSetListener)
	 */
	public void registerOnDataSetListener(@NonNull final OnDataSetListener listener) {
		if (listeners == null) this.listeners = new ArrayList<>(1);
		if (!listeners.contains(listener)) this.listeners.add(listener);
	}

	/**
	 * Notifies all registered {@link OnDataSetListener OnDataSetListeners} that the associated data
	 * set has been changed.
	 *
	 * @see #notifyDataSetInvalidated()
	 * @see #registerOnDataSetListener(OnDataSetListener)
	 */
	public void notifyDataSetChanged() {
		if (listeners != null && !listeners.isEmpty()) {
			for (final OnDataSetListener listener : listeners) {
				listener.onDataSetChanged();
			}
		}
	}

	/**
	 * Notifies all registered {@link OnDataSetListener OnDataSetListeners} that the associated data
	 * set has been invalidated.
	 *
	 * @see #notifyDataSetChanged()
	 * @see #registerOnDataSetListener(OnDataSetListener)
	 */
	public void notifyDataSetInvalidated() {
		if (listeners != null && !listeners.isEmpty()) {
			for (final OnDataSetListener listener : listeners) {
				listener.onDataSetInvalidated();
			}
		}
	}

	/**
	 * Unregisters the given <var>listener</var> from this listeners registry, so it will not receive
	 * any callbacks further.
	 *
	 * @param listener The desired listener to unregister.
	 *
	 * @see #registerOnDataSetListener(OnDataSetListener)
	 */
	public void unregisterOnDataSetListener(@NonNull final OnDataSetListener listener) {
		if (listeners != null) this.listeners.remove(listener);
	}

	/**
	 * Registers a callback to be invoked whenever {@link #notifyDataSetActionSelected(int, int, Object)}
	 * is called upon this listeners registry.
	 *
	 * @param listener The desired listener to register.
	 *
	 * @see #notifyDataSetActionSelected(int, int, Object)
	 * @see #unregisterOnDataSetActionListener(OnDataSetActionListener)
	 */
	public void registerOnDataSetActionListener(@NonNull final OnDataSetActionListener listener) {
		if (actionListeners == null) this.actionListeners = new ArrayList<>(1);
		if (!actionListeners.contains(listener)) this.actionListeners.add(listener);
	}

	/**
	 * Notifies all registered {@link OnDataSetActionListener OnDataSetActionListeners} that the
	 * specified <var>action</var> has been selected in the associated data set.
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled by one of the registered listeners,
	 * {@code false} otherwise.
	 *
	 * @see #registerOnDataSetActionListener(OnDataSetActionListener)
	 */
	public boolean notifyDataSetActionSelected(final int action, final int position, final Object payload) {
		if (actionListeners != null && !actionListeners.isEmpty()) {
			final long itemId = dataSet.getItemId(position);
			for (final OnDataSetActionListener listener : actionListeners) {
				if (listener.onDataSetActionSelected(action, position, itemId, payload)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Unregister the given <var>callback</var> from this listeners registry, so it will not receive
	 * any callbacks further.
	 *
	 * @param listener The desired listener to unregister.
	 *
	 * @see #registerOnDataSetActionListener(OnDataSetActionListener)
	 */
	public void unregisterOnDataSetActionListener(@NonNull final OnDataSetActionListener listener) {
		if (actionListeners != null) this.actionListeners.remove(listener);
	}

	/**
	 * Checks whether this listeners registry is empty or not.
	 *
	 * @return {@code True} if there are not listeners registered, {@code false} if there is at least
	 * one listener registered.
	 */
	public boolean isEmpty() {
		return (listeners == null || listeners.isEmpty()) && (actionListeners == null || actionListeners.isEmpty());
	}

	/*
	 * Inner classes ===============================================================================
	 */
}