/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Listener which receives callbacks about started and finished swap of adapter's data.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <A> Type of the adapter to which will be this data set listener attached.
 * @param <D> Type of data that may be swapped for the adapter.
 */
public interface OnAdapterDataSetSwapListener<A, D> {

	/**
	 * Invoked whenever the given <var>data</var> are about to be changed for the specified
	 * <var>adapter</var>.
	 *
	 * @param adapter The adapter of which data are about to be swapped.
	 * @param data    The new data that are about to be swapped.
	 */
	void onDataSetSwapStarted(@NonNull A adapter, @Nullable D data);

	/**
	 * Invoked whenever the given <var>data</var> has been swapped for the specified <var>adapter</var>.
	 *
	 * @param adapter The adapter of which data has been swapped.
	 * @param data    The swapped data.
	 */
	void onDataSetSwapFinished(@NonNull A adapter, @Nullable D data);

	/**
	 * Implementation of {@link OnDataSetSwapListener} which delegates its {@link OnDataSetSwapListener#onDataSetSwapStarted(Object)}
	 * and {@link OnDataSetSwapListener#onDataSetSwapFinished(Object)} callbacks to {@link #onDataSetSwapStarted(Object, Object)}}
	 * and to {@link #onDataSetSwapFinished(Object, Object)}} along with instance of the associated
	 * adapter.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <A> Type of the adapter to which will be this delegate attached.
	 * @param <D> Type of data that may be swapped for the adapter.
	 */
	final class Delegate<A, D> implements OnDataSetSwapListener<D> {

		/**
		 * The adapter to be passed to {@link #onDataSetSwapStarted(Object, Object)} whenever this
		 * listener receives {@link #onDataSetSwapStarted(Object)} or to {@link #onDataSetSwapFinished(Object, Object)}
		 * whenever this listener receives {@link #onDataSetSwapFinished(Object)}.
		 */
		final A adapter;

		/**
		 * Listener to which to delegate {@link #onDataSetSwapStarted(Object)} and {@link #onDataSetSwapFinished(Object)}
		 * along with the attached <var>adapter</var>.
		 */
		final OnAdapterDataSetSwapListener<A, D> delegateListener;

		/**
		 * Creates a new instance of AdapterDelegate for the given <var>adapter</var> and <var>listener</var>.
		 *
		 * @param adapter  The desired adapter for which to create delegate.
		 * @param listener The desired delegate listener.
		 */
		private Delegate(final A adapter, final OnAdapterDataSetSwapListener<A, D> listener) {
			this.adapter = adapter;
			this.delegateListener = listener;
		}

		/**
		 * Creates a new instance of AdapterDelegate which will delegate its {@link #onDataSetSwapStarted(Object)}
		 * and {@link #onDataSetSwapFinished(Object)} callbacks to the given <var>listener</var> along
		 * with the specified <var>adapter</var>.
		 *
		 * @param adapter  The desired adapter to be passed along to to given listener's callback.
		 * @param listener The desired listener to which to delegate {@link OnDataSetListener#onDataSetChanged()}
		 *                 and {@link OnDataSetListener#onDataSetInvalidated()} callbacks along with
		 *                 the specified <var>adapter</var>.
		 * @param <A>      Type of the adapter to which will be the delegate attached.
		 * @return AdapterDelegate ready to be registered as data set listener.
		 */
		@NonNull public static <A, D> Delegate<A, D> create(@NonNull final A adapter, @NonNull final OnAdapterDataSetSwapListener<A, D> listener) {
			return new Delegate<>(adapter, listener);
		}

		/**
		 */
		@Override public void onDataSetSwapStarted(@Nullable final D data) {
			this.delegateListener.onDataSetSwapStarted(adapter, data);
		}

		/**
		 */
		@Override public void onDataSetSwapFinished(@Nullable final D data) {
			this.delegateListener.onDataSetSwapFinished(adapter, data);
		}
	}
}