/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.NonNull;

/**
 * An {@link ItemPositionResolver} implementation that may be used for position resolving of items
 * provided by {@link DataSet}. This resolver implementation simply iterates the attached data set
 * and when the matching item id is found returns the current iterated position.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class DataSetItemPositionResolver implements ItemPositionResolver {

	/**
	 * Data set used to resolve item's position.
	 */
	private final DataSet dataSet;

	/**
	 * Creates a new instance of DataSetItemPositionResolver for the given <var>dataSet</var>.
	 *
	 * @param dataSet The desired data set for which to create new resolver.
	 */
	public DataSetItemPositionResolver(@NonNull final DataSet dataSet) {
		this.dataSet = dataSet;
	}

	/**
	 */
	@Override public int resolveItemPosition(final long itemId) {
		final int itemCount = dataSet.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			if (dataSet.getItemId(i) == itemId) return i;
		}
		return NO_POSITION;
	}
}