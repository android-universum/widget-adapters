/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

/**
 * A simple interface which may be used for resolving position of an item by its corresponding id.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
interface ItemPositionResolver {

	/**
	 * Constant that identifies invalid/unspecified position in data set.
	 */
	int NO_POSITION = -1;

	/**
	 * Resolves position of the item which is associated with the specified <var>itemId</var>.
	 *
	 * @param itemId Id of the item of which position to resolve.
	 * @return Position of the item or {@link #NO_POSITION} if position failed to be resolved.
	 */
	int resolveItemPosition(long itemId);
}