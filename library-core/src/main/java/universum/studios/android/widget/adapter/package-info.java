/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * Provides extended adapter classes for {@link android.widget.AdapterView AdapterView} and
 * {@link androidx.recyclerview.widget.RecyclerView RecyclerView} with <b>holder-pattern</b> support.
 * <p>
 * This package also contains some useful adapter callbacks that may be used to listen for changes
 * and/or actions occurred in an adapter's data set.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.widget.adapter;
