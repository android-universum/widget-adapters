/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A simple registry that may be used by data set adapters of which data set may be swapped, for a
 * new one, in order to track registered {@link OnDataSetSwapListener OnDataSetSwapListenera} and
 * also to fire callbacks for those listeners.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <D> Type of data set that may be swapped for the adapter.
 */
public final class SwappableDataSetAdapterListeners<D> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SwappableDataSetAdapterListeners";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * List containing all {@link OnDataSetSwapListener} that has been registered via
	 * {@link #registerOnDataSetSwapListener(OnDataSetSwapListener)}.
	 */
	private List<OnDataSetSwapListener<D>> listeners;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Registers a callback to be invoked whenever {@link #notifyDataSetSwapStarted(Object)} or
	 * {@link #notifyDataSetSwapFinished(Object)} is called upon this listeners registry.
	 *
	 * @param listener The desired listener to register.
	 *
	 * @see #unregisterOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	public void registerOnDataSetSwapListener(@NonNull final OnDataSetSwapListener<D> listener) {
		if (this.listeners == null) this.listeners = new ArrayList<>(1);
		if (!listeners.contains(listener)) this.listeners.add(listener);
	}

	/**
	 * Notifies all registered {@link OnDataSetSwapListener OnDataSetSwapListeners} that the specified
	 * <var>dataSet</var> is about to be swapped for the associated adapter.
	 *
	 * @param dataSet The data set that is about to be swapped for the adapter.
	 *
	 * @see #notifyDataSetSwapFinished(Object)
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	protected void notifyDataSetSwapStarted(@Nullable final D dataSet) {
		if (listeners != null && !listeners.isEmpty()) {
			for (final OnDataSetSwapListener<D> listener : listeners) {
				listener.onDataSetSwapStarted(dataSet);
			}
		}
	}

	/**
	 * Notifies all registered {@link OnDataSetSwapListener OnDataSetSwapListeners} that the specified
	 * <var>dataSet</var> has been swapped for the associated adapter.
	 *
	 * @param dataSet The data set that has been swapped for the adapter.
	 *
	 * @see #notifyDataSetSwapStarted(Object)
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	protected void notifyDataSetSwapFinished(@Nullable final D dataSet) {
		if (listeners != null && !listeners.isEmpty()) {
			for (final OnDataSetSwapListener<D> listener : listeners) {
				listener.onDataSetSwapFinished(dataSet);
			}
		}
	}

	/**
	 * Unregisters the given <var>listener</var> from this listeners registry, so it will not receive
	 * any callbacks further.
	 *
	 * @param listener The desired listener to unregister.
	 *
	 * @see #registerOnDataSetSwapListener(OnDataSetSwapListener)
	 */
	public void unregisterOnDataSetSwapListener(@NonNull final OnDataSetSwapListener<D> listener) {
		if (listeners != null) this.listeners.remove(listener);
	}

	/**
	 * Checks whether this listeners registry is empty or not.
	 *
	 * @return {@code True} if there are not listeners registered, {@code false} if there is at least
	 * one listener registered.
	 */
	public boolean isEmpty() {
		return listeners == null || listeners.isEmpty();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}