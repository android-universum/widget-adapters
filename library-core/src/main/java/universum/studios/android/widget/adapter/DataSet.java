/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.NonNull;

/**
 * Interface specifying API for data sets that contain a simple set of items.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <I> Item model that represents data structure of the data provided by this data set.
 */
public interface DataSet<I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant that identifies invalid/unspecified position in data set.
	 */
	int NO_POSITION = -1;

	/**
	 * Constant that identifies invalid/unspecified id.
	 */
	long NO_ID = -1;

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns a boolean flag indicating whether this data set is empty or not.
	 *
	 * @return {@code True} if cursor attached to this data set has no rows or it is {@code null},
	 * {@code false} otherwise.
	 *
	 * @see #getItemCount()
	 */
	boolean isEmpty();

	/**
	 * Returns the count of items available within this data set.
	 *
	 * @return Count of rows of the attached cursor or {@code 0} if no cursor is attached or it has
	 * no rows.
	 *
	 * @see #isEmpty()
	 * @see #hasItemAt(int)
	 * @see #getItem(int)
	 */
	int getItemCount();

	/**
	 * Returns a boolean flag indicating whether this data set has item that can provide data for the
	 * specified <var>position</var> or not.
	 *
	 * @param position The position of item to check.
	 * @return {@code True} if {@link #getItem(int)} can be called 'safely', {@code false} otherwise.
	 */
	boolean hasItemAt(int position);

	/**
	 * Returns a boolean flag indicating whether an item at the specified <var>position</var> is
	 * enabled or not.
	 *
	 * @param position Position of the item to check.
	 * @return {@code True} if the item is enabled, {@code false} otherwise.
	 */
	boolean isEnabled(int position);

	/**
	 * Returns a boolean flag indicating whether this data set has stable ids or not.
	 *
	 * @return {@code True} if the data set has stable ids so each position has its unique id associated,
	 * {@code false} otherwise.
	 *
	 * @see #getItemId(int)
	 */
	boolean hasStableIds();

	/**
	 * Returns the ID of the item at the specified <var>position</var>.
	 *
	 * @param position The position of item of which id to obtain.
	 * @return Item's id or {@link #NO_ID} if there is no item at the specified position.
	 *
	 * @see #hasItemAt(int)
	 * @see #getItem(int)
	 */
	long getItemId(int position);

	/**
	 * Returns the item (model) containing data from this data set for the specified <var>position</var>.
	 *
	 * @param position Position of the item to obtain.
	 * @return Item with data for the requested position.
	 * @throws IndexOutOfBoundsException If the specified position is out of bounds of the current
	 *                                   data set.
	 * @see #hasItemAt(int)
	 * @see #getItemId(int)
	 */
	@NonNull I getItem(int position);
}