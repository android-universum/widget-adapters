/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Listener like {@link OnDataSetActionListener} which may be used to receive same callback along
 * with the associated adapter instance.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <A> Type of the adapter for which to register this listener.
 */
public interface OnAdapterDataSetActionListener<A> {

	/**
	 * Invoked whenever the specified <var>action</var> was selected for the specified
	 * <var>position</var> within the given <var>adapter</var>.
	 *
	 * @param adapter  The adapter in which was the specified action selected.
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param id       An id of the item at the specified position within the current data set of the
	 *                 passed adapter.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled, {@code false} otherwise.
	 */
	boolean onDataSetActionSelected(@NonNull A adapter, int action, int position, long id, @Nullable Object payload);

	/**
	 * Implementation of {@link OnDataSetActionListener} which delegates its
	 * {@link OnDataSetActionListener#onDataSetActionSelected(int, int, long, Object)} callback to
	 * {@link #onDataSetActionSelected(Object, int, int, long, Object)} along with instance of the
	 * associated adapter.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <A> Type of the adapter to which will be this delegate attached.
	 */
	final class Delegate<A> implements OnDataSetActionListener {

		/**
		 * The adapter to be passed to {@link #onDataSetActionSelected(Object, int, int, long, Object)}
		 * whenever this listener receives {@link #onDataSetActionSelected(int, int, long, Object)}.
		 */
		final A adapter;

		/**
		 * Listener to which to delegate {@link #onDataSetActionSelected(int, int, long, Object)}
		 * along with the attached <var>adapter</var>.
		 */
		final OnAdapterDataSetActionListener<A> delegateListener;

		/**
		 * Creates a new instance of Delegate for the given <var>adapter</var> and <var>listener</var>.
		 *
		 * @param adapter  The desired adapter for which to create delegate.
		 * @param listener The desired delegate listener.
		 */
		private Delegate(final A adapter, final OnAdapterDataSetActionListener<A> listener) {
			this.adapter = adapter;
			this.delegateListener = listener;
		}

		/**
		 * Creates a new instance of Delegate which will delegate its {@link #onDataSetActionSelected(int, int, long, Object)}
		 * callback to the given <var>listener</var> along with the specified <var>adapter</var>.
		 *
		 * @param adapter  The desired adapter to be passed along to to given listener's callback.
		 * @param listener The desired listener to which to delegate {@link OnDataSetActionListener#onDataSetActionSelected(int, int, long, Object)}
		 *                 callback along with the specified <var>adapter</var>.
		 * @param <A>      Type of the adapter to which will be the delegate attached.
		 * @return Delegate ready to be registered as data set action listener.
		 */
		@NonNull public static <A> Delegate<A> create(@NonNull final A adapter, @NonNull final OnAdapterDataSetActionListener<A> listener) {
			return new Delegate<>(adapter, listener);
		}

		/**
		 */
		@Override public boolean onDataSetActionSelected(final int action, final int position, final long id, @Nullable final Object payload) {
			return delegateListener.onDataSetActionSelected(adapter, action, position, id, payload);
		}
	}
}