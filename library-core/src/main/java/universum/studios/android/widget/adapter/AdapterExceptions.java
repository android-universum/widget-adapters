/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.NonNull;

/**
 * Simple factory providing exceptions that may be thrown by adapters from this library.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class AdapterExceptions {

	/**
	 */
	private AdapterExceptions() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/**
	 * Creates a new instance of RuntimeException informing that the specified <var>adapter</var>
	 * does not have holder factory attached.
	 *
	 * @param adapter The adapter for which to create the requested exception.
	 * @return Runtime exception ready to be thrown.
	 */
	@NonNull static RuntimeException noHolderFactoryAttached(@NonNull final Object adapter) {
		final String adapterName = adapter.getClass().getSimpleName();
		return new UnsupportedOperationException(
				"Cannot create view holder without holder factory attached! " +
						"A desired factory may be specified via " + adapterName + ".setHolderFactory(...), " +
						"otherwise implementation of " + adapterName + ".onCreateViewHolder(...) method is required."
		);
	}

	/**
	 * Creates a new instance of RuntimeException informing that the specified <var>adapter</var>
	 * does not have holder binder attached.
	 *
	 * @param adapter The adapter for which to create the requested exception.
	 * @return Runtime exception ready to be thrown.
	 */
	@NonNull static RuntimeException noHolderBinderAttached(@NonNull final Object adapter) {
		final String adapterName = adapter.getClass().getSimpleName();
		return new UnsupportedOperationException(
				"Cannot bind view holder without holder binder attached! " +
						"A desired binder may be specified via " + adapterName + ".setHolderBinder(...), " +
						"otherwise implementation of " + adapterName + ".onBindViewHolder(...) method is required."
		);
	}
}