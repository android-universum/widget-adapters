/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import android.os.Parcelable;
import androidx.annotation.NonNull;

/**
 * A convenience interface specifying common API for adapters with data set.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <I> Model that represents a data structure of the adapter's data set.
 */
public interface DataSetAdapter<I> extends DataSet<I> {

	/**
	 * Registers a callback to be invoked when a data set event occurs for this adapter. The occurred
	 * event may be either a change in the data set, or an invalidation of the data set.
	 *
	 * @param listener The desired listener to register.
	 *
	 * @see #unregisterOnDataSetListener(OnDataSetListener)
	 */
	void registerOnDataSetListener(@NonNull OnDataSetListener listener);

	/**
	 * Unregisters the given <var>listener</var> from data set listeners, so it will not receive any
	 * callbacks further.
	 *
	 * @param listener The desired listener to unregister.
	 *
	 * @see #registerOnDataSetListener(OnDataSetListener)
	 */
	void unregisterOnDataSetListener(@NonNull OnDataSetListener listener);

	/**
	 * Registers a callback to be invoked when a specific data set action is selected within this
	 * adapter.
	 *
	 * @param listener The desired listener to register.
	 *
	 * @see #unregisterOnDataSetActionListener(OnDataSetActionListener)
	 */
	void registerOnDataSetActionListener(@NonNull OnDataSetActionListener listener);

	/**
	 * Unregisters the given <var>listener</var> from data set action listeners, so it will not
	 * receive any callbacks further.
	 *
	 * @param listener The desired listener to unregister.
	 *
	 * @see #registerOnDataSetActionListener(OnDataSetActionListener)
	 */
	void unregisterOnDataSetActionListener(@NonNull OnDataSetActionListener listener);

	/**
	 * Saves the current state of this adapter.
	 * <p>
	 * If you decide to override this method, do not forget to call {@code super.saveInstanceState()}
	 * and pass the obtained super state to the corresponding constructor of your saved state
	 * implementation to ensure the state of all classes along the chain is properly saved.
	 *
	 * @return Saved state of this adapter or an <b>empty</b> state if this adapter does not need to
	 * save its state.
	 */
	@NonNull Parcelable saveInstanceState();

	/**
	 * Restores the previous state, saved via {@link #saveInstanceState()}, of this adapter.
	 * <p>
	 * If you decide to override this method, do not forget to call {@code super.restoreInstanceState(Parcelable)}
	 * and pass there the parent state obtained from your saved state implementation to ensure the
	 * state of all classes along the chain is properly restored.
	 *
	 * @param savedState Should be the same state as obtained via {@link #saveInstanceState()} before.
	 */
	void restoreInstanceState(@NonNull Parcelable savedState);
}