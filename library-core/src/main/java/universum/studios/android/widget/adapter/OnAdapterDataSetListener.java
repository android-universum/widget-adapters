/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.NonNull;

/**
 * Listener like {@link OnDataSetListener} which may be used to receive same callbacks along
 * with the associated adapter instance.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <A> Type of the adapter for which to register this listener.
 */
public interface OnAdapterDataSetListener<A> {

	/**
	 * Invoked whenever the current data of the specified <var>adapter</var> has been changed.
	 *
	 * @param adapter The adapter of which data set has been changed.
	 *
	 * @see #onDataSetInvalidated(Object)
	 */
	void onDataSetChanged(@NonNull A adapter);

	/**
	 * Invoked whenever the current data set of the specified <var>adapter</var> has bee invalidated.
	 *
	 * @param adapter The adapter of which data set has been invalidated.
	 *
	 * @see #onDataSetChanged(Object)
	 */
	void onDataSetInvalidated(@NonNull A adapter);

	/**
	 * Implementation of {@link OnAdapterDataSetListener} which delegates its {@link OnDataSetListener#onDataSetChanged()}
	 * and {@link OnDataSetListener#onDataSetInvalidated()} callbacks to {@link #onDataSetChanged(Object)}}
	 * and to {@link #onDataSetInvalidated(Object)}} along with instance of the associated adapter.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <A> Type of the adapter to which will be this delegate attached.
	 */
	final class Delegate<A> implements OnDataSetListener {

		/**
		 * The adapter to be passed to {@link #onDataSetChanged(Object)} whenever this listener
		 * receives {@link #onDataSetChanged()} or to {@link #onDataSetInvalidated(Object)} whenever
		 * this listener receives {@link #onDataSetInvalidated()}.
		 */
		final A adapter;

		/**
		 * Listener to which to delegate {@link #onDataSetChanged()} and {@link #onDataSetInvalidated()}
		 * along with the attached <var>adapter</var>.
		 */
		final OnAdapterDataSetListener<A> delegateListener;

		/**
		 * Creates a new instance of AdapterDelegate for the given <var>adapter</var> and <var>listener</var>.
		 *
		 * @param adapter  The desired adapter for which to create delegate.
		 * @param listener The desired delegate listener.
		 */
		private Delegate(final A adapter, final OnAdapterDataSetListener<A> listener) {
			this.adapter = adapter;
			this.delegateListener = listener;
		}

		/**
		 * Creates a new instance of AdapterDelegate which will delegate its {@link #onDataSetChanged()}
		 * and {@link #onDataSetInvalidated()} callbacks to the given <var>listener</var> along with
		 * the specified <var>adapter</var>.
		 *
		 * @param adapter  The desired adapter to be passed along to to given listener's callback.
		 * @param listener The desired listener to which to delegate {@link OnDataSetListener#onDataSetChanged()}
		 *                 and {@link OnDataSetListener#onDataSetInvalidated()} callbacks along with
		 *                 the specified <var>adapter</var>.
		 * @param <A>      Type of the adapter to which will be the delegate attached.
		 * @return AdapterDelegate ready to be registered as data set listener.
		 */
		@NonNull public static <A> Delegate<A> create(@NonNull final A adapter, @NonNull final OnAdapterDataSetListener<A> listener) {
			return new Delegate<>(adapter, listener);
		}

		/**
		 */
		@Override public void onDataSetChanged() {
			this.delegateListener.onDataSetChanged(adapter);
		}

		/**
		 */
		@Override public void onDataSetInvalidated() {
			this.delegateListener.onDataSetInvalidated(adapter);
		}
	}
}