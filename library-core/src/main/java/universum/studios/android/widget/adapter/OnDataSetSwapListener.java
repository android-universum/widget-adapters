/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.Nullable;

/**
 * Listener which receives callbacks about started and finished swap of a specific data in data set.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <D> Type of data that may be swapped for the adapter.
 */
public interface OnDataSetSwapListener<D> {

	/**
	 * Invoked whenever the given <var>data</var> are about to be changed for the associated data set.
	 *
	 * @param data The new data that is about to be swapped.
	 */
	void onDataSetSwapStarted(@Nullable D data);

	/**
	 * Invoked whenever the given <var>data</var> has been swapped for the associated data set.
	 *
	 * @param data The swapped data.
	 */
	void onDataSetSwapFinished(@Nullable D data);
}