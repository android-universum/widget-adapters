/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import androidx.annotation.Nullable;

/**
 * Listener which receives callback about selected action within data set for a specific position.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface OnDataSetActionListener {

	/**
	 * Invoked whenever the specified <var>action</var> was selected for the specified <var>position</var>
	 * within the associated data set where is this callback registered.
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param id       An id of the item at the specified position within the associated data set.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled, {@code false} otherwise.
	 */
	boolean onDataSetActionSelected(int action, int position, long id, @Nullable Object payload);
}