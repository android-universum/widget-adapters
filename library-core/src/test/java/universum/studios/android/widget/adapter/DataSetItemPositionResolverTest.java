/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import org.junit.Test;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class DataSetItemPositionResolverTest implements TestCase {

	@Test public void testResolveItemPosition() {
		// Arrange:
		final DataSet mockDataSet = mock(DataSet.class);
		final ItemPositionResolver resolver = new DataSetItemPositionResolver(mockDataSet);
		when(mockDataSet.getItemCount()).thenReturn(10);
		// Act + Assert:
		for (int i = 0; i < mockDataSet.getItemCount(); i++) {
			when(mockDataSet.getItemId(i)).thenReturn(i + 2L);
			assertThat(resolver.resolveItemPosition(i + 2L), is(i));
			verify(mockDataSet).getItemId(i);
		}
	}

	@Test public void testResolvePositionForEmptyDataSet() {
		// Arrange:
		final DataSet mockDataSet = mock(DataSet.class);
		final ItemPositionResolver resolver = new DataSetItemPositionResolver(mockDataSet);
		when(mockDataSet.getItemCount()).thenReturn(0);
		// Act + Assert:
		for (int i = 0; i < 25; i++) {
			assertThat(resolver.resolveItemPosition(i + 1L), is(ItemPositionResolver.NO_POSITION));
		}
	}

	@Test public void testResolvePositionForItemNotInDataSet() {
		// Arrange:
		final DataSet mockDataSet = mock(DataSet.class);
		final ItemPositionResolver resolver = new DataSetItemPositionResolver(mockDataSet);
		when(mockDataSet.getItemCount()).thenReturn(1);
		when(mockDataSet.getItemId(99)).thenReturn(1L);
		// Act + Assert:
		assertThat(resolver.resolveItemPosition(100), is(ItemPositionResolver.NO_POSITION));
	}
}