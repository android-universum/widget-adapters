/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Martin Albedinsky
 */
public final class OnAdapterDataSetSwapListenerTest implements TestCase {

	@Test public void testDelegateCreate() {
		// Arrange:
		final TestAdapter mockAdapter = mock(TestAdapter.class);
		final TestListener mockListener = mock(TestListener.class);
		// Act:
		final OnAdapterDataSetSwapListener.Delegate<TestAdapter, List<String>> delegate = OnAdapterDataSetSwapListener.Delegate.create(mockAdapter, mockListener);
		// Assert:
		assertThat(delegate, is(not(nullValue())));
		assertThat(delegate.adapter, is(mockAdapter));
		assertThat(delegate.delegateListener, is(mockListener));
	}

	@Test public void testDelegateOnDataSetChanged() {
		// Arrange:
		final TestAdapter mockAdapter = mock(TestAdapter.class);
		final TestListener mockListener = mock(TestListener.class);
		final OnAdapterDataSetSwapListener.Delegate<TestAdapter, List<String>> delegate = OnAdapterDataSetSwapListener.Delegate.create(mockAdapter, mockListener);
		// Act:
		delegate.onDataSetSwapStarted(Collections.emptyList());
		// Assert:
		verify(mockListener).onDataSetSwapStarted(mockAdapter, Collections.emptyList());
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testDelegateOnDataSetInvalidated() {
		// Arrange:
		final TestAdapter mockAdapter = mock(TestAdapter.class);
		final TestListener mockListener = mock(TestListener.class);
		final OnAdapterDataSetSwapListener.Delegate<TestAdapter, List<String>> delegate = OnAdapterDataSetSwapListener.Delegate.create(mockAdapter, mockListener);
		// Act:
		delegate.onDataSetSwapFinished(Collections.emptyList());
		// Assert:
		verify(mockListener).onDataSetSwapFinished(mockAdapter, Collections.emptyList());
		verifyNoMoreInteractions(mockListener);
	}

	private static class TestAdapter {}

	private interface TestListener extends OnAdapterDataSetSwapListener<TestAdapter, List<String>> {}
}