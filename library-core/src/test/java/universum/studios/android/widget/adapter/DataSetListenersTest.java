/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import org.junit.Test;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class DataSetListenersTest implements TestCase {

	private static final int DATA_SET_ACTION = 0x01;

	@Test public void testInstantiation() {
		// Act:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		// Assert:
		assertThat(listeners.isEmpty(), is(true));
	}

	@Test public void testRegisterOnDataSetListener() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		final OnDataSetListener firstMockListener = mock(OnDataSetListener.class);
		final OnDataSetListener secondMockListener = mock(OnDataSetListener.class);
		// Act:
		listeners.registerOnDataSetListener(firstMockListener);
		listeners.registerOnDataSetListener(firstMockListener);
		listeners.registerOnDataSetListener(firstMockListener);
		listeners.registerOnDataSetListener(secondMockListener);
		// Assert:
		listeners.notifyDataSetChanged();
		listeners.notifyDataSetInvalidated();
		verify(firstMockListener).onDataSetChanged();
		verify(firstMockListener).onDataSetInvalidated();
		verify(secondMockListener).onDataSetChanged();
		verify(secondMockListener).onDataSetInvalidated();
		verifyNoMoreInteractions(firstMockListener, secondMockListener);
	}

	@Test public void testUnregisterOnDataSetListener() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		final OnDataSetListener firstMockListener = mock(OnDataSetListener.class);
		final OnDataSetListener secondMockListener = mock(OnDataSetListener.class);
		listeners.registerOnDataSetListener(firstMockListener);
		listeners.registerOnDataSetListener(secondMockListener);
		// Act:
		listeners.unregisterOnDataSetListener(firstMockListener);
		// Assert:
		listeners.notifyDataSetChanged();
		listeners.notifyDataSetInvalidated();
		verifyNoInteractions(firstMockListener);
		verify(secondMockListener).onDataSetChanged();
		verify(secondMockListener).onDataSetInvalidated();
		verifyNoMoreInteractions(secondMockListener);
	}

	@Test public void testUnregisterOnDataSetListenerWithoutRegisteredListeners() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.unregisterOnDataSetListener(mock(OnDataSetListener.class));
	}

	@Test public void testNotifyDataSetChanged() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		listeners.registerOnDataSetListener(mockListener);
		// Act:
		listeners.notifyDataSetChanged();
		// Assert:
		verify(mockListener).onDataSetChanged();
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetChangedWithoutRegisteredListeners() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.notifyDataSetChanged();
	}

	@Test public void testNotifyDataSetInvalidated() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		final OnDataSetListener mockListener = mock(OnDataSetListener.class);
		listeners.registerOnDataSetListener(mockListener);
		// Act:
		listeners.notifyDataSetInvalidated();
		// Assert:
		verify(mockListener).onDataSetInvalidated();
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetInvalidatedWithoutRegisteredListeners() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.notifyDataSetInvalidated();
	}

	@Test public void testRegisterOnDataSetActionListener() {
		// Arrange:
		final DataSet mockDataSet = mock(DataSet.class);
		final DataSetListeners listeners = new DataSetListeners(mockDataSet);
		final OnDataSetActionListener firstMockListener = mock(OnDataSetActionListener.class);
		final OnDataSetActionListener secondMockListener = mock(OnDataSetActionListener.class);
		// Act:
		listeners.registerOnDataSetActionListener(firstMockListener);
		listeners.registerOnDataSetActionListener(firstMockListener);
		listeners.registerOnDataSetActionListener(firstMockListener);
		listeners.registerOnDataSetActionListener(secondMockListener);
		listeners.notifyDataSetActionSelected(DATA_SET_ACTION, 0, null);
		// Assert:
		verify(firstMockListener).onDataSetActionSelected(DATA_SET_ACTION, 0, mockDataSet.getItemId(0), null);
		verify(secondMockListener).onDataSetActionSelected(DATA_SET_ACTION, 0, mockDataSet.getItemId(0), null);
	}

	@Test public void testUnregisterOnDataSetActionListener() {
		// Arrange:
		final DataSet mockDataSet = mock(DataSet.class);
		final DataSetListeners listeners = new DataSetListeners(mockDataSet);
		final OnDataSetActionListener firstMockListener = mock(OnDataSetActionListener.class);
		final OnDataSetActionListener secondMockListener = mock(OnDataSetActionListener.class);
		listeners.registerOnDataSetActionListener(firstMockListener);
		listeners.registerOnDataSetActionListener(secondMockListener);
		// Act:
		listeners.unregisterOnDataSetActionListener(firstMockListener);
		// Assert:
		listeners.notifyDataSetActionSelected(DATA_SET_ACTION, 0, null);
		verifyNoInteractions(firstMockListener);
		verify(secondMockListener, times(1)).onDataSetActionSelected(DATA_SET_ACTION, 0, mockDataSet.getItemId(0), null);
	}

	@Test public void testUnregisterOnDataSetActionListenerWithoutRegisteredListeners() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.unregisterOnDataSetActionListener(mock(OnDataSetActionListener.class));
	}

	@Test public void testNotifyDataSetActionSelected() {
		// Arrange:
		final DataSet mockDataSet = mock(DataSet.class);
		final DataSetListeners listeners = new DataSetListeners(mockDataSet);
		final OnDataSetActionListener mockListener = mock(OnDataSetActionListener.class);
		listeners.registerOnDataSetActionListener(mockListener);
		// Act + Assert:
		for (int i = 0; i < 20; i++) {
			when(mockDataSet.getItemId(i)).thenReturn(i + 1L);
			when(mockListener.onDataSetActionSelected(DATA_SET_ACTION, i, i + 1L, null)).thenReturn(i % 2 == 0);
			assertThat(listeners.notifyDataSetActionSelected(DATA_SET_ACTION, i, null), is(i % 2 == 0));
			verify(mockListener).onDataSetActionSelected(DATA_SET_ACTION, i, i + 1L, null);
		}
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetActionSelectedWithoutRegisteredListeners() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.notifyDataSetActionSelected(0, 0, null);
	}

	@Test public void testIsEmpty() {
		// Arrange:
		final DataSetListeners listeners = new DataSetListeners(mock(DataSet.class));
		final OnDataSetListener firstMockListener = mock(OnDataSetListener.class);
		final OnDataSetListener secondMockListener = mock(OnDataSetListener.class);
		final OnDataSetActionListener firstMockActionListener = mock(OnDataSetActionListener.class);
		final OnDataSetActionListener secondMockActionListener = mock(OnDataSetActionListener.class);
		// Act + Assert:
		listeners.registerOnDataSetListener(firstMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.registerOnDataSetListener(secondMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.registerOnDataSetActionListener(firstMockActionListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.registerOnDataSetActionListener(secondMockActionListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.unregisterOnDataSetListener(firstMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.unregisterOnDataSetListener(secondMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.unregisterOnDataSetActionListener(firstMockActionListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.unregisterOnDataSetActionListener(secondMockActionListener);
		assertThat(listeners.isEmpty(), is(true));
	}
}