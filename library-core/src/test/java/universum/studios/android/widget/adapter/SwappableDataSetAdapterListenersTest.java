/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import org.junit.Test;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyNoInteractions;

/**
 * @author Martin Albedinsky
 */
public final class SwappableDataSetAdapterListenersTest implements TestCase {

	@Test public void testInstantiation() {
		// Act:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		// Assert:
		assertThat(listeners.isEmpty(), is(true));
	}

	@Test public void testRegisterOnDataSetSwapListener() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> firstMockListener = mock(TestListener.class);
		final OnDataSetSwapListener<String> secondMockListener = mock(TestListener.class);
		// Act:
		listeners.registerOnDataSetSwapListener(firstMockListener);
		listeners.registerOnDataSetSwapListener(firstMockListener);
		listeners.registerOnDataSetSwapListener(firstMockListener);
		listeners.registerOnDataSetSwapListener(secondMockListener);
		// Assert:
		listeners.notifyDataSetSwapStarted("data");
		listeners.notifyDataSetSwapFinished("data");
		verify(firstMockListener).onDataSetSwapStarted("data");
		verify(firstMockListener).onDataSetSwapFinished("data");
		verify(secondMockListener).onDataSetSwapStarted("data");
		verify(secondMockListener).onDataSetSwapFinished("data");
		verifyNoMoreInteractions(firstMockListener, secondMockListener);
	}

	@Test public void testUnregisterOnDataSetSwapListener() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> firstMockListener = mock(TestListener.class);
		final OnDataSetSwapListener<String> secondMockListener = mock(TestListener.class);
		listeners.registerOnDataSetSwapListener(firstMockListener);
		listeners.registerOnDataSetSwapListener(secondMockListener);
		// Act:
		listeners.unregisterOnDataSetSwapListener(firstMockListener);
		// Assert:
		listeners.notifyDataSetSwapStarted("data");
		listeners.notifyDataSetSwapFinished("data");
		verifyNoInteractions(firstMockListener);
		verify(secondMockListener).onDataSetSwapStarted("data");
		verify(secondMockListener).onDataSetSwapFinished("data");
		verifyNoMoreInteractions(secondMockListener);
	}

	@Test
	public void testUnregisterOnDataSetSwapListenerWithoutRegisteredListeners() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.unregisterOnDataSetSwapListener(mock(TestListener.class));
	}

	@Test public void testNotifyDataSetSwapStarted() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> mockListener = mock(TestListener.class);
		listeners.registerOnDataSetSwapListener(mockListener);
		// Act:
		listeners.notifyDataSetSwapStarted("data");
		// Assert:
		verify(mockListener).onDataSetSwapStarted("data");
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetSwapStartedOnEmptyListeners() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> mockListener = mock(TestListener.class);
		listeners.registerOnDataSetSwapListener(mockListener);
		listeners.unregisterOnDataSetSwapListener(mockListener);
		// Act:
		listeners.notifyDataSetSwapStarted("data");
	}

	@Test
	public void testNotifyDataSetSwapStartedWithoutRegisteredListeners() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.notifyDataSetSwapStarted("data");
	}

	@Test public void testNotifyDataSetSwapFinished() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> mockListener = mock(TestListener.class);
		listeners.registerOnDataSetSwapListener(mockListener);
		// Act:
		listeners.notifyDataSetSwapFinished("data");
		// Assert:
		verify(mockListener).onDataSetSwapFinished("data");
		verifyNoMoreInteractions(mockListener);
	}

	@Test public void testNotifyDataSetSwapFinishedOnEmptyListeners() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> mockListener = mock(TestListener.class);
		listeners.registerOnDataSetSwapListener(mockListener);
		listeners.unregisterOnDataSetSwapListener(mockListener);
		// Act:
		listeners.notifyDataSetSwapFinished("data");
	}

	@Test public void testNotifyDataSetSwapFinishedWithoutRegisteredListeners() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		// Act:
		// Only ensure that the listeners registry does not throw a NullPointerException exception.
		listeners.notifyDataSetSwapFinished("data");
	}

	@Test public void testIsEmpty() {
		// Arrange:
		final SwappableDataSetAdapterListeners<String> listeners = new SwappableDataSetAdapterListeners<>();
		final OnDataSetSwapListener<String> firstMockListener = mock(TestListener.class);
		final OnDataSetSwapListener<String> secondMockListener = mock(TestListener.class);
		// Act + Assert:
		listeners.registerOnDataSetSwapListener(firstMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.registerOnDataSetSwapListener(secondMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.unregisterOnDataSetSwapListener(firstMockListener);
		assertThat(listeners.isEmpty(), is(false));
		listeners.unregisterOnDataSetSwapListener(secondMockListener);
		assertThat(listeners.isEmpty(), is(true));
	}

	private interface TestListener extends OnDataSetSwapListener<String> {}
}