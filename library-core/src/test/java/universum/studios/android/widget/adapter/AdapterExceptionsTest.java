/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.widget.adapter;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class AdapterExceptionsTest implements TestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		AdapterExceptions.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<AdapterExceptions> constructor = AdapterExceptions.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testNoHolderFactoryAttached() {
		// Act:
		final RuntimeException exception = AdapterExceptions.noHolderFactoryAttached(new TestAdapter());
		final String adapterName = TestAdapter.class.getSimpleName();
		// Assert:
		assertThat(exception.getMessage(), is("Cannot create view holder without holder factory attached! " +
				"A desired factory may be specified via " + adapterName + ".setHolderFactory(...), " +
				"otherwise implementation of " + adapterName + ".onCreateViewHolder(...) method is required."));
		assertThat(exception.getCause(), is(nullValue()));
		throw exception;
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testNoHolderBinderAttached() {
		// Act:
		final RuntimeException exception = AdapterExceptions.noHolderBinderAttached(new TestAdapter());
		final String adapterName = TestAdapter.class.getSimpleName();
		// Assert:
		assertThat(exception.getMessage(), is("Cannot bind view holder without holder binder attached! " +
				"A desired binder may be specified via " + adapterName + ".setHolderBinder(...), " +
				"otherwise implementation of " + adapterName + ".onBindViewHolder(...) method is required."));
		assertThat(exception.getCause(), is(nullValue()));
		throw exception;
	}

	private static final class TestAdapter {}
}