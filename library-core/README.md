Widget-Adapters-Core
===============

This module contains core elements and interfaces that are used by adapter implementations from 
this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Awidget-adapters/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Awidget-adapters/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:widget-adapters-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DataSet](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core/src/main/java/universum/studios/android/widget/adapter/DataSet.java)
- [DataSetAdapter](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core/src/main/java/universum/studios/android/widget/adapter/DataSetAdapter.java)
- [OnDataSetListener](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core/src/main/java/universum/studios/android/widget/adapter/OnDataSetListener.java)
- [OnDataSetActionListener](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core/src/main/java/universum/studios/android/widget/adapter/OnDataSetActionListener.java)
- [OnDataSetSwapListener](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core/src/main/java/universum/studios/android/widget/adapter/OnDataSetSwapListener.java)
- [ItemPositionResolver](https://bitbucket.org/android-universum/widget-adapters/src/main/library-core/src/main/java/universum/studios/android/widget/adapter/ItemPositionResolver.java)